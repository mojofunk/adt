# adt

[![Build Status](https://ci.appveyor.com/api/projects/status/gitlab/mojofunk/adt?branch=master&svg=true)](https://ci.appveyor.com/project/mojofunk/adt)
[![Build Status](https://travis-ci.com/mojofunk/adt.svg?branch=master)](https://travis-ci.com/gitlab/mojofunk/adt)

adt is an open-source instrumentation library for C and C++(11).

The [{fmt}](http://fmtlib.net) library is a public dependency and is used for
string formatting. The 4.x branch is required at the moment, you can get
fmt from [github](https://github.com/fmtlib/fmt).

The [moodycamel concurrent
queue](https://github.com/cameron314/concurrentqueue) is a private
dependency that is included with the source.

## Features

- Macro API for conditional compilation in any build type.
- Lock-free pool based allocation and queuing.
- Support for output to [Trace Event
  Format](https://docs.google.com/document/d/1CvAClvFfyA5R-PhYUmn5OOQtYMH4h6I0nSsKchNAySU)
- Support for adding custom Log Sinks.
- Instrumentation of the Global C++ allocators.

## Documentation

There isn't any yet unfortunately and this needs to be improved.

I made a presentation that may help to get an understanding of the
library and how or why you might want to use it.

<iframe src="https://player.vimeo.com/video/260590555" width="800" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/260590555">Instrumenting Ardour</a> from <a href="https://vimeo.com/user82314406">Tim Mayberry</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## License

adt is licensed under the GNU GPLv3. The LICENSE.GPLv3 file applies to
everything in this repository except that which is explicitly annotated
as being written by other authors.

## Examples

TODO

## Building

The included CMake build script can be used to build and install the adt
library.

Currently only builds on Linux (Fedora) with gcc and Windows with MSVC
2017 have been tested.

gcc/MinGW builds have an issue that prevent the adt library from working
correctly.

## Tests

The [Google Test](https://github.com/google/googletest) framework/library
is used for unit testing.

The coverage and depth of the testsuite is currently insufficient. TODO

## Performance

Some time has been spent on improving the performance of the library but
there are currently no benchmarks or regression tests. TODO

## Related Projects

[adtmm](https://gitlab.com/mojofunk/adtmm) is a
[gtkmm](https://www.gtkmm.org) based GUI to control the instrumentation
and visualize the logging.
