#include <benchmark/benchmark.h>

int64_t
RandomNumber()
{
	return 0;
}

std::set<int64_t> ConstructRandomSet(int64_t size)
{
	std::set<int64_t> s;
	for (int i = 0; i < size; ++i)
		s.insert(s.end(), i);
	return s;
}

static void
BM_SetInsert(benchmark::State& state)
{
	std::set<int64_t> data;
	for (auto _ : state) {
		state.PauseTiming();
		data = ConstructRandomSet(state.range(0));
		state.ResumeTiming();
		for (int j = 0; j < state.range(1); ++j)
			data.insert(RandomNumber());
	}
}
BENCHMARK(BM_SetInsert)->Ranges({{1<<10, 8<<10}, {128, 512}});
