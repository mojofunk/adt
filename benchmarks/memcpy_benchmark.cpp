#include <benchmark/benchmark.h>

#include <cstring>

static void
BM_memcpy(benchmark::State& state)
{
	auto const size = static_cast<size_t>(state.range(0));
	char* src = new char[size];
	char* dst = new char[size];
	std::memset(src, 'x', size);
	for (auto _ : state) {
		std::memcpy(dst, src, size);
	}
	auto const bytes_processed = static_cast<int64_t>(state.iterations() * size);
	state.SetBytesProcessed(bytes_processed);
	delete[] src;
	delete[] dst;
}
BENCHMARK(BM_memcpy)->Range(8, 8 << 10);

//BENCHMARK_MAIN();
