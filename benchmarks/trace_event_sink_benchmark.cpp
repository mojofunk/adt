#include "adt_benchmark_includes.hpp"

static void
BM_TraceEventSinkRecordToString(benchmark::State& state)
{
	adt::init();

	double double_val = std::numeric_limits<double>::max();
	float float_val = std::numeric_limits<float>::min();
	uint16_t uint16_val = std::numeric_limits<uint16_t>::max();
	int16_t int16_val = std::numeric_limits<int16_t>::min();
	uint32_t uint32_val = std::numeric_limits<uint32_t>::max();
	int32_t int32_val = std::numeric_limits<int32_t>::min();
	uint64_t uint64_val = std::numeric_limits<uint64_t>::max();
	int64_t int64_val = std::numeric_limits<int64_t>::min();

	LogCategory category("BM_TraceEventSinkRecordToString");

	LogRecord log_record(&category,
	                     RecordType::FUNCTION_CALL,
	                     Timing(Timing::Type::DURATION,
	                            std::numeric_limits<uint64_t>::min(),
	                            std::numeric_limits<uint64_t>::max()),
	                     __LINE__,
	                     __FILE__,
	                     A_STRFUNC);

	log_record.copy_to_property_1(A_STRINGIFY(double_val), double_val);
	log_record.copy_to_property_2(A_STRINGIFY(float_val), float_val);
	log_record.copy_to_property_3(A_STRINGIFY(uint16_val), uint16_val);
	log_record.copy_to_property_4(A_STRINGIFY(int16_val), int16_val);
	log_record.copy_to_property_5(A_STRINGIFY(uint32_val), uint32_val);
	log_record.copy_to_property_6(A_STRINGIFY(int32_val), int32_val);
	log_record.copy_to_property_7(A_STRINGIFY(uint64_val), uint64_val);
	log_record.copy_to_property_8(A_STRINGIFY(int64_val), int64_val);

	std::size_t bytes = 0;
	for (auto _ : state) {
		std::string const record_string(
		    TraceEventSinkPrivate::record_to_string(log_record));
		bytes += record_string.size();
	}
	state.SetBytesProcessed(static_cast<int64_t>(bytes));
}
BENCHMARK(BM_TraceEventSinkRecordToString);
