#ifndef ADT_BENCHMARK_INCLUDES_H
#define ADT_BENCHMARK_INCLUDES_H

#include <benchmark/benchmark.h>

#include <adt-private.hpp>

#include <adt-test.hpp>

using namespace adt;

#endif // ADT_BENCHMARK_INCLUDES_H
