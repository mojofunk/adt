#include "adt_benchmark_includes.hpp"

template <class QueueType, class VectorType>
bool
test_enqueue(QueueType& queue, VectorType const& items)
{
	for (auto const& item : items) {
		bool enqueued = queue.try_enqueue(item);
		if (!enqueued) {
			return false;
		}
	}
	return true;
}

template <class QueueType, class VectorType>
bool
test_dequeue(QueueType& queue, VectorType& items)
{
	for (auto& item : items) {
		bool dequeued = queue.try_dequeue(item);
		if (!dequeued) {
			return false;
		}
	}

	return true;
}

template <class QueueType>
void
bm_queue_enqueue(benchmark::State& state)
{
	for (auto _ : state) {
		state.PauseTiming();
		auto const size = static_cast<size_t>(state.range(0));
		QueueType queue(size);
		std::vector<int64_t> in_vec(size, {});
		state.ResumeTiming();
		if (!test_enqueue(queue, in_vec)) {
			state.SkipWithError("Enqueue Failed!");
		}
	}
}

template <class QueueType>
void
bm_queue_dequeue(benchmark::State& state)
{
	for (auto _ : state) {
		state.PauseTiming();
		auto const size = static_cast<size_t>(state.range(0));
		QueueType queue(size);
		std::vector<int64_t> in_vec(size, {});
		std::vector<int64_t> out_vec(size, {});
		if (!test_enqueue(queue, in_vec)) {
			state.SkipWithError("Enqueue Failed!");
		}
		state.ResumeTiming();
		if (!test_dequeue(queue, out_vec)) {
			state.SkipWithError("Dequeue Failed!");
		}
	}
}

static void
BM_FSQueueEnqueue(benchmark::State& state)
{
	using FSQueue = FixedSizeQueue<int64_t, StdMallocAllocator<int64_t>>;
	bm_queue_enqueue<FSQueue>(state);
}
BENCHMARK(BM_FSQueueEnqueue)->Range(8, 8 << 12);

static void
BM_FSQueueDequeue(benchmark::State& state)
{
	using FSQueue = FixedSizeQueue<int64_t, StdMallocAllocator<int64_t>>;
	bm_queue_dequeue<FSQueue>(state);
}
BENCHMARK(BM_FSQueueDequeue)->Range(8, 8 << 12);

static void
BM_MoodyCamelQueueEnqueue(benchmark::State& state)
{
	using MoodyQueue = moodycamel::ConcurrentQueue<int64_t>;
	bm_queue_enqueue<MoodyQueue>(state);
}
BENCHMARK(BM_MoodyCamelQueueEnqueue)->Range(8, 8 << 7);

static void
BM_MoodyCamelQueueDequeue(benchmark::State& state)
{
	using MoodyQueue = moodycamel::ConcurrentQueue<int64_t>;
	bm_queue_dequeue<MoodyQueue>(state);
}
BENCHMARK(BM_MoodyCamelQueueDequeue)->Range(8, 8 << 7);