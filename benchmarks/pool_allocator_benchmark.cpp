#include "adt_benchmark_includes.hpp"

class Mallocator
{
public:
	/// @brief parameters are unused.
	Mallocator(std::size_t, std::size_t) {}

public:
	void* allocate(std::size_t size) { return std::malloc(size); }
	void deallocate(void* ptr) { std::free(ptr); }
};

template <class PoolType, class VectorType>
bool
test_allocate(PoolType& pool, VectorType& vec_out, std::size_t alloc_size)
{
	for (auto& ptr : vec_out) {
		void* alloc = pool.allocate(alloc_size);
		if (alloc == nullptr) {
			return false;
		}
		ptr = alloc;
	}
	return true;
}

template <class PoolType, class VectorType>
bool
test_deallocate(PoolType& pool, VectorType& vec)
{
	for (auto* addr : vec) {
		pool.deallocate(addr);
	}
	return true;
}

template <class PoolType>
void
bm_pool_allocate(benchmark::State& state)
{
	for (auto _ : state) {
		state.PauseTiming();
		auto const size = static_cast<size_t>(state.range(0));
		auto const count = static_cast<size_t>(state.range(1));
		PoolType pool(size, count);
		std::vector<void*> vec(count, {});
		state.ResumeTiming();
		if (!test_allocate(pool, vec, size)) {
			state.SkipWithError("Allocate Failed!");
		}
		state.PauseTiming();
		if (!test_deallocate(pool, vec)) {
			state.SkipWithError("Deallocate Failed!");
		}
		state.ResumeTiming();
	}
}

template <class PoolType>
void
bm_pool_deallocate(benchmark::State& state)
{
	for (auto _ : state) {
		state.PauseTiming();
		auto const size = static_cast<size_t>(state.range(0));
		auto const count = static_cast<size_t>(state.range(1));
		PoolType pool(size, count);
		std::vector<void*> vec(count, {});
		if (!test_allocate(pool, vec, size)) {
			state.SkipWithError("Allocate Failed!");
		}
		state.ResumeTiming();
		if (!test_deallocate(pool, vec)) {
			state.SkipWithError("Deallocate Failed!");
		}
	}
}

class MallocContentionThreads
{
public:
	MallocContentionThreads(
	    std::size_t num_allocators = std::thread::hardware_concurrency() * 2)
	{
		for (size_t i = 0; i != num_allocators; ++i) {
			allocator_threads.emplace_back(
			    std::thread(&MallocContentionThreads::test_allocation_thread, this));
		}
		std::this_thread::sleep_for(std::chrono::microseconds(50));
	}

	~MallocContentionThreads()
	{
		m_done = true;

		for (auto& t : allocator_threads) {
			t.join();
		}
	}

private:
	void test_allocation_thread()
	{
		while (!m_done) {
			size_t alloc_size = TestUtils::get_random_allocation_size();
			int sleep_time = TestUtils::get_random_sleep_time_us();

			char* alloc_ptr = (char*)std::malloc(alloc_size);

			for (auto i = 0ull; i < alloc_size; ++i) {
				*alloc_ptr = static_cast<char>(TestUtils::get_random_allocation_size());
			}

			std::this_thread::sleep_for(std::chrono::microseconds(sleep_time));

			std::free(alloc_ptr);
		}
	}

private:
	std::atomic<bool> m_done{ false };
	std::vector<std::thread> allocator_threads;
};

static std::vector<std::pair<int64_t, int64_t>> ranges = {
	{ 512, 2048 }, { 4 << 10, 8 << 10 }
};

static void
BM_STVectorMemoryPoolAllocate(benchmark::State& state)
{
	MallocContentionThreads threads;
	bm_pool_allocate<STVectorMemoryPool>(state);
}
BENCHMARK(BM_STVectorMemoryPoolAllocate)->Ranges(ranges);

static void
BM_STVectorMemoryPoolDeallocate(benchmark::State& state)
{
	MallocContentionThreads threads;
	bm_pool_deallocate<STVectorMemoryPool>(state);
}
BENCHMARK(BM_STVectorMemoryPoolDeallocate)->Ranges(ranges);

static void
BM_STFSQueueMemoryPoolAllocate(benchmark::State& state)
{
	MallocContentionThreads threads;
	bm_pool_allocate<FixedSizeQueueMemoryPool>(state);
}
BENCHMARK(BM_STFSQueueMemoryPoolAllocate)->Ranges(ranges);

static void
BM_STFSQueueMemoryPoolDeallocate(benchmark::State& state)
{
	MallocContentionThreads threads;
	bm_pool_deallocate<FixedSizeQueueMemoryPool>(state);
}
BENCHMARK(BM_STFSQueueMemoryPoolDeallocate)->Ranges(ranges);

static void
BM_MTMemoryPoolAllocate(benchmark::State& state)
{
	MallocContentionThreads threads;
	bm_pool_allocate<MTMemoryPool>(state);
}
BENCHMARK(BM_MTMemoryPoolAllocate)->Ranges(ranges);

static void
BM_MTMemoryPoolDeallocate(benchmark::State& state)
{
	MallocContentionThreads threads;
	bm_pool_deallocate<MTMemoryPool>(state);
}
BENCHMARK(BM_MTMemoryPoolDeallocate)->Ranges(ranges);

static void
BM_MallocatorAllocate(benchmark::State& state)
{
	MallocContentionThreads threads;
	bm_pool_allocate<Mallocator>(state);
}
BENCHMARK(BM_MallocatorAllocate)->Ranges(ranges);

static void
BM_MallocatorDeallocate(benchmark::State& state)
{
	MallocContentionThreads threads;
	bm_pool_deallocate<Mallocator>(state);
}
BENCHMARK(BM_MallocatorDeallocate)->Ranges(ranges);