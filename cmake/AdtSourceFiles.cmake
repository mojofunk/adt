file(GLOB ADT_PUBLIC_HEADER "src/adt.hpp")
file(GLOB ADT_PUBLIC_SUBHEADERS "src/adt/*.h*")
file(GLOB ADT_NOOP_HEADER "src/adt-noop.hpp")

file(GLOB ADT_PRIVATE_HEADER "src/adt/adt-private.hpp")
file(GLOB ADT_PRIVATE_SUBHEADERS "src/adt/private/*.hpp")

file(GLOB ADT_MOODYCAMEL_HEADERS "src/moodycamel/*.h")

file(GLOB ADT_PUBLIC_SOURCES "src/adt/*.cpp")
file(GLOB ADT_PRIVATE_SOURCES "src/adt/private/*.cpp")

file(GLOB ADT_UNITY_SOURCE "src/adt.cpp")

set(ADT_SOURCES
	"${ADT_PUBLIC_HEADER}"
	"${ADT_PUBLIC_SUBHEADERS}"
	"${ADT_NOOP_HEADER}"
	"${ADT_PRIVATE_HEADER}"
	"${ADT_PRIVATE_SUBHEADERS}"
	"${ADT_MOODYCAMEL_HEADERS}"
)

file(GLOB ADT_CAPI_HEADER "src/adt-capi.h")
file(GLOB ADT_CAPI_UNITY_SOURCE "src/adt-capi.cpp")
file(GLOB ADT_CAPI_SUBHEADERS "src/adt-capi/*.h")
file(GLOB ADT_CAPI_PUBLIC_SOURCES "src/adt-capi/*.cpp")

set(ADT_CAPI_SOURCES
	"${ADT_CAPI_HEADER}"
	"${ADT_CAPI_SUBHEADERS}"
)

if(ADT_BUILD_UNITY)
	list(APPEND ADT_SOURCES
		"${ADT_UNITY_SOURCE}"
	)
else()
	list(APPEND ADT_SOURCES
		"${ADT_PUBLIC_SOURCES}"
		"${ADT_PRIVATE_SOURCES}"
		"${ADT_CAPI_SOURCES}"
		"${ADT_CAPI_PUBLIC_SOURCES}"
	)
endif()
