set(ADT_TEST_TARGET adt-test-${ADT_VERSION_MAJOR})

if(ADT_BUILD_SHARED_LIBS)
	add_library(${ADT_TEST_TARGET} SHARED ${ADT_TEST_SOURCES})
else()
	add_library(${ADT_TEST_TARGET} STATIC ${ADT_TEST_SOURCES})
endif()

# This seems a bit silly
add_library(${ADT_TEST_TARGET}::${ADT_TEST_TARGET} ALIAS ${ADT_TEST_TARGET})

set_target_properties(${ADT_TEST_TARGET} PROPERTIES
    VERSION ${ADT_VERSION}
    SOVERSION ${ADT_VERSION_MAJOR}
)

if(NOT ADT_BUILD_UNITY)
    target_precompile_headers(${ADT_TEST_TARGET} PRIVATE src/adt-test/source_includes.cpp)
endif()

adt_set_target_cxx_properties(${ADT_TEST_TARGET})

if(ADT_ENABLE_CLANG_TIDY)
	set_target_properties(
		${ADT_TEST_TARGET} PROPERTIES
		CXX_CLANG_TIDY "${ADT_CLANG_TIDY_CMD}"
	)
endif()

target_include_directories(${ADT_TEST_TARGET}
    PUBLIC
        $<INSTALL_INTERFACE:include/${ADT_TARGET}>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
    PRIVATE
        # For config.h that isn't public, see below
        ${CMAKE_CURRENT_BINARY_DIR}
)

target_compile_options(${ADT_TEST_TARGET} PRIVATE ${ADT_WARNINGS})

if(ADT_BUILD_SHARED_LIBS)
    target_compile_definitions(${ADT_TEST_TARGET} PRIVATE "ADT_BUILDING_DLL")
else()
    target_compile_definitions(${ADT_TEST_TARGET} PUBLIC "ADT_STATIC")
endif()

target_link_libraries(${ADT_TEST_TARGET} PUBLIC ${ADT_TARGET}::${ADT_TARGET})
