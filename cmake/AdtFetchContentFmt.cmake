include(FetchContent)

FetchContent_Declare(
	fmt
	GIT_REPOSITORY https://gitlab.com/mojofunk/fmt.git
	GIT_TAG 7.1.3
	GIT_SHALLOW 1
)

message(STATUS "Downloading and configuring fmt library")
FetchContent_MakeAvailable(fmt)

# Required to export the fmt target during install:
SET(ADT_FMT_TARGET fmt)
