find_path(DBGHELP_INCLUDE_DIRS dbghelp.h)

find_library(DBGHELP_LIBRARY DbgHelp)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(DBGHELP DEFAULT_MSG DBGHELP_LIBRARY DBGHELP_INCLUDE_DIRS)

if(DBGHELP_FOUND)
	set(DBGHELP_LIBRARIES ${DBGHELP_LIBRARY})
endif()