set(ADT_INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/${ADT_TARGET})

install(EXPORT ${ADT_TARGET}-targets
    FILE ${ADT_TARGET}-targets.cmake
    NAMESPACE ${ADT_TARGET}::
    DESTINATION ${ADT_INSTALL_CONFIGDIR}
)

include(CMakePackageConfigHelpers)

configure_package_config_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake/adt-config.cmake.in
    ${CMAKE_BINARY_DIR}/cmake/${ADT_TARGET}-config.cmake
    INSTALL_DESTINATION ${ADT_INSTALL_CONFIGDIR}
)

# Use AnyNewerVersion here rather than SameMajorVersion as the namespace
# contains the major version. Unless I'm missing some detail about how
# this is all supposed to work.
write_basic_package_version_file(
    ${CMAKE_BINARY_DIR}/cmake/${ADT_TARGET}-config-version.cmake
    VERSION ${ADT_VERSION}
    COMPATIBILITY AnyNewerVersion
)

install(
    FILES
        ${CMAKE_BINARY_DIR}/cmake/${ADT_TARGET}-config.cmake
        ${CMAKE_BINARY_DIR}/cmake/${ADT_TARGET}-config-version.cmake
    DESTINATION ${ADT_INSTALL_CONFIGDIR}
)

export(EXPORT ${ADT_TARGET}-targets
    FILE ${CMAKE_BINARY_DIR}/cmake/${ADT_TARGET}-targets.cmake
    NAMESPACE ${ADT_TARGET}::
)
