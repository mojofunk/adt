enable_testing()

set(ADT_COMMON_TEST_SOURCES
   tests/gtest_main.cpp
   tests/test_sink.cpp
)

if(MSVC)
    # remove dll-interface warnings related to gtest...which are
    # hopefully not relevant
    add_definitions(/wd4251 /wd4275)
endif()

set(ADT_TEST_LINK_LIBRARIES GTest::GTest GTest::Main ${ADT_TEST_TARGET}::${ADT_TEST_TARGET})

# Not possible until CMake version 3.12
add_library(ADT_COMMON_TEST_OBJECTS OBJECT ${ADT_COMMON_TEST_SOURCES})
target_link_libraries(ADT_COMMON_TEST_OBJECTS PUBLIC ${ADT_TEST_LINK_LIBRARIES})
target_compile_options(ADT_COMMON_TEST_OBJECTS PUBLIC ${ADT_WARNINGS})
target_precompile_headers(ADT_COMMON_TEST_OBJECTS PRIVATE tests/adt_test_includes.hpp)

adt_set_target_cxx_properties(ADT_COMMON_TEST_OBJECTS)

set(FMT_ALLOC_TEST_SOURCE tests/fmt_alloc_test.cpp)

# A standalone test that can't link to adt
add_executable(adt-fmt-alloc-test ${FMT_ALLOC_TEST_SOURCE})
target_link_libraries(adt-fmt-alloc-test PRIVATE GTest::GTest GTest::Main fmt::fmt)
target_compile_options(adt-fmt-alloc-test PRIVATE ${ADT_WARNINGS})
adt_set_target_cxx_properties(adt-fmt-alloc-test)
add_test(NAME adt-fmt-alloc-test COMMAND adt-fmt-alloc-test)

set(ADT_TRACE_EVENT_SINK_TEST_SOURCE tests/trace_event_sink_old_test.cpp)

add_executable(adt-trace-event-sink-old-test ${ADT_TRACE_EVENT_SINK_TEST_SOURCE})
target_link_libraries(adt-trace-event-sink-old-test PRIVATE GTest::GTest ${ADT_TEST_TARGET}::${ADT_TEST_TARGET})
target_compile_options(adt-trace-event-sink-old-test PRIVATE ${ADT_WARNINGS})
adt_set_target_cxx_properties(adt-trace-event-sink-old-test)
add_test(NAME adt-trace-event-sink-old-test COMMAND adt-trace-event-sink-old-test)

if(ADT_ENABLE_STACK_TRACE)
    set(ADT_STACK_TRACE_TEST_SOURCE tests/stack_trace_test.cpp)

    check_cxx_compiler_flag(-fno-omit-frame-pointer ADT_FLAG_NO_OMIT_FRAME_POINTER)

    add_executable(adt-stack-trace-test ${ADT_STACK_TRACE_TEST_SOURCE})
    target_link_libraries(adt-stack-trace-test PRIVATE ${ADT_TEST_LINK_LIBRARIES})
    target_compile_options(adt-stack-trace-test PRIVATE ${ADT_WARNINGS})
    adt_set_target_cxx_properties(adt-stack-trace-test)
    if(ADT_FLAG_NO_OMIT_FRAME_POINTER)
        target_compile_options(adt-stack-trace-test PUBLIC -fno-omit-frame-pointer)
        # rdynamic is needed so backtrace() can generate symbol names for
        # functions in the executable
         set_target_properties(adt-stack-trace-test PROPERTIES LINK_FLAGS -rdynamic)
    endif()
    add_test(NAME adt-stack-trace-test COMMAND adt-stack-trace-test)
endif()

set(ADT_MACRO_API_NOOP_TEST_SOURCE tests/macro_api_test.cpp)

add_executable(adt-macro-api-noop-test ${ADT_MACRO_API_NOOP_TEST_SOURCE})
target_compile_definitions(adt-macro-api-noop-test PRIVATE ADT_ENABLE_NOOP)
target_link_libraries(adt-macro-api-noop-test PRIVATE ${ADT_TEST_LINK_LIBRARIES})
target_compile_options(adt-macro-api-noop-test PRIVATE ${ADT_WARNINGS})
adt_set_target_cxx_properties(adt-macro-api-noop-test)
add_test(NAME adt-macro-api-noop-test COMMAND adt-macro-api-noop-test)

set(ADT_INIT_TEST_SOURCE tests/init_test.cpp)

add_executable(adt-init-test ${ADT_INIT_TEST_SOURCE})
target_link_libraries(adt-init-test PRIVATE ${ADT_TEST_LINK_LIBRARIES})
target_compile_options(adt-init-test PRIVATE ${ADT_WARNINGS})
adt_set_target_cxx_properties(adt-init-test)
add_test(NAME adt-init-test COMMAND adt-init-test)

file(GLOB ADT_TEST_SOURCES "tests/*_test.cpp")

list(FILTER ADT_TEST_SOURCES EXCLUDE REGEX ".*${FMT_ALLOC_TEST_SOURCE}$")
list(FILTER ADT_TEST_SOURCES EXCLUDE REGEX ".*${ADT_TRACE_EVENT_SINK_TEST_SOURCE}$")
list(FILTER ADT_TEST_SOURCES EXCLUDE REGEX ".*${ADT_STACK_TRACE_TEST_SOURCE}$")
list(FILTER ADT_TEST_SOURCES EXCLUDE REGEX ".*${ADT_INIT_TEST_SOURCE}$")

if(ADT_BUILD_SINGLE_TESTS)
    foreach(TEST_SOURCE ${ADT_TEST_SOURCES})
        get_filename_component(TEST_NAME ${TEST_SOURCE} NAME_WE)
        string(REPLACE "_" "-" TEST_NAME ${TEST_NAME})
        set(TEST_NAME adt-${TEST_NAME})
        add_executable(${TEST_NAME} ${TEST_SOURCE})
        target_link_libraries(${TEST_NAME} PRIVATE ADT_COMMON_TEST_OBJECTS)
        target_precompile_headers(${TEST_NAME} REUSE_FROM ADT_COMMON_TEST_OBJECTS)
        adt_set_target_cxx_properties(${TEST_NAME})
        add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})
    endforeach()
endif()

set(ADT_TESTS_TARGET "adt-tests")
add_executable(${ADT_TESTS_TARGET} ${ADT_TEST_SOURCES})
target_link_libraries(${ADT_TESTS_TARGET} PRIVATE ADT_COMMON_TEST_OBJECTS)
target_precompile_headers(${ADT_TESTS_TARGET} REUSE_FROM ADT_COMMON_TEST_OBJECTS)
adt_set_target_cxx_properties(${ADT_TESTS_TARGET})
add_test(NAME ${ADT_TESTS_TARGET} COMMAND ${ADT_TESTS_TARGET})

if(ADT_ENABLE_COVERAGE)
    setup_target_for_coverage_gcovr(
        NAME ${ADT_TESTS_TARGET}-coverage
        EXECUTABLE ${ADT_TESTS_TARGET}
        DEPENDENCIES ${ADT_TESTS_TARGET})
endif()

if(ADT_ENABLE_CLANG_TIDY)
	set_target_properties(
		${ADT_TESTS_TARGET} PROPERTIES
		CXX_CLANG_TIDY "${DO_CLANG_TIDY}"
	)
endif()
