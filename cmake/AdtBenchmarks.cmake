set(ADT_BENCHMARK_DUMMY_PCH_SOURCE
    benchmarks/adt_benchmark_sources.cpp
)

add_library(ADT_COMMON_BENCHMARK_PCH OBJECT ${ADT_BENCHMARK_DUMMY_PCH_SOURCE})
target_compile_options(ADT_COMMON_BENCHMARK_PCH PUBLIC ${ADT_WARNINGS})

target_link_libraries(ADT_COMMON_BENCHMARK_PCH PUBLIC
    benchmark::benchmark
    ${ADT_TEST_TARGET}::${ADT_TEST_TARGET}
    )

adt_set_target_cxx_properties(ADT_COMMON_BENCHMARK_PCH)

target_precompile_headers(ADT_COMMON_BENCHMARK_PCH PRIVATE benchmarks/adt_benchmark_includes.hpp)

file(GLOB ADT_BENCHMARK_SOURCES "benchmarks/*_benchmark.cpp")

foreach(BENCHMARK_SOURCE ${ADT_BENCHMARK_SOURCES})
    get_filename_component(BENCHMARK_NAME ${BENCHMARK_SOURCE} NAME_WE)
    string(REPLACE "_" "-" BENCHMARK_NAME ${BENCHMARK_NAME})
    set(BENCHMARK_TARGET adt-${BENCHMARK_NAME})
    add_executable(${BENCHMARK_TARGET} ${BENCHMARK_SOURCE})
    target_link_libraries(${BENCHMARK_TARGET} PRIVATE
        benchmark::benchmark
        benchmark::benchmark_main
        ${ADT_TEST_TARGET}::${ADT_TEST_TARGET}
        ADT_COMMON_BENCHMARK_PCH
    )
    target_precompile_headers(${BENCHMARK_TARGET} REUSE_FROM ADT_COMMON_BENCHMARK_PCH)
    adt_set_target_cxx_properties(${BENCHMARK_TARGET})
endforeach()
