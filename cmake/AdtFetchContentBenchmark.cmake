include(FetchContent)

FetchContent_Declare(
	benchmark
	GIT_REPOSITORY https://github.com/google/benchmark.git
	GIT_TAG master
	GIT_SHALLOW 1
)

FetchContent_GetProperties(benchmark)
if(NOT benchmark_POPULATED)
	message(STATUS "Downloading and configuring Google Benchmark library")
	FetchContent_Populate(benchmark)
	# Don't run the benchmark tests as part of our build
	SET(BENCHMARK_ENABLE_TESTING OFF)
	add_subdirectory(${benchmark_SOURCE_DIR} ${benchmark_BINARY_DIR})
endif()
