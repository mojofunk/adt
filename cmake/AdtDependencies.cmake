if(ADT_BUILD_TESTS)
	find_package(GTest)

	if(NOT GTest_FOUND)
		include(AdtFetchContentGTest)
	endif()
endif()

if(ADT_BUILD_BENCHMARKS)
	find_package(benchmark CONFIG)

	if(NOT benchmark_FOUND)
		include(AdtFetchContentBenchmark)
	endif()
endif()

# Use the fmt target that is already defined in the environment then fallback
# to finding fmt outside the project (e.g. system wide install), and if that
# fails download the appropriate version of fmt and use that.
if(NOT TARGET fmt::fmt)
	# version >= 5.X is required
	find_package(fmt CONFIG)

	if(NOT fmt_FOUND)
		include(AdtFetchContentFmt)
	endif()
endif()

include(CheckIncludeFiles)

CHECK_INCLUDE_FILES(execinfo.h HAVE_EXECINFO_H)

if(MSVC)
	include(AdtFindDbgHelp)
endif()

if(ADT_ENABLE_STACK_TRACE)
	if(NOT HAVE_EXECINFO_H AND NOT DBGHELP_FOUND)
		if(NOT DEFINED HAVE_EXECINFO_H)
			message(FATAL_ERROR "execinfo.h Stack Trace dependency not found")
		else()
			message(FATAL_ERROR "dbghelp.h Stack Trace dependency not found")
		endif()
	endif()
else()
	message(WARNING "Stack Trace functionality has been disabled")
endif()

if(ADT_ENABLE_CLANG_TIDY)
	find_program(
		CLANG_TIDY_EXE
		NAMES "clang-tidy"
		DOC "Path to clang-tidy executable"
	)
	if(NOT CLANG_TIDY_EXE)
		message(FATAL_ERROR "clang-tidy not found.")
	else()
		message(STATUS "clang-tidy found: ${CLANG_TIDY_EXE}")
		set(ADT_CLANG_TIDY_MODERNIZE_OPTIONS
			"-checks=modernize-*,-modernize-make-unique,-modernize-use-transparent-functors,-modernize-use-equals-default"
		)
		set(ADT_CLANG_TIDY_READABILITY_OPTIONS
			"readability-*"
		)
		set(ADT_CLANG_TIDY_ANALYZER_OPTIONS
			"clang-analyzer-*,-clang-analyzer-alpha.*"
		)
		set(ADT_CLANG_TIDY_OPTIONS
			"${ADT_CLANG_TIDY_MODERNIZE_OPTIONS},${ADT_CLANG_TIDY_READABILITY_OPTIONS},${ADT_CLANG_TIDY_ANALYZER_OPTIONS}"
		)
		set(ADT_CLANG_TIDY_CMD "${CLANG_TIDY_EXE}" "${ADT_CLANG_TIDY_OPTIONS} -- -std=c++11")
	endif()
endif(ADT_ENABLE_CLANG_TIDY)
