set(ADT_TARGET adt-${ADT_VERSION_MAJOR})

if(ADT_BUILD_SHARED_LIBS)
	add_library(${ADT_TARGET} SHARED ${ADT_SOURCES})
else()
	add_library(${ADT_TARGET} STATIC ${ADT_SOURCES})
endif()

# This seems a bit silly
add_library(${ADT_TARGET}::${ADT_TARGET} ALIAS ${ADT_TARGET})

set_target_properties(${ADT_TARGET} PROPERTIES
    VERSION ${ADT_VERSION}
    SOVERSION ${ADT_VERSION_MAJOR}
)

if(NOT ADT_BUILD_UNITY)
    target_precompile_headers(${ADT_TARGET} PRIVATE src/adt/private/source_includes.cpp)
endif()

adt_set_target_cxx_properties(${ADT_TARGET})

if(ADT_ENABLE_CLANG_TIDY)
	set_target_properties(
		${ADT_TARGET} PROPERTIES
		CXX_CLANG_TIDY "${ADT_CLANG_TIDY_CMD}"
	)
endif()

target_include_directories(${ADT_TARGET}
    PUBLIC
        $<INSTALL_INTERFACE:include/${ADT_TARGET}>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
    PRIVATE
        # For config.h that isn't public, see below
        ${CMAKE_CURRENT_BINARY_DIR}
)

if(ADT_BUILD_SHARED_LIBS)
    target_compile_definitions(${ADT_TARGET} PRIVATE "ADT_BUILDING_DLL")
else()
    target_compile_definitions(${ADT_TARGET} PUBLIC "ADT_STATIC")
endif()

target_compile_definitions(${ADT_TARGET} PRIVATE "HAVE_CONFIG_H")

target_compile_options(${ADT_TARGET} PRIVATE ${ADT_WARNINGS})

target_link_libraries(${ADT_TARGET} PUBLIC fmt::fmt)

if(DBGHELP_FOUND)
    target_include_directories(${ADT_TARGET} PRIVATE ${DBGHELP_INCLUDE_DIRS})
    target_link_libraries(${ADT_TARGET} PUBLIC ${DBGHELP_LIBRARIES})
endif()
