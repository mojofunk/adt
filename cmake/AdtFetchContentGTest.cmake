include(FetchContent)

FetchContent_Declare(
	googletest	
	GIT_REPOSITORY https://github.com/google/googletest.git
	GIT_TAG v1.10.x
)

# TODO use FetchContent_MakeAvailable when min CMake version is >= 3.14
FetchContent_GetProperties(googletest)
if(NOT googletest_POPULATED)
	FetchContent_Populate(googletest)
	message(STATUS "Downloading and configuring Google Test library")
	set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
	add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
	# GTest doesn't yet define alias targets
	add_library(GTest::GTest ALIAS gtest)
	add_library(GTest::Main ALIAS gtest_main)
endif()
