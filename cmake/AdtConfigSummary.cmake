message("------------------------------------------------------------------------")
message("")
message("Configuration Summary - adt")
message("")

if(ADT_MASTER_PROJECT)
    include(FeatureSummary)
    feature_summary(WHAT ALL
                    DESCRIPTION "adt Features:"
                    VAR allFeaturesText)
    message(STATUS "${allFeaturesText}")

    # cmake info
    message(STATUS "CMAKE_BINARY_DIR:        ${CMAKE_BINARY_DIR}")
    message(STATUS "CMAKE_INSTALL_PREFIX:    ${CMAKE_INSTALL_PREFIX}")
    message(STATUS "CMAKE_SYSTEM_NAME:       ${CMAKE_SYSTEM_NAME}")
    message(STATUS "CMAKE_SYSTEM_VERSION:    ${CMAKE_SYSTEM_VERSION}")
    message(STATUS "CMAKE_SYSTEM_PROCESSOR:  ${CMAKE_SYSTEM_PROCESSOR}")
    message(STATUS "CMAKE_C_COMPILER:        ${CMAKE_C_COMPILER}")
    message(STATUS "CMAKE_CXX_COMPILER:      ${CMAKE_CXX_COMPILER}")
    message(STATUS "CMAKE_BUILD_TYPE:        ${CMAKE_BUILD_TYPE}")
    message("")
endif()

# adt specific
message(STATUS "ADT_VERSION:                 ${ADT_VERSION}")
message(STATUS "ADT_MASTER_PROJECT:          ${ADT_MASTER_PROJECT}")
message(STATUS "ADT_BUILD_SHARED_LIBS:       ${ADT_BUILD_SHARED_LIBS}")
message(STATUS "ADT_BUILD_UNITY              ${ADT_BUILD_UNITY}")
message(STATUS "ADT_BUILD_TESTS:             ${ADT_BUILD_TESTS}")
message(STATUS "ADT_BUILD_SINGLE_TESTS:      ${ADT_BUILD_SINGLE_TESTS}")
message(STATUS "ADT_WARNINGS_AS_ERRORS       ${ADT_WARNINGS_AS_ERRORS}")
message(STATUS "ADT_BUILD_BENCHMARKS:        ${ADT_BUILD_BENCHMARKS}")
message(STATUS "ADT_INSTALL_HEADERS:         ${ADT_INSTALL_HEADERS}")
message(STATUS "ADT_INSTALL_PKGCONFIG:       ${ADT_INSTALL_PKGCONFIG}")
message(STATUS "ADT_INSTALL_CMAKECONFIG:     ${ADT_INSTALL_CMAKECONFIG}")
message(STATUS "ADT_ENABLE_COVERAGE:         ${ADT_ENABLE_COVERAGE}")
message(STATUS "ADT_ENABLE_STACK_TRACE       ${ADT_ENABLE_STACK_TRACE}")
message(STATUS "ADT_ENABLE_CXX_ALLOCATORS    ${ADT_ENABLE_CXX_ALLOCATORS}")
message(STATUS "ADT_ENABLE_CLANG_TIDY:       ${ADT_ENABLE_CLANG_TIDY}")

message("")
message("------------------------------------------------------------------------")
