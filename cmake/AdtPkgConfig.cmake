configure_file(
    ${CMAKE_MODULE_PATH}/adt.pc.in
    ${CMAKE_CURRENT_BINARY_DIR}/${ADT_TARGET}.pc
    @ONLY
)

install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/${ADT_TARGET}.pc
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
)
