#include "adt_test_includes.hpp"

namespace LOG
{
A_DEFINE_LOG_CATEGORY(LogCategoryAPITest, "LogCategoryAPITest")
}

class LogCategoryAPITest : public adt::Test
{
public:
	bool run() override
	{
		int sleep_time = TestUtils::get_random_sleep_time_us();

		A_LOG_DATA1(LOG::LogCategoryAPITest, sleep_time);

		function_with_args_1(TestUtils::get_random_call_depth(), sleep_time);

		std::this_thread::sleep_for(std::chrono::microseconds(sleep_time));

		return true;
	}

private:
	static void function_with_args_1(int call_depth, int sleep_time)
	{
		A_LOG_CALL1(LOG::LogCategoryAPITest, sleep_time);

		sleep_time = TestUtils::get_random_sleep_time_us();

		std::this_thread::sleep_for(std::chrono::microseconds(sleep_time));

		--call_depth;

		if (call_depth == 0) {
			A_LOG_MSG(LOG::LogCategoryAPITest, "call_depth == 0");
			return;
		}
		A_LOG_DATA2(LOG::LogCategoryAPITest, call_depth, sleep_time);

		function_with_args_1(call_depth, sleep_time);
	}
};

TEST(LogCategoryAPI, ConcurrentLogDisabled)
{
	ConcurrencyTest test;

	test.add_default_count<LogCategoryAPITest>();

	ASSERT_TRUE(test.run());
}

TEST(LogCategoryAPI, ConcurrentLogEnabled)
{
	ScopedLogEnable le;

	ConcurrencyTest test;

	test.add_default_count<LogCategoryAPITest>();

	ASSERT_TRUE(test.run());
}

TEST(LogCategoryAPI, ConcurrentTraceEnabled)
{
	ScopedTraceEnable te;

	ConcurrencyTest test;

	test.add_default_count<LogCategoryAPITest>();

	ASSERT_TRUE(test.run());
}


TEST(LogCategoryAPI, ConcurrentSyncEnabled)
{
	Log::set_sync_enabled(true);

	ConcurrencyTest test;

	test.add_default_count<LogCategoryAPITest>();

	ASSERT_TRUE(test.run());

	Log::set_sync_enabled(false);
}
