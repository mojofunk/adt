#include "adt_test_includes.hpp"

#include "adt-capi.h"

#include "test_sink.hpp"

A_DEFINE_LOG_CATEGORY(CAPITestCategory, "TestCAPICategory");

class ScopedTestSink
{
public:
	ScopedTestSink()
	{
		adt::Log::set_enabled();
		adt::Log::set_sync_enabled(false); // default
		adt::Log::set_all_categories_enabled();
		adt::Log::set_all_types_enabled();

		test_sink = std::make_shared<TestSink>();

		adt::Log::set_sink(test_sink);
	}

	~ScopedTestSink() { adt::Log::set_enabled(false); }

public:
	std::shared_ptr<TestSink> test_sink;
};

class CAPITest
{
public:
	static const char* test_lvalue() { return "test_macro"; }

	static void test_capi_log_call()
	{
		ScopedTestSink sts;

		const int line = __LINE__;
		const char* const file_path = __FILE__;
		const char* const function_name = A_STRFUNC;

		sts.test_sink->set_enabled();

		logger_t logr =
		    adt_log_call_begin(CAPITestCategory, line, file_path, function_name);

		adt_log_add_string_property(&logr, "file_path", file_path);

		std::this_thread::sleep_for(std::chrono::seconds(1));

		adt_log_call_end(&logr);

		std::this_thread::sleep_for(std::chrono::seconds(1));

		ASSERT_EQ(sts.test_sink->m_records.size(), 1);

		auto const& record = sts.test_sink->m_records[0];

		ASSERT_STREQ(record->category->name, "TestCAPICategory");
		ASSERT_EQ(record->type, adt::RecordType::FUNCTION_CALL);
		ASSERT_EQ(record->thread_id, std::this_thread::get_id());
		ASSERT_EQ(record->src_location.file_path, file_path);
		ASSERT_EQ(record->src_location.line, line);
		ASSERT_EQ(record->src_location.function_name, function_name);
		ASSERT_EQ(record->properties.size(), 1);

		auto const& prop = record->properties.front();

		ASSERT_STREQ(prop.name.c_str(), "file_path");
		ASSERT_EQ(prop.value.type, adt::VariantType::STRING);
		ASSERT_STREQ(prop.value.string_value.c_str(), file_path);
	}

	static void test_capi_log_call_macro()
	{
		ScopedTestSink sts;

		sts.test_sink->set_enabled();

		A_LOG_CALL_BEGIN(CAPITestCategory);

		A_LOG_CALL_ADD_STRING_PROPERTY(test_lvalue());

		std::this_thread::sleep_for(std::chrono::seconds(1));

		A_LOG_CALL_END();

		std::this_thread::sleep_for(std::chrono::seconds(1));

		ASSERT_EQ(sts.test_sink->m_records.size(), 1);

		auto const& record = sts.test_sink->m_records[0];

		ASSERT_STREQ(record->category->name, "TestCAPICategory");
		ASSERT_EQ(record->type, adt::RecordType::FUNCTION_CALL);
		ASSERT_EQ(record->thread_id, std::this_thread::get_id());
		ASSERT_EQ(record->properties.size(), 1);

		auto const& prop = record->properties.front();

		ASSERT_STREQ(prop.name.c_str(), "test_lvalue()");
		ASSERT_EQ(prop.value.type, adt::VariantType::STRING);
		ASSERT_STREQ(prop.value.string_value.c_str(), "test_macro");
	}
};

TEST(testCAPI, testCAPILogCall)
{
	CAPITest::test_capi_log_call();
}

TEST(testCAPI, testCAPILogCallMacro)
{
	CAPITest::test_capi_log_call_macro();
}
