#include "adt_test_includes.hpp"

TEST(TraceEventSink, RecordToString)
{
	adt::init();

	double double_val = std::numeric_limits<double>::max();
	float float_val = std::numeric_limits<float>::min();
	uint16_t uint16_val = std::numeric_limits<uint16_t>::max();
	int16_t int16_val = std::numeric_limits<int16_t>::min();
	uint32_t uint32_val = std::numeric_limits<uint32_t>::max();
	int32_t int32_val = std::numeric_limits<int32_t>::min();
	uint64_t uint64_val = std::numeric_limits<uint64_t>::max();
	int64_t int64_val = std::numeric_limits<int64_t>::min();

	TraceEventSinkPrivate trace_event_sink_private;

	LogCategory category("adt::TraceEventSink::RecordToString");

	LogRecord log_record(&category,
	                     RecordType::FUNCTION_CALL,
	                     Timing(Timing::Type::DURATION,
	                            std::numeric_limits<uint64_t>::min(),
	                            std::numeric_limits<uint64_t>::max()),
	                     __LINE__,
	                     __FILE__,
	                     A_STRFUNC);

	log_record.copy_to_property_1(A_STRINGIFY(double_val), double_val);
	log_record.copy_to_property_2(A_STRINGIFY(float_val), float_val);
	log_record.copy_to_property_3(A_STRINGIFY(uint16_val), uint16_val);
	log_record.copy_to_property_4(A_STRINGIFY(int16_val), int16_val);
	log_record.copy_to_property_5(A_STRINGIFY(uint32_val), uint32_val);
	log_record.copy_to_property_6(A_STRINGIFY(int32_val), int32_val);
	log_record.copy_to_property_7(A_STRINGIFY(uint64_val), uint64_val);
	log_record.copy_to_property_8(A_STRINGIFY(int64_val), int64_val);

	std::string const record_string(
	    TraceEventSinkPrivate::record_to_string(log_record));

	// The string is required to be split as the function signature maybe/is
	// different on different systems/compilers and include __cdecl on Windows for
	// instance

	std::string const expected_string1 =
	    R"("ts":0,"ph":"X","cat":"adt::TraceEventSink::RecordToString","name":")";
	ASSERT_TRUE(record_string.find(expected_string1) != std::string::npos);

	std::string const expected_string2 =
	    R"(TraceEventSink_RecordToString_Test::TestBody","args":{"src_file":"trace_event_sink_test.cpp","src_line":"25",)";
	//ASSERT_EQ(record_string, expected_string2);
	ASSERT_TRUE(record_string.find(expected_string2) != std::string::npos);

	std::string const double_val_string = R"("double_val":"1.79769e+308")";
	ASSERT_TRUE(record_string.find(double_val_string) != std::string::npos);

	std::string const float_val_string = R"("float_val":"1.17549e-38")";
	ASSERT_TRUE(record_string.find(float_val_string) != std::string::npos);

	std::string const uint16_val_string = R"("uint16_val":"65535")";
	ASSERT_TRUE(record_string.find(uint16_val_string) != std::string::npos);

	std::string const int16_val_string = R"("int16_val":"-32768")";
	ASSERT_TRUE(record_string.find(int16_val_string) != std::string::npos);

	std::string const uint32_val_string = R"("uint32_val":"4294967295")";
	ASSERT_TRUE(record_string.find(uint32_val_string) != std::string::npos);

	std::string const int32_val_string = R"("int32_val":"-2147483648")";
	ASSERT_TRUE(record_string.find(int32_val_string) != std::string::npos);

	std::string const uint64_val_string = R"(uint64_val":"18446744073709551615")";
	ASSERT_TRUE(record_string.find(uint64_val_string) != std::string::npos);

	std::string const int64_val_string = R"("int64_val":"-9223372036854775808")";
	ASSERT_TRUE(record_string.find(int64_val_string) != std::string::npos);

	std::string const duration_val_string = R"("dur":"18446744073709551615")";
	ASSERT_TRUE(record_string.find(duration_val_string) != std::string::npos);
}