#include "adt_test_includes.hpp"

class NoLeaksTest : public adt::Test
{
public:
	bool run() override
	{
		bool passed = false;
		int* int_array1{ nullptr };
		int* int_array2 = new int[2048];

		TestUtils::NoLeaks no_leaks;

		int_array1 = new int[1024];
		delete[] int_array2;

		// failure is expected
		passed = no_leaks.test_failed();

		delete[] int_array1;
		return passed;
	}
};

class NoNewDeleteTest : public adt::Test
{
public:
	bool run() override
	{
		int* int_array = new int[1024];

		TestUtils::NoNewDelete no_new_delete;

		delete[] int_array;

		// failure is expected
		return no_new_delete.test_failed();
	}
};

class RandomIntRangeTest : public adt::Test
{
public:
	bool run() override
	{
		bool passed = true;

		std::pair<int, int> const bounds = std::minmax(std::rand(), std::rand());

		int const random_int =
		    TestUtils::get_random_int_in_range(bounds.first, bounds.second);

		if (random_int < bounds.first || random_int > bounds.second) {
			passed = false;
		}
		return passed;
	}
};

TEST(TestUtils, NoLeaks)
{
	ASSERT_TRUE(NoLeaksTest().run());
}

TEST(TestUtils, NoNewDelete)
{
	ASSERT_TRUE(NoNewDeleteTest().run());
}

TEST(TestUtils, RandomIntRangeTest)
{
	ASSERT_TRUE(RandomIntRangeTest().run());
}

TEST(TestUtils, Concurrent)
{
	ConcurrencyTest test;

	test.add_default_count<NoLeaksTest>();
	test.add_default_count<NoNewDeleteTest>();
	test.add_default_count<RandomIntRangeTest>();

	ASSERT_TRUE(test.run());
}
