#include "adt_test_includes.hpp"

class TestRunLoop : public adt::RunLoop
{
public:
	void on_iteration() override
	{
		int sleep_time = TestUtils::get_random_sleep_time_us();
		std::this_thread::sleep_for(std::chrono::microseconds(sleep_time));
		schedule_iteration();
	}
};

TEST(RunLoopTest, TestRunLoopST)
{
	TestRunLoop run_loop;

	ASSERT_FALSE(run_loop.running());

	std::thread run_loop_thread(&TestRunLoop::run, &run_loop);

	run_loop.wait_for_iteration();

	ASSERT_TRUE(run_loop.running());

	run_loop.schedule_iteration();

	std::this_thread::sleep_for(std::chrono::seconds(5));

	ASSERT_TRUE(run_loop.running());

	run_loop.quit();

	run_loop_thread.join();

	ASSERT_FALSE(run_loop.running());

	// Test that we can run it again in another thread.
	run_loop_thread = std::thread(&TestRunLoop::run, &run_loop);

	run_loop.schedule_iteration();

	std::this_thread::sleep_for(std::chrono::seconds(5));

	ASSERT_TRUE(run_loop.running());

	run_loop.quit();

	run_loop_thread.join();
}

#if 0
TEST(RunLoopTest, SimpleTest)
{
	std::vector<std::thread> test_threads;
	std::vector<TestRunLoop> test_run_loops;

	const int num_test_threads = std::thread::hardware_concurrency() * 2;

	for (int i = 0; i != num_test_threads; ++i) {
		test_threads.push_back(
		    std::thread(&TestRunLoop::run, this));
	}

	for (int i = 0; i != num_test_threads; ++i) {
		test_threads.push_back(
		    std::thread(&AllocationOperatorsTest::test_operator_new_array, this));
	}

	std::this_thread::sleep_for (std::chrono::seconds (5));

	for (auto& t : test_threads) {
		t.join();
	}
}

#endif
