#include "adt_test_includes.hpp"

template <class QueueType, class VectorType>
bool
test_enqueue(QueueType& queue, VectorType const& items)
{
	for (auto const& item : items) {
		bool enqueued = queue.try_enqueue(item);
		if (!enqueued) {
			return false;
		}
	}
	return true;
}

template <class QueueType, class VectorType>
bool
test_dequeue(QueueType& queue, VectorType& items)
{
	for (auto& item : items) {
		bool dequeued = queue.try_dequeue(item);
		if (!dequeued) {
			return false;
		}
	}

	return true;
}

template <class QueueType>
void
test_enqueue_dequeue(std::size_t size)
{
	QueueType queue(size);

	std::vector<uint64_t> in_vec(size, {});
	std::vector<uint64_t> out_vec(size, {});

	for (auto& value : in_vec) {
		value = TestUtils::get_random_uint64();
	}

	ASSERT_TRUE(test_enqueue(queue, in_vec));
	ASSERT_TRUE(test_dequeue(queue, out_vec));

	ASSERT_EQ(in_vec, out_vec);
}

template <class QueueType>
void
test_queue_sizes(std::vector<std::size_t> const& sizes)
{
	for (auto const& size : sizes) {
		test_enqueue_dequeue<QueueType>(size);
	}
}

TEST(QueueTest, SingleThreadFixedSizeQueue)
{
	using FSQueue = FixedSizeQueue<uint64_t, StdMallocAllocator<uint64_t>>;
	test_queue_sizes<FSQueue>({ 1, 2, 4, 8, 64, 1024, 4096, 65536 });
}

TEST(QueueTest, SingleThreadMoodyCamelQueue)
{
	using MoodyQueue = moodycamel::ConcurrentQueue<uint64_t>;
	test_queue_sizes<MoodyQueue>({ 1, 2, 4, 8, 64, 1024 });
}
