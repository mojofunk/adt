#include "adt_test_includes.hpp"

TEST(StdFile, OpenWriteClose)
{
	adt::StdFile std_file("OpenWriteClose.txt");

	ASSERT_FALSE(std_file.write("This should fail as the file isn't open yet."));

	// not open yet
	ASSERT_FALSE(std_file.close());

	ASSERT_TRUE(std_file.open());

	ASSERT_TRUE(std_file.write("This should succeed as the file is now open"));

	for (int i = 0; i <= 1000; ++i) {
		ASSERT_TRUE(std_file.write(std::to_string(i)));
	}

	ASSERT_TRUE(std_file.close());
}

TEST(StdFile, ReOpenTruncate)
{
	const char* const filename = "OpenTruncateWriteRead.txt";

	adt::StdFile std_file(filename);

	ASSERT_TRUE(std_file.open());

	ASSERT_TRUE(std_file.write("This will be replaced after truncate"));

	ASSERT_TRUE(std_file.close());

	ASSERT_TRUE(std_file.open());

	std::string const test_str("This file has been truncated.");

	ASSERT_TRUE(std_file.write(test_str));

	ASSERT_TRUE(std_file.close());

	std::ifstream file_istream(filename);

	ASSERT_TRUE(file_istream.is_open());

	std::string line;

	std::getline(file_istream, line);

	ASSERT_TRUE(line == test_str);

	ASSERT_TRUE(file_istream.eof());
}

TEST(StdFile, Utf8Filename)
{
	// TODO ??
}
