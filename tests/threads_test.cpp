#include "adt_test_includes.hpp"

TEST(Threads, i_can_set_a_null_thread_info)
{
	std::shared_ptr<adt::ThreadInfo> null_thread_info;

	adt::Threads::set_this_thread_info(null_thread_info);

	// Test that we always get a valid ThreadInfo ptr
	auto const this_thread_info = adt::Threads::get_this_thread_info();

	ASSERT_NE(nullptr, this_thread_info);

	ASSERT_EQ(adt::ThreadPriority::NONE, this_thread_info->priority);
}

TEST(Threads, i_can_set_thread_info)
{
	// GIVEN an existing thread info
	auto const this_thread_info = adt::Threads::get_this_thread_info();

	auto const name = "i_can_set_thread_info";
	auto const priority = adt::ThreadPriority::NORMAL;

	// AND it doesn't have any existing thread info set
	ASSERT_NE(priority, this_thread_info->priority);
	ASSERT_NE(name, this_thread_info->name);

	auto new_thread_info = std::make_shared<adt::ThreadInfo>(
	    std::this_thread::get_id(), name, priority);

	// WHEN I set the thread info for this thread
	adt::Threads::set_this_thread_info(new_thread_info);

	auto const thread_info = adt::Threads::get_this_thread_info();

	// THEN I get the expected values
	ASSERT_EQ(std::this_thread::get_id(), thread_info->id);
	ASSERT_EQ(name, thread_info->name);
	ASSERT_EQ(priority, thread_info->priority);

	// AFTER I reset the current ThreadInfo
	adt::Threads::set_this_thread_info({});
}

TEST(Threads, i_can_set_thread_info_priority)
{
	auto const priority = adt::ThreadPriority::HIGH;

	auto const this_thread_info = adt::Threads::get_this_thread_info();

	ASSERT_NE(priority, this_thread_info->priority);

	this_thread_info->priority = priority;

	ASSERT_EQ(priority, adt::Threads::get_this_thread_info()->priority);
	ASSERT_EQ(std::this_thread::get_id(),
	          adt::Threads::get_this_thread_info()->id);

	// AFTER I reset the current ThreadInfo
	adt::Threads::set_this_thread_info({});
}

TEST(Threads, i_can_register_thread_name_and_priority_using_a_macro)
{
	const std::string thread_name =
	    "i_can_register_thread_name_and_priority_using_a_macro";
	const auto priority = adt::ThreadPriority::HIGH;

	auto const this_thread_info = adt::Threads::get_this_thread_info();

	ASSERT_NE(priority, this_thread_info->priority);

	A_REGISTER_THREAD(thread_name.c_str(), adt::ThreadPriority::HIGH);

	ASSERT_EQ(thread_name, adt::Threads::get_this_thread_info()->name);
	ASSERT_EQ(adt::ThreadPriority::HIGH,
	          adt::Threads::get_this_thread_info()->priority);

	// AFTER I reset the current ThreadInfo
	adt::Threads::set_this_thread_info({});
}

TEST(Threads, i_can_register_thread_name_using_a_macro)
{
	auto const& thread_info = adt::Threads::get_this_thread_info();

	const std::string thread_name = "i_can_register_thread_name_using_a_macro";

	ASSERT_NE(thread_name, thread_info->name);

	A_REGISTER_THREAD_NAME(thread_name);

	ASSERT_EQ(thread_name, thread_info->name);

	// AFTER I reset the current ThreadInfo
	adt::Threads::set_this_thread_info({});
}

TEST(Threads, i_can_register_thread_priority_using_a_macro)
{
	auto const& priority = adt::Threads::get_this_thread_info()->priority;

	ASSERT_NE(adt::ThreadPriority::REALTIME, priority);

	A_REGISTER_THREAD_PRIORITY(adt::ThreadPriority::REALTIME);

	ASSERT_EQ(adt::ThreadPriority::REALTIME, priority);

	// AFTER I reset the current ThreadInfo
	adt::Threads::set_this_thread_info({});
}
