#include "adt_test_includes.hpp"

namespace LOG {
	A_DEFINE_LOG_CATEGORY(TraceEventSinkTest, "TraceEventSinkTest")
}

/**
 * This tests needs to be separate to the other tests because it will
 * replace the test sink set by TestUtils.
 */

TEST(TraceEventSink, WriteAsync)
{
	adt::ScopedLogEnable sle;

	const char* const test_filename = "TraceEventSink.WriteAsync.trace";
	auto trace_event_sink = std::make_shared<adt::TraceEventSink>();

	auto old_sink = adt::Log::get_sink();

	auto log_file = std::make_shared<adt::StdFile>(test_filename);
	trace_event_sink->set_output_file(log_file);

	trace_event_sink->set_enabled(true);
	adt::Log::set_sink(trace_event_sink);

	{
		double d = 0.123456789;
		float f = 0.987654321f;
		uint16_t ui16 = 0;
		int16_t i16 = 0;
		uint32_t ui32 = 0;
		int32_t i32 = 0;
		uint64_t ui64 = 0;
		int64_t i64 = 0;

		for (int i = 0; i < 10 ; ++i) {
			A_LOG_MSG(LOG::TraceEventSinkTest,
			          A_FMT("An async message for the TraceEventSink {} {}", d, f));

			A_LOG_DATA8(LOG::TraceEventSinkTest, d, f, ui16, i16, ui32, i32, ui64, i64);

			d++, f--, ui16++, i16--, ui32++, i32--, ui64++, i64--;
		}
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	adt::Log::set_sink(old_sink);

	// make sure file is closed etc.
	trace_event_sink.reset();
	log_file.reset();

	std::ifstream file_istream(test_filename);

	ASSERT_TRUE(file_istream.is_open());

	std::string line;

	int line_count = 0;
	while(!file_istream.eof()) {
		std::getline(file_istream, line);
		++line_count;
	}

	ASSERT_TRUE(line_count == 20);
}

int
main(int argc, char** argv)
{
	adt::Log::set_enabled();

	int exit_success(1);

	::testing::InitGoogleTest(&argc, argv);

	{
		exit_success = RUN_ALL_TESTS();
	}

	return exit_success;
}
