#include "test_sink.hpp"

using namespace adt;

TestSink::TestSink()
    : m_record_handling_thread(&TestSink::run, this)
    , m_enabled(false)
    , m_record_queue(8192)
{
	m_records.reserve(8192);
	wait_for_iteration();
}

TestSink::~TestSink()
{
	quit();
	m_record_handling_thread.join();

	// Don't handle any records still left in queue
	// process_records();

	TestSink::set_enabled(false);
}

void
TestSink::clear_record_queue()
{
	std::shared_ptr<Record> new_record;

	while (m_record_queue.try_dequeue(new_record)) {
		// we aren't doing anything with the dequeued record
	}
}

void
TestSink::set_enabled(bool enable)
{
	ADT_DEBUG("TraceEventSink::set_enabled: %s\n", enable ? "true" : "false");

	if (enable && !m_enabled) {
		clear_record_queue();
		m_enabled = true;
		m_records.clear();
	} else if (!enable && m_enabled) {
		m_enabled = false;
	}
}

bool
TestSink::is_internal_thread(std::thread::id thread_id)
{
	if (!running()) {
		return true;
	}
#ifdef ADT_ENABLE_INTERNAL_LOGGING
	// Log allocations made in the record writing thread
	return false;
#else
	return (thread_id == m_record_handling_thread.get_id());
#endif
}

void
TestSink::handle_record_async(const std::shared_ptr<Record>& record)
{
	if (!m_enabled) {
		return;
	}

	m_record_queue.enqueue(record);

	// wakeup processing thread
	schedule_iteration();
}

void
TestSink::handle_record_sync(const std::shared_ptr<Record>& record)
{
	if (!m_enabled) {
		return;
	}

	m_records.push_back(record);
}

void
TestSink::on_iteration()
{
	process_records();
}

void
TestSink::process_records()
{
	if (!m_enabled) {
		clear_record_queue();
		return;
	}

	std::shared_ptr<Record> new_record;

	while (m_record_queue.try_dequeue(new_record)) {
		m_records.push_back(new_record);
	}
}
