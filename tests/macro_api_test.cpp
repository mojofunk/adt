// Don't include adt_test_includes.hpp as these tests should
// only depend on public adt macro API

#include <gtest/gtest.h>

#include <adt-test.hpp>

namespace LOG
{
A_DEFINE_LOG_CATEGORY(MacroAPITest, "MacroAPITest")
}

#ifdef ADT_ENABLE_NOOP

class MacroAPITest
{
public:
	MacroAPITest() = default;
	virtual ~MacroAPITest() {}

public: // adt::Test interface
	virtual bool run() = 0;

protected:
	A_DECLARE_CLASS_MEMBERS(MacroAPITest)
};

#else // Use actual adt API

class MacroAPITest : public adt::Test
{
public:
	MacroAPITest() = default;
	virtual ~MacroAPITest() {}

public: // adt::Test interface
	virtual bool run() = 0;

protected: // Data Members
	// TODO Init all members with random values
	char const* const m_char_array_value{ "CharArrayValue" };

	std::string const m_std_string_value{ "StdStringValue" };

	bool const m_bool_value{ adt::TestUtils::get_random_bool() };

	int8_t const m_int8_value{ adt::TestUtils::get_random_int8() };
	uint8_t const m_uint8_value{ adt::TestUtils::get_random_uint8() };

	int16_t const m_int16_value{ adt::TestUtils::get_random_int16() };
	uint16_t const m_uint16_value{ adt::TestUtils::get_random_uint16() };

	int32_t const m_int32_value{ adt::TestUtils::get_random_int32() };
	uint32_t const m_uint32_value{ adt::TestUtils::get_random_uint32() };

	int64_t const m_int64_value{ adt::TestUtils::get_random_int64() };
	uint64_t const m_uint64_value{ adt::TestUtils::get_random_uint64() };

	float const m_float_value{ adt::TestUtils::get_random_float() };

	double const m_double_value{ adt::TestUtils::get_random_double() };

protected:
	A_DECLARE_CLASS_MEMBERS(MacroAPITest)
};

#endif // !ADT_ENABLE_NOOP

A_DEFINE_CLASS_MEMBERS(MacroAPITest)

class LogMsgMacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_msg();
		a_log_class_msg();
		a_class_static_msg();
		a_class_msg();
		return true;
	}

public: // Methods
	static void a_log_msg() { A_LOG_MSG(LOG::MacroAPITest, A_STRFUNC); }

	void a_log_class_msg()
	{
		A_LOG_CLASS_MSG(LOG::MacroAPITest, m_char_array_value);
	}

	static void a_class_static_msg() { A_CLASS_STATIC_MSG(A_STRFUNC); }

	void a_class_msg() { A_CLASS_MSG(m_std_string_value); }
};

TEST(MacroAPITest, LogMsg)
{
	LogMsgMacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogDurationMacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_duration();
		a_log_class_duration();
		a_class_static_duration();
		a_class_duration();
		return true;
	}

public: // Methods
	static void a_log_duration() { A_LOG_DURATION(LOG::MacroAPITest, A_STRFUNC); }

	void a_log_class_duration()
	{
		A_LOG_CLASS_DURATION(LOG::MacroAPITest, A_STRFUNC);
	}

	static void a_class_static_duration() { A_CLASS_STATIC_DURATION(A_STRFUNC); }

	void a_class_duration() { A_CLASS_DURATION(A_STRFUNC); }
};

TEST(MacroAPITest, LogDuration)
{
	LogDurationMacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCallMacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call();
		a_log_class_call();
		a_class_static_call();
		a_class_call();
		return true;
	}

public: // Methods
	static void a_log_call() { A_LOG_CALL(LOG::MacroAPITest); }

	void a_log_class_call() { A_LOG_CLASS_CALL(LOG::MacroAPITest); }

	static void a_class_static_call() { A_CLASS_STATIC_CALL(); }

	void a_class_call() { A_CLASS_CALL(); }
};

TEST(MacroAPITest, LogCall)
{
	LogCallMacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCall1MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call1();
		a_log_class_call1();
		a_class_static_call1();
		a_class_call1();
		return true;
	}

public: // Methods
	static void a_log_call1()
	{
		A_LOG_CALL1(LOG::MacroAPITest, adt::TestUtils::get_random_int8());
	}

	void a_log_class_call1()
	{
		A_LOG_CLASS_CALL1(LOG::MacroAPITest, m_double_value);
	}

	static void a_class_static_call1()
	{
		A_CLASS_STATIC_CALL1(adt::TestUtils::get_random_int8());
	}

	void a_class_call1() { A_CLASS_CALL1(m_int64_value); }
};

TEST(MacroAPITest, LogCall1)
{
	LogCall1MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCall2MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call2();
		a_log_class_call2();
		a_class_static_call2();
		a_class_call2();
		return true;
	}

public: // Methods
	static void a_log_call2()
	{
		A_LOG_CALL2(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_int8());
	}

	void a_log_class_call2()
	{
		A_LOG_CLASS_CALL2(LOG::MacroAPITest, m_double_value, m_float_value);
	}

	static void a_class_static_call2()
	{
		A_CLASS_STATIC_CALL2(adt::TestUtils::get_random_int8(),
		                     adt::TestUtils::get_random_int8());
	}

	void a_class_call2() { A_CLASS_CALL2(m_int64_value, m_float_value); }
};

TEST(MacroAPITest, LogCall2)
{
	LogCall2MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCall3MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call3();
		a_log_class_call3();
		a_class_static_call3();
		a_class_call3();
		return true;
	}

public: // Methods
	static void a_log_call3()
	{
		A_LOG_CALL3(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_call3()
	{
		A_LOG_CLASS_CALL3(LOG::MacroAPITest, m_double_value, m_float_value, m_uint32_value);
	}

	static void a_class_static_call3()
	{
		A_CLASS_STATIC_CALL3(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_call3() { A_CLASS_CALL3(m_int64_value, m_float_value, m_double_value); }
};

TEST(MacroAPITest, LogCall3)
{
	LogCall3MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCall4MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call4();
		a_log_class_call4();
		a_class_static_call4();
		a_class_call4();
		return true;
	}

public: // Methods
	static void a_log_call4()
	{
		A_LOG_CALL4(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_call4()
	{
		A_LOG_CLASS_CALL4(LOG::MacroAPITest,
		                  m_double_value,
		                  m_float_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_call4()
	{
		A_CLASS_STATIC_CALL4(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_call4()
	{
		A_CLASS_CALL4(m_int64_value, m_uint16_value, m_float_value, m_double_value);
	}
};

TEST(MacroAPITest, LogCall4)
{
	LogCall4MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCall5MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call5();
		a_log_class_call5();
		a_class_static_call5();
		a_class_call5();
		return true;
	}

public: // Methods
	static void a_log_call5()
	{
		A_LOG_CALL5(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_bool(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_call5()
	{
		A_LOG_CLASS_CALL5(LOG::MacroAPITest,
		                  m_double_value,
		                  m_float_value,
		                  m_bool_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_call5()
	{
		A_CLASS_STATIC_CALL5(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_bool(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_call5()
	{
		A_CLASS_CALL5(
		    m_int64_value, m_uint16_value, m_uint32_value, m_float_value, m_double_value);
	}
};

TEST(MacroAPITest, LogCall5)
{
	LogCall5MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCall6MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call6();
		a_log_class_call6();
		a_class_static_call6();
		a_class_call6();
		return true;
	}

public: // Methods
	static void a_log_call6()
	{
		A_LOG_CALL6(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_bool(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int16(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_call6()
	{
		A_LOG_CLASS_CALL6(LOG::MacroAPITest,
		                  m_double_value,
		                  m_uint8_value,
		                  m_float_value,
		                  m_bool_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_call6()
	{
		A_CLASS_STATIC_CALL6(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_uint16(),
		                     adt::TestUtils::get_random_bool(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_call6()
	{
		A_CLASS_CALL6(m_int64_value,
		              m_uint16_value,
		              m_int16_value,
		              m_uint32_value,
		              m_float_value,
		              m_double_value);
	}
};

TEST(MacroAPITest, LogCall6)
{
	LogCall6MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCall7MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call7();
		a_log_class_call7();
		a_class_static_call7();
		a_class_call7();
		return true;
	}

public: // Methods
	static void a_log_call7()
	{
		A_LOG_CALL7(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_bool(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int16(),
		            adt::TestUtils::get_random_int32(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_call7()
	{
		A_LOG_CLASS_CALL7(LOG::MacroAPITest,
		                  m_double_value,
		                  m_int8_value,
		                  m_uint8_value,
		                  m_float_value,
		                  m_bool_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_call7()
	{
		A_CLASS_STATIC_CALL7(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_int16(),
		                     adt::TestUtils::get_random_uint16(),
		                     adt::TestUtils::get_random_bool(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_call7()
	{
		A_CLASS_CALL7(m_int64_value,
		              m_uint16_value,
		              m_int16_value,
		              m_int32_value,
		              m_uint32_value,
		              m_float_value,
		              m_double_value);
	}
};

TEST(MacroAPITest, LogCall7)
{
	LogCall7MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogCall8MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_call8();
		a_log_class_call8();
		a_class_static_call8();
		a_class_call8();
		return true;
	}

public: // Methods
	static void a_log_call8()
	{
		A_LOG_CALL8(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_bool(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int16(),
		            adt::TestUtils::get_random_int32(),
		            adt::TestUtils::get_random_uint32(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_call8()
	{
		A_LOG_CLASS_CALL8(LOG::MacroAPITest,
		                  m_double_value,
		                  m_int8_value,
		                  m_uint8_value,
		                  m_float_value,
		                  m_int32_value,
		                  m_bool_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_call8()
	{
		A_CLASS_STATIC_CALL8(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_int16(),
		                     adt::TestUtils::get_random_uint16(),
		                     adt::TestUtils::get_random_uint64(),
		                     adt::TestUtils::get_random_bool(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_call8()
	{
		A_CLASS_CALL8(m_int64_value,
		              m_uint64_value,
		              m_uint16_value,
		              m_int16_value,
		              m_int32_value,
		              m_uint32_value,
		              m_float_value,
		              m_double_value);
	}
};

TEST(MacroAPITest, LogCall8)
{
	LogCall8MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogData1MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_data1();
		a_log_class_data1();
		a_class_static_data1();
		a_class_data1();
		return true;
	}

public: // Methods
	static void a_log_data1()
	{
		A_LOG_DATA1(LOG::MacroAPITest, adt::TestUtils::get_random_int8());
	}

	void a_log_class_data1()
	{
		A_LOG_CLASS_DATA1(LOG::MacroAPITest, m_double_value);
	}

	static void a_class_static_data1()
	{
		A_CLASS_STATIC_DATA1(adt::TestUtils::get_random_int8());
	}

	void a_class_data1() { A_CLASS_DATA1(m_int64_value); }
};

TEST(MacroAPITest, LogData1)
{
	LogData1MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogData2MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_data2();
		a_log_class_data2();
		a_class_static_data2();
		a_class_data2();
		return true;
	}

public: // Methods
	static void a_log_data2()
	{
		A_LOG_DATA2(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_int8());
	}

	void a_log_class_data2()
	{
		A_LOG_CLASS_DATA2(LOG::MacroAPITest, m_double_value, m_float_value);
	}

	static void a_class_static_data2()
	{
		A_CLASS_STATIC_DATA2(adt::TestUtils::get_random_int8(),
		                     adt::TestUtils::get_random_int8());
	}

	void a_class_data2() { A_CLASS_DATA2(m_int64_value, m_float_value); }
};

TEST(MacroAPITest, LogData2)
{
	LogData2MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogData3MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_data3();
		a_log_class_data3();
		a_class_static_data3();
		a_class_data3();
		return true;
	}

public: // Methods
	static void a_log_data3()
	{
		A_LOG_DATA3(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_data3()
	{
		A_LOG_CLASS_DATA3(LOG::MacroAPITest, m_double_value, m_float_value, m_uint32_value);
	}

	static void a_class_static_data3()
	{
		A_CLASS_STATIC_DATA3(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_data3() { A_CLASS_DATA3(m_int64_value, m_float_value, m_double_value); }
};

TEST(MacroAPITest, LogData3)
{
	LogData3MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogData4MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_data4();
		a_log_class_data4();
		a_class_static_data4();
		a_class_data4();
		return true;
	}

public: // Methods
	static void a_log_data4()
	{
		A_LOG_DATA4(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_data4()
	{
		A_LOG_CLASS_DATA4(LOG::MacroAPITest,
		                  m_double_value,
		                  m_float_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_data4()
	{
		A_CLASS_STATIC_DATA4(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_data4()
	{
		A_CLASS_DATA4(m_int64_value, m_uint16_value, m_float_value, m_double_value);
	}
};

TEST(MacroAPITest, LogData4)
{
	LogData4MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogData5MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_data5();
		a_log_class_data5();
		a_class_static_data5();
		a_class_data5();
		return true;
	}

public: // Methods
	static void a_log_data5()
	{
		A_LOG_DATA5(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_bool(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_data5()
	{
		A_LOG_CLASS_DATA5(LOG::MacroAPITest,
		                  m_double_value,
		                  m_float_value,
		                  m_bool_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_data5()
	{
		A_CLASS_STATIC_DATA5(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_bool(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_data5()
	{
		A_CLASS_DATA5(
		    m_int64_value, m_uint16_value, m_uint32_value, m_float_value, m_double_value);
	}
};

TEST(MacroAPITest, LogData5)
{
	LogData5MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogData6MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_data6();
		a_log_class_data6();
		a_class_static_data6();
		a_class_data6();
		return true;
	}

public: // Methods
	static void a_log_data6()
	{
		A_LOG_DATA6(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_bool(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int16(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_data6()
	{
		A_LOG_CLASS_DATA6(LOG::MacroAPITest,
		                  m_double_value,
		                  m_uint8_value,
		                  m_float_value,
		                  m_bool_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_data6()
	{
		A_CLASS_STATIC_DATA6(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_uint16(),
		                     adt::TestUtils::get_random_bool(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_data6()
	{
		A_CLASS_DATA6(m_int64_value,
		              m_uint16_value,
		              m_int16_value,
		              m_uint32_value,
		              m_float_value,
		              m_double_value);
	}
};

TEST(MacroAPITest, LogData6)
{
	LogData6MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogData7MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_data7();
		a_log_class_data7();
		a_class_static_data7();
		a_class_data7();
		return true;
	}

public: // Methods
	static void a_log_data7()
	{
		A_LOG_DATA7(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_bool(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int16(),
		            adt::TestUtils::get_random_int32(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float());
	}

	void a_log_class_data7()
	{
		A_LOG_CLASS_DATA7(LOG::MacroAPITest,
		                  m_double_value,
		                  m_int8_value,
		                  m_uint8_value,
		                  m_float_value,
		                  m_bool_value,
		                  m_double_value,
		                  m_uint32_value);
	}

	static void a_class_static_data7()
	{
		A_CLASS_STATIC_DATA7(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_int16(),
		                     adt::TestUtils::get_random_uint16(),
		                     adt::TestUtils::get_random_bool(),
		                     adt::TestUtils::get_random_double());
	}

	void a_class_data7()
	{
		A_CLASS_DATA7(m_int64_value,
		              m_uint16_value,
		              m_int16_value,
		              m_int32_value,
		              m_uint32_value,
		              m_float_value,
		              m_double_value)
	}
};

TEST(MacroAPITest, LogData7)
{
	LogData7MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class LogData8MacroAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		a_log_data8();
		a_log_class_data8();
		a_class_static_data8();
		a_class_data8();
		return true;
	}

public: // Methods
	static void a_log_data8()
	{
		A_LOG_DATA8(LOG::MacroAPITest,
		            adt::TestUtils::get_random_int8(),
		            adt::TestUtils::get_random_bool(),
		            adt::TestUtils::get_random_uint16(),
		            adt::TestUtils::get_random_int16(),
		            adt::TestUtils::get_random_int32(),
		            adt::TestUtils::get_random_uint32(),
		            adt::TestUtils::get_random_int64(),
		            adt::TestUtils::get_random_float())
	}

	void a_log_class_data8()
	{
		A_LOG_CLASS_DATA8(LOG::MacroAPITest,
		                  m_double_value,
		                  m_int8_value,
		                  m_uint8_value,
		                  m_float_value,
		                  m_int32_value,
		                  m_bool_value,
		                  m_double_value,
		                  m_uint32_value)
	}

	static void a_class_static_data8()
	{
		A_CLASS_STATIC_DATA8(adt::TestUtils::get_random_int64(),
		                     adt::TestUtils::get_random_int32(),
		                     adt::TestUtils::get_random_uint32(),
		                     adt::TestUtils::get_random_int16(),
		                     adt::TestUtils::get_random_uint16(),
		                     adt::TestUtils::get_random_uint64(),
		                     adt::TestUtils::get_random_bool(),
		                     adt::TestUtils::get_random_double())
	}

	void a_class_data8()
	{
		A_CLASS_DATA8(m_int64_value,
		              m_uint64_value,
		              m_uint16_value,
		              m_int16_value,
		              m_int32_value,
		              m_uint32_value,
		              m_float_value,
		              m_double_value)
	}
};

TEST(MacroAPITest, LogData8)
{
	LogData8MacroAPITest test;
	ASSERT_TRUE(test.run());
}

class RegisterThreadAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		A_REGISTER_THREAD("RegisterThreadTest", adt::ThreadPriority::NONE)
		return true;
	}
};

TEST(MacroAPITest, RegisterThread)
{
	RegisterThreadAPITest test;
	ASSERT_TRUE(test.run());
}

class RegisterThreadNameAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		A_REGISTER_THREAD_NAME("RegisterThreadNameTest")
		return true;
	}
};

TEST(MacroAPITest, RegisterThreadName)
{
	RegisterThreadNameAPITest test;
	ASSERT_TRUE(test.run());
}

class RegisterThreadPriorityAPITest : public MacroAPITest
{
public: // Adt::Test
	bool run() override
	{
		A_REGISTER_THREAD_PRIORITY(adt::ThreadPriority::NONE)
		return true;
	}
};

TEST(MacroAPITest, RegisterThreadPriority)
{
	RegisterThreadPriorityAPITest test;
	ASSERT_TRUE(test.run());
}
