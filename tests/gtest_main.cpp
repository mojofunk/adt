#include <gtest/gtest.h>

#include <adt-test.hpp>

int
main(int argc, char** argv)
{
	adt::TestUtils test_utils;

	int exit_success(1);

	::testing::InitGoogleTest(&argc, argv);

	{
		exit_success = RUN_ALL_TESTS();
	}

	return exit_success;
}
