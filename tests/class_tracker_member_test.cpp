#include "adt_test_includes.hpp"

class Copyable
{
public:
	Copyable() {}

	Copyable(const Copyable&) = default;

	Copyable(Copyable&&) = delete;

public:
	static const char* get_class_name();

	adt::ClassTrackerMember<Copyable> mTracker;
};

const char*
Copyable::get_class_name()
{
	return "Copyable";
}
TEST(ClassTrackerMember, Copy)
{

	Copyable m1;

	adt::ScopedClassTracker tracker(m1.mTracker.get_class_tracker());

	Copyable m2(m1);

	ASSERT_EQ(tracker.get_instance_count_difference(), 1);
	ASSERT_EQ(tracker.get_copied_count_difference(), 1);
	ASSERT_EQ(tracker.get_moved_count_difference(), 0);
}

TEST(ClassTrackerMember, CopyAssignment)
{
	Copyable m1;

	adt::ScopedClassTracker tracker(m1.mTracker.get_class_tracker());

	Copyable m2 = m1;

	ASSERT_EQ(tracker.get_instance_count_difference(), 1);
	ASSERT_EQ(tracker.get_copied_count_difference(), 1);
	ASSERT_EQ(tracker.get_moved_count_difference(), 0);
}

class Movable
{
public:
	Movable() {}

	Movable(const Movable&) = delete;

	Movable(Movable&&) = default;

public:
	static const char* get_class_name();

	adt::ClassTrackerMember<Movable> mTracker;
};

const char*
Movable::get_class_name()
{
	return "Movable";
}

TEST(ClassTrackerMember, Move)
{
	Movable m1;

	adt::ScopedClassTracker tracker(m1.mTracker.get_class_tracker());

	Movable m2(std::move(m1));

	ASSERT_EQ(tracker.get_instance_count_difference(), 1);
	ASSERT_EQ(tracker.get_copied_count_difference(), 0);
	ASSERT_EQ(tracker.get_moved_count_difference(), 1);
}

TEST(ClassTrackerMember, MoveAssignment)
{
	Movable m1;

	adt::ScopedClassTracker tracker(m1.mTracker.get_class_tracker());

	Movable m2 = std::move(m1);

	ASSERT_EQ(tracker.get_instance_count_difference(), 1);
	ASSERT_EQ(tracker.get_copied_count_difference(), 0);
	ASSERT_EQ(tracker.get_moved_count_difference(), 1);
}
