#include "adt_test_includes.hpp"

#include "test_sink.hpp"

namespace LOG
{
A_DEFINE_LOG_CATEGORY(GlobalAllocatorTest, "AdtTest::GlobalAllocatorTest")
}

class GlobalAllocatorTest
{
public:
	static void test_logging()
	{
		adt::Log::set_enabled();
		std::shared_ptr<TestSink> test_sink = std::make_shared<TestSink>();

		adt::Log::set_sink(test_sink);

		std::array<int*, 17> int_ptr_array;
		int_ptr_array.fill(nullptr);

		test_sink->set_enabled();

		ASSERT_TRUE(test_sink->m_records.empty());

		adt::GlobalAllocator::set_logging_enabled();

		{
			for (auto& int_ptr : int_ptr_array) {
				int_ptr = new int;
			}

			// Dodgy way to try and ensure TestSink thread has a chance to
			// process all the records.
			std::this_thread::sleep_for(std::chrono::seconds(1));

			test_sink->set_enabled(false);

			ASSERT_TRUE(test_sink->m_records.size() == int_ptr_array.size());

			for (auto& int_ptr : int_ptr_array) {
				delete int_ptr;
			}

			std::this_thread::sleep_for(std::chrono::seconds(1));

			ASSERT_TRUE(test_sink->m_records.size() == int_ptr_array.size());
		}

		adt::GlobalAllocator::set_logging_enabled(false);
		adt::Log::set_enabled(false);
	}
};

TEST(GlobalAllocator, Logging)
{
	GlobalAllocatorTest::test_logging();
}
