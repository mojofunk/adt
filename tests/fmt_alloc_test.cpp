#include <gtest/gtest.h>

#include "fmt/format.h"

class GlobalAllocatorPrivate
{
public:
	void set_enabled(bool enabled = true) { m_enabled = enabled; }

public:
	void* allocate(std::size_t bytes)
	{
		if (m_enabled) {
			return std::malloc(bytes);
		}
		return nullptr;
	}

	void deallocate(void* ptr) { std::free(ptr); }

	void deallocate(void* ptr, std::size_t bytes)
	{
		(void)bytes;
		std::free(ptr);
	}

private:
	bool m_enabled = { true };
};

static GlobalAllocatorPrivate&
get_global_allocator_private()
{
	static GlobalAllocatorPrivate allocator_private;
	return allocator_private;
}

class GlobalAllocator
{
public:
	static void* allocate(std::size_t bytes)
	{
		return get_global_allocator_private().allocate(bytes);
	}

	static void deallocate(void* ptr)
	{
		return get_global_allocator_private().deallocate(ptr);
	}

	static void deallocate(void* ptr, std::size_t bytes)
	{
		return get_global_allocator_private().deallocate(ptr, bytes);
	}

	static void set_enabled(bool enabled = true)
	{
		get_global_allocator_private().set_enabled(enabled);
	}
};

void*
operator new(std::size_t count)
{
	return GlobalAllocator::allocate(count);
}

void*
operator new[](std::size_t count)
{
	return GlobalAllocator::allocate(count);
}

void
operator delete(void* ptr) noexcept
{
	GlobalAllocator::deallocate(ptr);
}

void
operator delete[](void* ptr) noexcept
{
	GlobalAllocator::deallocate(ptr);
}

void
operator delete(void* ptr, std::size_t size)
{
	GlobalAllocator::deallocate(ptr, size);
}

void
operator delete[](void* ptr, std::size_t size)
{
	GlobalAllocator::deallocate(ptr, size);
}

template <typename T>
class TestStringAllocator
{
public:
	typedef T value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;

public:
	template <typename U>
	struct rebind {
		typedef TestStringAllocator<U> other;
	};

public:
	inline TestStringAllocator(){}; // = default

	inline ~TestStringAllocator() {}

	inline explicit TestStringAllocator(const TestStringAllocator<T>&) {}

	template <typename U>
	inline TestStringAllocator(const TestStringAllocator<U>&)
	{
	}

	bool empty() { return false; }

	inline pointer address(reference r) { return &r; }
	inline const_pointer address(const_reference r) { return &r; }

	inline pointer allocate(size_type cnt,
	                        typename std::allocator<void>::const_pointer = 0)
	{
		if (cnt <= std::numeric_limits<std::size_t>::max() / sizeof(T)) {
			void* ptr = std::malloc(cnt * sizeof(T));
			if (ptr) {
				return static_cast<T*>(ptr);
			}
		}
		throw std::bad_alloc();
	}

	inline void deallocate(pointer ptr, size_type cnt)
	{
		(void)cnt;
		std::free(ptr);
	}

	inline size_type max_size() const
	{
		return std::numeric_limits<std::size_t>::max() / sizeof(T);
	}

	inline void construct(pointer p, const T& t) { new (p) T(t); }

	inline void destroy(pointer p) { p->~T(); }

#if 0
	inline bool operator==(const TestStringAllocator&) { return true; }
	inline bool operator!=(const TestStringAllocator& a) { return !operator==(a); }
#endif

	template <class U1, class U2>
	friend bool operator==(const TestStringAllocator<U1>&,
	                       const TestStringAllocator<U2>&) noexcept;

	template <class U>
	friend class TestStringAllocator;
};

template <class U1, class U2>
inline bool
operator==(const TestStringAllocator<U1>&,
           const TestStringAllocator<U2>&) noexcept
{
	return true;
}

template <class U1, class U2>
inline bool
operator!=(const TestStringAllocator<U1>& x,
           const TestStringAllocator<U2>& y) noexcept
{
	return !(x == y);
}

using TestString =
    std::basic_string<char, std::char_traits<char>, TestStringAllocator<char>>;

using adt_test_fmt_memory_buffer =
    fmt::basic_memory_buffer<char,
                             fmt::inline_buffer_size,
                             TestStringAllocator<char>>;

TestString
test_vformat(TestStringAllocator<char> alloc,
        fmt::string_view format_str,
        fmt::format_args args)
{
	adt_test_fmt_memory_buffer buf(alloc);
	fmt::vformat_to(buf, format_str, args);
	return TestString(buf.data(), buf.size(), alloc);
}

template <typename... Args>
inline TestString
test_format(TestStringAllocator<char> alloc,
       fmt::string_view format_str,
       const Args&... args)
{
	return test_vformat(alloc, format_str, fmt::make_format_args(args...));
}

#define TEST_FMT(...) test_format(TestStringAllocator<char>(), __VA_ARGS__)

static void
format_string()
{
	size_t bytes = 42;
	const char* thread_name = "Test Thread";

	TestString str = TEST_FMT("bytes : {}, thread_name : {}", bytes, thread_name);
}

#if _MSC_VER
#include <Windows.h>
#include <exception>

static bool
expect_msvc_alloc()
{
	__try
	{
		format_string();
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
		return true;
	}
	return false;
}
#endif

TEST(Fmt, FormatNoAlloc)
{
	// Disable global allocation so std::bad_alloc is thrown upon allocation.
	GlobalAllocator::set_enabled(false);

#if _MSC_VER

// FMT_VERSION was added for 5.X and allocation is not expected.
#ifdef FMT_VERSION
	ASSERT_FALSE(expect_msvc_alloc());
#else
	// fmt version 4.X uses a std::vector that allocates when formatting
	// when using msvc compiler/C++ library. This has changed in 5.X.
	ASSERT_TRUE(expect_msvc_alloc());
#endif

#else
	// Allocation isn't expected on any other platforms ATM
	format_string();
#endif

	GlobalAllocator::set_enabled(true);
}
