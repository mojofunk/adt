#include "adt-private.hpp"

class TestSink : public adt::Sink, public adt::RunLoop
{
public: // Ctors
	TestSink();

	~TestSink();

public: // Sink interface
	void handle_record_async(const std::shared_ptr<adt::Record>& record) override;

	void handle_record_sync(const std::shared_ptr<adt::Record>& record) override;

private: // RunLoop interface
	void on_iteration() override;

public: // Methods
	bool get_enabled() const override { return m_enabled; }

	void set_enabled(bool enable = true) override;

	bool is_internal_thread(std::thread::id thread_id) override;

public: // Testing Data/Methods
	std::vector<std::shared_ptr<adt::Record>> m_records;

private: // Methods
	void process_records();
	void clear_record_queue();

private: // Data
	std::thread m_record_handling_thread;

	std::atomic<bool> m_enabled;

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4324) // structure was padded due to __declspec(align())
#endif

	using RecordQueueType =
	    moodycamel::ReaderWriterQueue<std::shared_ptr<adt::Record>>;
	RecordQueueType m_record_queue;

#ifdef _MSC_VER
#pragma warning(pop)
#endif
};
