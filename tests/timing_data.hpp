struct TimingData {
	uint32_t count;
	uint64_t min_duration;
	uint64_t max_duration;
	uint64_t total_duration;

	TimingData() { reset(); }

	void reset()
	{
		count = 0;
		min_duration = std::numeric_limits<uint64_t>::max();
		max_duration = 0;
		total_duration = 0;
	}

	void add_duration(uint64_t duration)
	{
		count++;
		total_duration += duration;
		min_duration = std::min(min_duration, duration);
		max_duration = std::max(max_duration, duration);
	}

	uint64_t mean_avg() const
	{
		assert(count);
		return total_duration / count;
	}
};

struct TimedDuration {
	TimedDuration(TimingData& td)
	    : start(TimestampSource::get_microseconds())
	    , timing_data(td)
	{
	}

	~TimedDuration()
	{
		uint64_t end = TimestampSource::get_microseconds();

		uint64_t duration = end > start ? end - start : 0;

		timing_data.add_duration(duration);
	}

	uint64_t start;
	TimingData& timing_data;
};
