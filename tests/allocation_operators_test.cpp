#include "adt_test_includes.hpp"

class OperatorNewTest : public adt::Test
{
public:
	bool run() override
	{
		const size_t alloc_count =
		    static_cast<size_t>(TestUtils::get_random_int_in_range(32, 64));

		auto const random_values =
		    TestUtils::get_random_allocation_sizes(alloc_count);

		std::vector<int*> allocations;
		allocations.reserve(alloc_count);

		TestUtils::NoLeaks no_leaks;

		for (auto const random_value : random_values) {
			auto ptr = new int{ static_cast<int>(random_value) };
			allocations.push_back(ptr);
		}

		for (auto& alloc : allocations) {
			delete alloc;
		}

		return no_leaks.test_passed();
	}
};

class OperatorNewArrayTest : public adt::Test
{
public:
	bool run() override
	{
		const size_t alloc_count =
		    static_cast<size_t>(TestUtils::get_random_int_in_range(32, 64));

		auto const random_sizes = TestUtils::get_random_allocation_sizes(alloc_count);

		std::vector<int*> allocations;
		allocations.reserve(alloc_count);

		TestUtils::NoLeaks no_leaks;

		for (auto const random_size : random_sizes) {
			auto ptr = new int[random_size];
			allocations.push_back(ptr);
		}

		for (auto& alloc : allocations) {
			delete[] alloc;
		}

		return no_leaks.test_passed();
	}
};

TEST(AllocationOperators, OperatorNewTest)
{
	ASSERT_TRUE(OperatorNewTest().run());
}

TEST(AllocationOperators, OperatorNewArrayTest)
{
	ASSERT_TRUE(OperatorNewArrayTest().run());
}

TEST(AllocationOperators, Concurrency)
{
	ConcurrencyTest test;

	test.add_default_count<OperatorNewTest>();
	test.add_default_count<OperatorNewArrayTest>();

	test.set_runtime(std::chrono::seconds(2));

	ASSERT_TRUE(test.run());
}
