#include "adt_test_includes.hpp"

A_DEFINE_LOG_CATEGORY(TestProperties, "TestProperties")

class PropertiesTest
{
public:
	static void test_log_call_1()
	{
		int ten = 10;
		A_LOG_CALL1(TestProperties, ten);
	}

	static void test_variant()
	{
		std::unique_ptr<int> int_value(new int);

		LogVariant rt_pointer_variant(int_value.get());

		ASSERT_EQ(rt_pointer_variant.type, VariantType::POINTER);

		A_LOG_DATA1(TestProperties, int_value.get());

		bool bool_value = false;

		LogVariant rt_bool_variant(bool_value);

		ASSERT_EQ(rt_bool_variant.type, VariantType::BOOL);

		A_LOG_DATA2(TestProperties, int_value.get(), bool_value);

		float float_value = 0.0f;

		LogVariant rt_float_variant(float_value);

		ASSERT_EQ(rt_float_variant.type, VariantType::FLOAT);

		A_LOG_DATA3(TestProperties, int_value.get(), bool_value, float_value);
	}
};

TEST(testProperties, testLogCall)
{
	PropertiesTest::test_log_call_1();
}

TEST(testProperties, testVariant)
{
	PropertiesTest::test_variant();
}
