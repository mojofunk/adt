#include "adt_test_includes.hpp"

#include "timing_data.hpp"

static size_t alloc_perf_max_pool_size = 2048;

class AllocTestClass
{
public:
	AllocTestClass(uint16_t offset = 1)
	    : m_array{ nullptr }
	    , m_offset{ offset }
	{
		(void)m_array;
		(void)m_offset;
		(void)m_count;
	}

private:
	static const int s_array_size = 62;

	std::array<void*, s_array_size> m_array;

	uint16_t m_offset;
	size_t m_count{0};
};

class STVectorTestClass : public AllocTestClass
{
public:
	STVectorTestClass(uint16_t offset = 2)
	    : AllocTestClass(offset)
	{
	}

public: // class new/delete
	void* operator new(std::size_t size);

	void operator delete(void* ptr);
};

STVectorMemoryPool&
get_thread_local_st_vector_pool()
{
	thread_local STVectorMemoryPool pool(sizeof(STVectorTestClass),
	                                     alloc_perf_max_pool_size);
	return pool;
}

void*
STVectorTestClass::operator new(std::size_t size)
{
	return get_thread_local_st_vector_pool().allocate(size);
}

void
STVectorTestClass::operator delete(void* ptr)
{
	get_thread_local_st_vector_pool().deallocate(ptr);
}

class LogTestClass : public AllocTestClass
{
public:
	LogTestClass(uint16_t offset = 2)
	    : AllocTestClass(offset)
	{
	}

public: // class new/delete
	void* operator new(std::size_t size);

	void operator delete(void* ptr);
};

void*
LogTestClass::operator new(std::size_t size)
{
	return LogAllocator::allocate(size);
}

void
LogTestClass::operator delete(void* ptr)
{
	LogAllocator::deallocate(ptr, sizeof(LogTestClass));
}

class STFSQueueTestClass : public AllocTestClass
{
public:
	STFSQueueTestClass(uint16_t offset = 2)
	    : AllocTestClass(offset)
	{
	}

public: // class new/delete
	void* operator new(std::size_t size);

	void operator delete(void* ptr);
};

FixedSizeQueueMemoryPool&
get_thread_local_st_fs_queue_pool()
{
	thread_local FixedSizeQueueMemoryPool pool(sizeof(STFSQueueTestClass),
	                                           alloc_perf_max_pool_size);
	return pool;
}

void*
STFSQueueTestClass::operator new(std::size_t size)
{
	return get_thread_local_st_fs_queue_pool().allocate(size);
}

void
STFSQueueTestClass::operator delete(void* ptr)
{
	get_thread_local_st_fs_queue_pool().deallocate(ptr);
}

class MTMemoryPoolTestClass : public AllocTestClass
{
public:
	MTMemoryPoolTestClass(uint16_t offset = 2)
	    : AllocTestClass(offset)
	{
	}

public: // class new/delete
	void* operator new(std::size_t size);

	void operator delete(void* ptr);
};

MTMemoryPool&
get_thread_local_mt_memory_pool()
{
	static MTMemoryPool pool(sizeof(MTMemoryPoolTestClass),
	                         alloc_perf_max_pool_size);
	return pool;
}

void*
MTMemoryPoolTestClass::operator new(std::size_t size)
{
	return get_thread_local_mt_memory_pool().allocate(size);
}

void
MTMemoryPoolTestClass::operator delete(void* ptr)
{
	get_thread_local_mt_memory_pool().deallocate(ptr);
}

template <class T>
void
test_class_alloc_template(uint32_t instance_count)
{
	std::vector<T*> class_ptr_vec;
	class_ptr_vec.reserve(instance_count);

	for (uint32_t i = 0; i < instance_count; ++i) {
		T* instance = new T();
		class_ptr_vec.push_back(instance);
	}

	for (auto const& instance : class_ptr_vec) {
		delete instance;
	}
}

static void
print_timing(uint32_t iterations,
             uint32_t instance_count,
             TimingData const& td,
             const std::string& class_name)
{
	std::cerr << class_name << " Allocations : " << iterations * instance_count
	          << " Mean average = " << td.mean_avg()
	          << "us, Min duration = " << td.min_duration
	          << "us, Max duration = " << td.max_duration << "us" << std::endl;
}

template <class T>
void
test_alloc_perf_iteration_template(uint32_t iterations,
                                   uint32_t instance_count,
                                   TimingData& timing_data)
{
	uint64_t start = TimestampSource::get_microseconds();

	for (uint32_t i = 0; i < iterations; ++i) {
		test_class_alloc_template<T>(instance_count);
	}

	uint64_t end = TimestampSource::get_microseconds();

	uint64_t duration = end > start ? end - start : 0;

	timing_data.add_duration(duration);
}

static void
test_alloc_perf_iterations(uint32_t iterations, uint32_t instance_count)
{
	std::cerr << std::endl;

	TimingData td_test, td_st, td_st2, td_rt_test, td_fs, td_mt_pool;

	for (uint32_t i = 0; i < iterations; ++i) {
		test_alloc_perf_iteration_template<AllocTestClass>(
		    iterations, instance_count, td_test);
		test_alloc_perf_iteration_template<STVectorTestClass>(
		    iterations, instance_count, td_st);
		test_alloc_perf_iteration_template<LogTestClass>(
		    iterations, instance_count, td_rt_test);
		test_alloc_perf_iteration_template<STFSQueueTestClass>(
		    iterations, instance_count, td_fs);
		test_alloc_perf_iteration_template<MTMemoryPoolTestClass>(
		    iterations, instance_count, td_mt_pool);
	}

	for (uint32_t i = 0; i < iterations; ++i) {
		test_alloc_perf_iteration_template<LogTestClass>(
		    iterations, instance_count, td_rt_test);
		test_alloc_perf_iteration_template<MTMemoryPoolTestClass>(
		    iterations, instance_count, td_mt_pool);
		test_alloc_perf_iteration_template<STFSQueueTestClass>(
		    iterations, instance_count, td_fs);
		test_alloc_perf_iteration_template<STVectorTestClass>(
		    iterations, instance_count, td_st);
		test_alloc_perf_iteration_template<AllocTestClass>(
		    iterations, instance_count, td_test);
	}

	for (uint32_t i = 0; i < iterations; ++i) {
		test_alloc_perf_iteration_template<STVectorTestClass>(
		    iterations, instance_count, td_st);
		test_alloc_perf_iteration_template<STFSQueueTestClass>(
		    iterations, instance_count, td_fs);
		test_alloc_perf_iteration_template<MTMemoryPoolTestClass>(
		    iterations, instance_count, td_mt_pool);
		test_alloc_perf_iteration_template<LogTestClass>(
		    iterations, instance_count, td_rt_test);
		test_alloc_perf_iteration_template<AllocTestClass>(
		    iterations, instance_count, td_test);
	}

	print_timing(iterations, instance_count, td_test, "AllocTestClass");
	print_timing(iterations, instance_count, td_st, "STVectorTestClass");
	print_timing(iterations, instance_count, td_rt_test, "LogTestClass");
	print_timing(iterations, instance_count, td_fs, "STFSTestClass");
	print_timing(iterations, instance_count, td_mt_pool, "MTMemoryPoolTestClass");
}

static std::atomic<bool> done(false);

void
test_allocation_thread()
{
	while (!done) {
		size_t alloc_size = TestUtils::get_random_allocation_size();
		int sleep_time = TestUtils::get_random_sleep_time_us();

		char* alloc_ptr = (char*)std::malloc(alloc_size);

		for (auto i = 0ull; i < alloc_size; ++i) {
			*alloc_ptr = static_cast<char>(TestUtils::get_random_uint8());
		}

		std::this_thread::sleep_for(std::chrono::microseconds(sleep_time));

		std::free(alloc_ptr);
	}
}

static void
test_alloc_perf()
{
	test_alloc_perf_iterations(32, 16);
	test_alloc_perf_iterations(32, 64);
	test_alloc_perf_iterations(32, 256);

	test_alloc_perf_iterations(128, 64);
}

TEST(AllocPerf, Concurrent)
{
	LogAllocator::initialize();

	std::vector<std::thread> allocator_threads;
	std::vector<std::thread> test_threads;

	const size_t num_allocators = std::thread::hardware_concurrency() * 2ull;
	const size_t num_test_threads = std::thread::hardware_concurrency();

	for (auto i = 0ull; i != num_allocators; ++i) {
		allocator_threads.emplace_back(std::thread(test_allocation_thread));
	}

	for (auto i = 0ull; i != num_test_threads; ++i) {
		test_threads.emplace_back(std::thread(test_alloc_perf));
	}

	done = true;

	for (auto& t : allocator_threads) {
		t.join();
	}

	for (auto& t : test_threads) {
		t.join();
	}
}

TEST(AllocPerf, SingleThread)
{
	LogAllocator::initialize();

	test_alloc_perf();
}
