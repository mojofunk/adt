#include "adt_test_includes.hpp"

TEST(Init, InitCleanup)
{
	/**
	 * A LogString cannot be used before the LogAllocator Singleton is initialized.
	 * Removing the comment should cause a crash.
	 */
	//LogString log_str("InitCleanup");

	adt::init();

	{
		LogString log_str("InitCleanup");
	}

	adt::cleanup();
}
