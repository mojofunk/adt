#include "new_allocator.hpp"

#include "adt-private.hpp"

class NewAllocatorPrivate;

class ThreadLocalAllocator
{
public:
	ThreadLocalAllocator(NewAllocatorPrivate* parent);

	~ThreadLocalAllocator();

	void* allocate(std::size_t bytes);

	void deallocate(void* ptr, std::size_t bytes);

private:
	NewAllocatorPrivate* m_parent;
};

ThreadLocalAllocator::ThreadLocalAllocator(NewAllocatorPrivate* parent)
    : m_parent(parent)
{
}

ThreadLocalAllocator::~ThreadLocalAllocator()
{
	// m_parent
}

void*
ThreadLocalAllocator::allocate(std::size_t bytes)
{
	void* ptr = nullptr;

	return ptr;
}

void
ThreadLocalAllocator::deallocate(void* ptr, std::size_t bytes)
{
}

class NewAllocatorPrivate : public RunLoop
{
public: // ctors
	NewAllocatorPrivate();

	A_SINGLETON(NewAllocatorPrivate);

	// cached instance pointer
	static NewAllocatorPrivate* s_instance;

public:
	static std::shared_ptr<ThreadLocalAllocator>& get_thread_local_allocator();

	static void set_thread_local_allocator(
	    std::shared_ptr<ThreadLocalAllocator> const& allocator);

	void* allocate(std::size_t bytes);

	void deallocate(void* ptr, std::size_t bytes);

	static ThreadLocalDataRegistry<std::shared_ptr<ThreadLocalAllocator>>&
	get_thread_allocator_registry();

	void add_block_size(std::size_t bytes, uint32_t count);

private: // RunLoop
	void do_work() override;

	void process_thread_allocators();

private:
	static void on_thread_exit(std::thread::id);

private: // global pool data
	using SizeCountMap = std::map<std::size_t, uint32_t>;

	SizeCountMap m_size_count_map;

	/**
	 * Access to the size count map is from any thread that calls either
	 * register_thread() or add_block_size
	 */
	std::mutex m_size_count_map_mutex;
};

// Move into NewAllocatorFunction without function call?
std::shared_ptr<ThreadLocalAllocator>&
NewAllocatorPrivate::get_thread_local_allocator()
{
	thread_local std::shared_ptr<ThreadLocalAllocator> allocator;
	return allocator;
}

// Move into NewAllocatorFunction without function call?
ThreadLocalDataRegistry<std::shared_ptr<ThreadLocalAllocator>>&
NewAllocatorPrivate::get_thread_allocator_registry()
{
	static ThreadLocalDataRegistry<std::shared_ptr<ThreadLocalAllocator>>
	    s_registry;
	return s_registry;
}

void
NewAllocatorPrivate::set_thread_local_allocator(
    std::shared_ptr<ThreadLocalAllocator> const& allocator)
{
	get_thread_local_allocator() = allocator;

	get_thread_allocator_registry().register_thread_data(allocator);

	AtThreadExitManager::on_thread_exit(
	    std::bind(&on_thread_exit, std::this_thread::get_id()));
}

void
NewAllocatorPrivate::on_thread_exit(std::thread::id id)
{
	// remove all blocks from the ThreadLocalAllocator for this thread and
	// add them back to the global pool

	get_thread_allocator_registry().unregister_thread_data();
}

void*
NewAllocatorPrivate::allocate(std::size_t bytes)
{
	void* ptr = nullptr;

	if (!get_thread_local_allocator()) {
		get_thread_local_allocator() = std::make_shared<ThreadLocalAllocator>(this);
	}
	// Lookup
	return ptr;
}

void
NewAllocatorPrivate::deallocate(void* ptr, std::size_t bytes)
{
	if (!get_thread_local_allocator()) {
		get_thread_local_allocator() = std::make_shared<ThreadLocalAllocator>(this);
	}
}

void
NewAllocatorPrivate::add_block_size(std::size_t bytes, uint32_t count)
{
}

NewAllocatorPrivate* NewAllocatorPrivate::s_instance = nullptr;

NewAllocatorPrivate::NewAllocatorPrivate()
{
	assert(s_instance == nullptr);
	// cache pointer for optimization
	s_instance = this;
}

// Singleton interface
NewAllocatorPrivate*
NewAllocatorPrivate::get_instance()
{
	return Singleton<NewAllocatorPrivate>::get();
}

void
NewAllocatorPrivate::do_work()
{
	process_thread_allocators();
}

void
NewAllocatorPrivate::process_thread_allocators()
{

#if 0
	RTLogRecord* record = nullptr;

	while (m_record_queue.try_dequeue(record)) {
		std::lock_guard<std::mutex> lock(m_sinks_mutex);
		for (auto sink : m_sinks) {
			sink->handle_record(*record);
		}
		delete record;
	}
#endif
}

void
NewAllocator::initialize()
{
	NewAllocatorPrivate::get_instance();
}

void*
NewAllocator::allocate(std::size_t bytes)
{
	return NewAllocatorPrivate::s_instance->allocate(bytes);
}

void
NewAllocator::deallocate(void* ptr, std::size_t bytes)
{
	NewAllocatorPrivate::s_instance->deallocate(ptr, bytes);
}

void
NewAllocator::add_block_size(std::size_t bytes, uint32_t count)
{
	NewAllocatorPrivate::s_instance->add_block_size(bytes, count);
}
