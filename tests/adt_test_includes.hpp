#ifndef ADT_TEST_INCLUDES_H
#define ADT_TEST_INCLUDES_H

#include <gtest/gtest.h>

#include <adt-private.hpp>

#include <adt-test.hpp>

using namespace adt;

#endif // ADT_TEST_INCLUDES_H
