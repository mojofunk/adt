#include "adt_test_includes.hpp"

TEST(StackTrace, New)
{
	std::unique_ptr<adt::StackTrace> trace_ptr(new adt::StackTrace);
	ASSERT_TRUE(trace_ptr->to_string().find("New") != std::string::npos);
}

TEST(StackTrace, ToStream)
{
	std::ostringstream oss;

	adt::StackTrace trace;
	trace.to_stream(oss);
	std::string str = oss.str();

	ASSERT_TRUE(str.find("Stream") != std::string::npos);
}

TEST(StackTrace, ToString)
{
	adt::StackTrace trace;
	std::string st_string(trace.to_string());
	std::size_t found = st_string.find("ToString");
	ASSERT_TRUE(found != std::string::npos);
}

TEST(StackTrace, OperatorEquals)
{
	adt::StackTrace traces[2];
	ASSERT_TRUE(traces[0] == traces[1]);
}

TEST(StackTrace, GetAddresses)
{
	adt::StackTrace trace;
	size_t count = 0;
	void* const* const function_addresses = trace.get_addresses(count);

	ASSERT_TRUE(count != 0);
	ASSERT_TRUE(function_addresses != NULL);

	ASSERT_TRUE(trace.to_string().find("GetAddresses") != std::string::npos);
}

TEST(StackTrace, GetStackFrames)
{
	adt::StackTrace trace;
	auto stack_frames = trace.get_stack_frames();

	ASSERT_TRUE(!stack_frames.empty());

	auto const top_stack_frame = stack_frames[0];

	ASSERT_TRUE(top_stack_frame->has_function_name());

	auto const& top_stack_function_name = top_stack_frame->get_function_name();
	std::size_t found = top_stack_function_name.find("GetStackFrames");
	ASSERT_TRUE(found != std::string::npos);
}
