#include "adt.hpp" // to remove

using namespace adt; // to remove

class ADT_API NewAllocator
{
public: // API
	// must be called before using allocation functions
	static void initialize();

	static void* allocate(std::size_t bytes);

	static void deallocate(void* ptr, std::size_t bytes);

	static void add_block_size(std::size_t bytes, uint32_t count);

private: // Ctor
	NewAllocator();
};
