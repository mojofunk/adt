#ifdef USE_INTERNAL_FMT
#include "fmt/format.cc"
#include "fmt/ostream.cc"
#include "fmt/posix.cc"
#endif

#include "adt/function_utils.cpp"

#include "adt/stack_trace.cpp"

#include "adt/allocator_registry.cpp"

#include "adt/global_allocator.cpp"

#include "adt/log_allocator.cpp"

#include "adt/threads.cpp"

#include "adt/timestamp_source.cpp"

#include "adt/timing.cpp"

#include "adt/variant_type.cpp"

#include "adt/log_record.cpp"

#include "adt/record_type.cpp"

#include "adt/record.cpp"

#include "adt/record_queue.cpp"

#include "adt/log.cpp"

#include "adt/format.cpp"

#include "adt/message_logger.cpp"
#include "adt/duration_logger.cpp"
#include "adt/function_logger.cpp"
#include "adt/data_logger.cpp"
#include "adt/allocation_logger.cpp"

#include "adt/ostream_sink.cpp"

#include "adt/trace_event_sink.cpp"

#include "adt/class_tracker_logger.cpp"
#include "adt/class_tracker_manager.cpp"

#include "adt/csv_utils.cpp"

#include "adt/init.cpp"

// avoid compiler warnings
#undef A_DECLARE_LOG_CATEGORY
#undef A_DEFINE_LOG_CATEGORY

#include "adt-capi.cpp"

#include "adt-private.cpp"
