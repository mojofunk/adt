#pragma once

#include <adt-capi.h>

typedef struct logger {
	log_record_t* log_record;
	int properties_enabled;
	// int sync_enabled;
	// int trace_enabled;
} logger_t;
