#include "adt-private.hpp"
#include "adt-capi.h"

extern "C" {

//using namespace adt;

log_category_t*
adt_log_get_category(const char* category_name)
{
	return reinterpret_cast<log_category_t*>(
	    adt::Log::make_category(category_name));
}

}
