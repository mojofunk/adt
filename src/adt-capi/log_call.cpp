#include "adt-private.hpp"
#include "adt-capi.h"

extern "C" {

using namespace adt;

logger_t
adt_log_call_begin(log_category_t* log_cat,
                   int line,
                   const char* file_path,
                   const char* function_str)
{
	logger_t logr;
	logr.log_record = nullptr;
	logr.properties_enabled = (int)Log::get_properties_enabled();

	auto category = reinterpret_cast<LogCategory*>(log_cat);

	if (!category->enabled || !category->function_type_enabled) {
		return logr;
	}

	LogRecord* log_record = new LogRecord(
	    category,
	    RecordType::FUNCTION_CALL,
	    Timing(Timing::Type::DURATION, TimestampSource::get_microseconds()),
	    line,
	    file_path,
	    function_str);

	logr.log_record = reinterpret_cast<log_record_t*>(log_record);

	return logr;
}

void
adt_log_call_end(logger_t* logr)
{
	if (logr->log_record == nullptr) {
		return;
	}

	auto log_record = reinterpret_cast<LogRecord*>(logr->log_record);

	log_record->timing.second = TimestampSource::get_microseconds();
	LogPrivate::s_instance->write_record_async(log_record);

	// prevent potential pointer reuse
	logr->log_record = nullptr;
}

void
adt_log_add_string_property(logger_t* logr, const char* name, const char* value)
{
	assert(logr && logr->log_record);

	auto log_record = reinterpret_cast<LogRecord*>(logr->log_record);

	if (log_record->property_1 == nullptr) {
		log_record->copy_to_property_1<const char*>(name, value);
		return;
	}
	if (log_record->property_2 == nullptr) {
		log_record->copy_to_property_2<const char*>(name, value);
		return;
	}
	if (log_record->property_3 == nullptr) {
		log_record->copy_to_property_3<const char*>(name, value);
		return;
	}
	if (log_record->property_4 == nullptr) {
		log_record->copy_to_property_4<const char*>(name, value);
		return;
	}
	if (log_record->property_5 == nullptr) {
		log_record->copy_to_property_5<const char*>(name, value);
		return;
	}
	if (log_record->property_6 == nullptr) {
		log_record->copy_to_property_6<const char*>(name, value);
		return;
	}
	if (log_record->property_7 == nullptr) {
		log_record->copy_to_property_7<const char*>(name, value);
		return;
	}
	if (log_record->property_8 == nullptr) {
		log_record->copy_to_property_8<const char*>(name, value);
		return;
	}
	// what to do here...silently ignore? overwrite properties? abort?
	assert(false);
}

} // extern "C"
