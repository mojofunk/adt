#pragma once

#include <adt-capi.h>

#ifdef A_DECLARE_LOG_CATEGORY
#undef A_DECLARE_LOG_CATEGORY
#endif

#define A_DECLARE_LOG_CATEGORY(VariableName) extern log_category_t* VariableName

#ifdef A_DEFINE_LOG_CATEGORY
#undef A_DEFINE_LOG_CATEGORY
#endif

#define A_DEFINE_LOG_CATEGORY(VariableName, LogCategoryName)                   \
	log_category_t* VariableName = adt_log_get_category(LogCategoryName)

#define A_LOG_CALL_BEGIN(LogCategoryTPtr)                                      \
	logger_t call_logger =                                                        \
	    adt_log_call_begin(LogCategoryTPtr, __LINE__, __FILE__, A_STRFUNC)

#define A_LOG_CALL_END() adt_log_call_end(&call_logger)

#define A_LOG_CALL_ADD_STRING_PROPERTY(Value)                                  \
	if (call_logger.log_record && call_logger.properties_enabled) {               \
		adt_log_add_string_property(&call_logger, A_STRINGIFY(Value), Value);        \
	}

