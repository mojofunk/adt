#pragma once

#include <adt-capi.h>

ADT_API logger_t
adt_log_call_begin(log_category_t*,
                   int line,
                   const char* file_path,
                   const char* function_str);

ADT_API void
adt_log_call_end(logger_t* call_logger);

ADT_API void
adt_log_add_string_property(logger_t* call_logger,
                            const char* name,
                            const char* str);

