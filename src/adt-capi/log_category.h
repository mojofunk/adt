#pragma once

#include <adt-capi.h>

typedef void log_category_t;

ADT_API log_category_t*
adt_log_get_category(const char* category_name);
