#pragma once

#include <adt-test.hpp>

namespace adt {

class TestUtils
{
public: // ctors
	/**
	 * Create TestUtils Singleton
	 */
	TestUtils();

	/**
	 * Destroy TestUtils Singleton
	 */
	~TestUtils();

public: // Random values

	static bool get_random_bool();

	static int8_t get_random_int8();

	static uint8_t get_random_uint8();

	static int16_t get_random_int16();

	static uint16_t get_random_uint16();

	static int32_t get_random_int32();

	static uint32_t get_random_uint32();

	static int64_t get_random_int64();

	static uint64_t get_random_uint64();

	static float get_random_float();

	static double get_random_double();

public: // random
	static int get_random_int_in_range (int min, int max);

	// @return random allocation size between 16 and 2048
	static int get_random_int();

	// @return random allocation size between 16 and 2048
	static size_t get_random_allocation_size();

	static std::vector<size_t> get_random_allocation_sizes(size_t count)
	{
		return get_random_data<size_t>(count, get_random_allocation_size);
	}

	// @return random int between 64 and 512
	static int get_random_sleep_time_us();

	// @return random int between 4 and 32
	static int get_random_call_depth();

	template <class DataType>
	static std::vector<DataType> get_random_data(size_t count,
	                                      std::function<DataType()> func)
	{
		std::vector<DataType> data_vec;
		data_vec.reserve(count);

		for (size_t i = 0ull; i < count; ++i) {
			data_vec.push_back(func());
		}
		return data_vec;
	}

public: // classes
	/**
	 * Throw a std::runtime_error exception if any heap allocations made
	 * using ::new() in the current thread during the lifetime of this
	 * instance that are not deallocated using ::delete().
	 */
	class NoLeaks
	{
	public:
		NoLeaks();

		bool test_passed() const;
		bool test_failed() const { return !test_passed(); }
	};

	/**
	 * Throw a std::runtime_error if any heap allocations are made using
	 * ::new() in the current thread for the during lifetime of this
	 * instance.
	 */
	class NoNewDelete
	{
	public:
		NoNewDelete();

		bool test_passed() const;
		bool test_failed() const { return !test_passed(); }
	};

private:
	A_DISALLOW_COPY_AND_ASSIGN(TestUtils);
};

} // namespace adt
