#include "adt-test/source_includes.cpp"

namespace adt {

class TestUtilsPrivate
{
public: // ctors
	static TestUtilsPrivate* get_instance()
	{
		static auto s_instance = new TestUtilsPrivate();
		return s_instance;
	}

	~TestUtilsPrivate()
	{
		GlobalAllocatorPrivate::get_instance().reset_allocator();
	}

private: //ctors
	TestUtilsPrivate()
	    : rng(rd())
	{
		GlobalAllocatorPrivate::get_instance().set_allocator(
		    std::shared_ptr<adt::Allocator>(new AdtTrackingAllocator));
	}

public:
	static bool get_random_bool()
	{
		std::uniform_int_distribution<int16_t> distribution{ 0, 1 };
#ifdef _MSC_VER
#pragma warning (push)
// forcing value to bool 'true' or 'false' (performance warning)
#pragma warning (disable:4800)
		return static_cast<bool>(distribution(get_instance()->rng));
#pragma warning (pop)
#else
		return static_cast<bool>(distribution(get_instance()->rng));
#endif
	}

	static int8_t get_random_int8()
	{
		std::uniform_int_distribution<int16_t> distribution{
			0, std::numeric_limits<int8_t>::max()
		};
		return static_cast<int8_t>(distribution(get_instance()->rng));
	}

	static uint8_t get_random_uint8()
	{
		std::uniform_int_distribution<int16_t> distribution{
			0, std::numeric_limits<uint8_t>::max()
		};
		return static_cast<uint8_t>(distribution(get_instance()->rng));
	}

	static int16_t get_random_int16()
	{
		std::uniform_int_distribution<int16_t> distribution;
		return distribution(get_instance()->rng);
	}

	static uint16_t get_random_uint16()
	{
		std::uniform_int_distribution<uint16_t> distribution;
		return distribution(get_instance()->rng);
	}


	static int32_t get_random_int32()
	{
		std::uniform_int_distribution<int32_t> distribution;
		return distribution(get_instance()->rng);
	}

	static uint32_t get_random_uint32()
	{
		std::uniform_int_distribution<uint32_t> distribution;
		return distribution(get_instance()->rng);
	}

	static int64_t get_random_int64()
	{
		std::uniform_int_distribution<int64_t> distribution;
		return distribution(get_instance()->rng);
	}

	static uint64_t get_random_uint64()
	{
		std::uniform_int_distribution<uint64_t> distribution;
		return distribution(get_instance()->rng);
	}

	static float get_random_float()
	{
		std::uniform_real_distribution<float> distribution;
		return distribution(get_instance()->rng);
	}

	static double get_random_double()
	{
		std::uniform_real_distribution<double> distribution;
		return distribution(get_instance()->rng);
	}

public:
	static int get_random_int_range(int min, int max)
	{
		std::uniform_int_distribution<int> int_distribution(min, max);
		return int_distribution(get_instance()->rng);
	}

private: // member variables
	std::random_device rd;
	std::mt19937 rng;
};

TestUtils::TestUtils()
{
	TestUtilsPrivate::get_instance();
}

TestUtils::~TestUtils()
{
	delete TestUtilsPrivate::get_instance();
}

TestUtils::NoLeaks::NoLeaks()
{
	TrackingAllocator::start_tracking_this_thread();
}

bool
TestUtils::NoLeaks::test_passed() const
{
	auto thread_alloc_data = TrackingAllocator::stop_tracking_this_thread();
	return thread_alloc_data->no_leaks();
}

TestUtils::NoNewDelete::NoNewDelete()
{
	TrackingAllocator::start_tracking_this_thread();
}

bool
TestUtils::NoNewDelete::test_passed() const
{
	auto thread_alloc_data = TrackingAllocator::stop_tracking_this_thread();
	return thread_alloc_data->constant_heap_usage();
}

bool
TestUtils::get_random_bool()
{
	return TestUtilsPrivate::get_random_bool();
}

int8_t
TestUtils::get_random_int8()
{
	return TestUtilsPrivate::get_random_int8();
}

uint8_t
TestUtils::get_random_uint8()
{
	return TestUtilsPrivate::get_random_uint8();
}

int16_t
TestUtils::get_random_int16()
{
	return TestUtilsPrivate::get_random_int16();
}

uint16_t
TestUtils::get_random_uint16()
{
	return TestUtilsPrivate::get_random_uint16();
}

int32_t
TestUtils::get_random_int32()
{
	return TestUtilsPrivate::get_random_int32();
}

uint32_t
TestUtils::get_random_uint32()
{
	return TestUtilsPrivate::get_random_uint32();
}

int64_t
TestUtils::get_random_int64()
{
	return TestUtilsPrivate::get_random_int64();
}

uint64_t
TestUtils::get_random_uint64()
{
	return TestUtilsPrivate::get_random_uint64();
}

float
TestUtils::get_random_float()
{
	return TestUtilsPrivate::get_random_float();
}

double
TestUtils::get_random_double()
{
	return TestUtilsPrivate::get_random_double();
}

int
TestUtils::get_random_int_in_range(int min, int max)
{
	return TestUtilsPrivate::get_random_int_range(min, max);
}

size_t
TestUtils::get_random_allocation_size()
{
	return static_cast<size_t>(TestUtilsPrivate::get_random_int_range(16, 2048));
}

int
TestUtils::get_random_sleep_time_us()
{
	return TestUtilsPrivate::get_random_int_range(64, 512);
}

int
TestUtils::get_random_call_depth()
{
	return TestUtilsPrivate::get_random_int_range(4, 32);
}

} // namespace adt
