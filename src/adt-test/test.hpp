#pragma once

#include <adt-test.hpp>

namespace adt {

class Test
{
public:
	virtual ~Test();

public:
	/**
	 * Run test, blocking.
	 * @return true if test passed
	 */
	virtual bool run() = 0;
};

} // namespace adt
