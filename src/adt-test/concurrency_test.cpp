#include "adt-test/source_includes.cpp"

namespace adt {

class AsyncTest
{
public:
	AsyncTest(std::shared_ptr<Test> test)
		: m_test(test)
	{
		auto lambda = [this]() { return run_async(); };
		std::packaged_task<bool()> task{ lambda };
		m_future = task.get_future();
		m_thread = std::thread(std::move(task));
	}

	bool get_result() {
		m_stop = true;
		m_thread.join();
		return m_future.get();
	}

private:
	bool run_async()
	{
		bool passed = true;
		while (!m_stop) {
			if (!m_test->run()) {
				passed = false;
				break;
			}
		}
		return passed;
	}

private:
	std::shared_ptr<Test> m_test;

	std::thread m_thread;

	std::future<bool> m_future;

	std::atomic<bool> m_stop{ false };
};

bool
ConcurrencyTest::run_tests_concurrently()
{
	bool passed = true;
	std::vector<std::shared_ptr<AsyncTest>> async_tests;

	async_tests.reserve(m_tests.size());

	for (auto const& test : m_tests) {
		auto async_test = std::make_shared<AsyncTest>(test);
		async_tests.emplace_back(async_test);
	}

	std::this_thread::sleep_for(m_runtime_usecs);

	for (auto const& async_test : async_tests) {
		if (!async_test->get_result()) {
			passed = false;
		}
	}
	return passed;
}

} // namespace adt
