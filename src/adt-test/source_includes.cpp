// Keep this at the top to ensure it doesn't depend on any of the
// includes being listed below.
#include "adt-test.hpp"

#include "adt-private.hpp"

#include <future>
#include <random>