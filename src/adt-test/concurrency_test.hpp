#pragma once

#include <adt-test.hpp>

namespace adt {

class ConcurrencyTest : public Test
{
public:
	ConcurrencyTest() = default;

	template <class TestClass>
	void add(std::size_t count)
	{
		for (std::size_t i = 0; i < count; ++i) {
			m_tests.emplace_back(std::make_shared<TestClass>());
		}
	}

	template <class TestClass>
	void add_default_count()
	{
		add<TestClass>(get_default_count());
	}

	template <class Rep, class Period>
	void set_runtime (const std::chrono::duration<Rep, Period>& runtime_duration)
	{
		m_runtime_usecs =
		    std::chrono::duration_cast<std::chrono::microseconds>(runtime_duration);
	}

	bool run() override { return run_tests_concurrently(); }

public:
	static std::size_t get_default_count()
	{
		return static_cast<std::size_t>(std::thread::hardware_concurrency()) * 2;
	}

	static std::chrono::microseconds get_default_runtime()
	{
		return std::chrono::microseconds(1000000);
	}

private:
	bool run_tests_concurrently();

private:
	std::vector<std::shared_ptr<Test>> m_tests;

	std::chrono::microseconds m_runtime_usecs { get_default_runtime() };
};

} // namespace adt
