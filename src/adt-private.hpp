#ifndef ADT_PRIVATE_H
#define ADT_PRIVATE_H

#include "moodycamel/concurrentqueue.h"

#include "moodycamel/readerwriterqueue.h"

#include "adt.hpp"

#include "adt/private/header_includes.hpp"

#include "adt/private/debug.hpp"

#include "adt/private/mt_memory_pool.hpp"

#include "adt/private/thread_allocation_data.hpp"

#include "adt/private/global_allocator_p.hpp"

#include "adt/private/tracking_allocator.hpp"

#include "adt/private/adt_tracking_allocator.hpp"

#include "adt/private/free_deleter.hpp"

#include "adt/private/run_loop.hpp"

#include "adt/private/at_thread_exit_manager.hpp"

#include "adt/private/at_exit_manager.hpp"

#include "adt/private/singleton.hpp"

#include "adt/private/log_p.hpp"

#include "adt/private/trace_event_sink_p.hpp"

#include "adt/private/demangle.hpp"
#include "adt/private/demangle_template.hpp"

#include "adt/private/thread_local_data_registry.hpp"

#include "adt/private/path_utils.hpp"

#include "adt/private/class_tracker_manager_p.hpp"

#if defined(__MINGW32__)
#include "adt/private/qpc_timestamp_source.hpp"
#endif

#include "adt/private/st_vector_memory_pool.hpp"
#include "adt/private/fixed_size_queue.hpp"
#include "adt/private/fixed_size_queue_memory_pool.hpp"

#endif // ADT_PRIVATE_H
