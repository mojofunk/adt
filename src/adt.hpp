#ifndef ADT_H
#define ADT_H

#ifdef ADT_ENABLE_NOOP

#include "adt-noop.hpp"

#else

#include "fmt/format.h"

#include "adt/header_includes.hpp"

#include "adt/visibility.h"

#include "adt/strfunc.h"
#include "adt/macros.h"

#include "adt/disallow_copy_and_assign.hpp"
#include "adt/function_utils.hpp"

#include "adt/void_cast.hpp"

#include "adt/source_file_location.hpp"

#include "adt/stack_frame.hpp"
#include "adt/stack_trace.hpp"

#include "adt/std_malloc_allocator.hpp"

#include "adt/thread_priority.hpp"

#include "adt/allocator.hpp"

#include "adt/allocator_registry.hpp"

#include "adt/global_allocator.hpp"

#include "adt/log_allocator.hpp"

#include "adt/log_string_allocator.hpp"
#include "adt/log_string.hpp"

#include "adt/thread_info.hpp"
#include "adt/threads.hpp"
#include "adt/thread_macros.hpp"

#include "adt/timestamp_source.hpp"

#include "adt/timing.hpp"

#include "adt/variant_type.hpp"
#include "adt/variant_template.hpp"

#include "adt/log_variant.hpp"
#include "adt/variant.hpp"

#include "adt/log_category.hpp"

#include "adt/property_template.hpp"

#include "adt/log_property.hpp"

#include "adt/property.hpp"

#include "adt/record_type.hpp"
#include "adt/record_base.hpp"

#include "adt/log_record.hpp"
#include "adt/record.hpp"

#include "adt/record_queue.hpp"

#include "adt/sink.hpp"

#include "adt/filter.hpp"

#include "adt/preset.hpp"

#include "adt/log.hpp"

#include "adt/format.hpp"
#include "adt/logging_macros.hpp"

#include "adt/logger.hpp"
#include "adt/message_logger.hpp"
#include "adt/duration_logger.hpp"
#include "adt/function_logger.hpp"
#include "adt/data_logger.hpp"
#include "adt/allocation_logger.hpp"

#include "adt/file.hpp"
#include "adt/std_file.hpp"

#include "adt/ostream_sink.hpp"

#include "adt/trace_event_sink.hpp"

#include "adt/class_tracker.hpp"
#include "adt/class_tracker_manager.hpp"
#include "adt/class_tracker_logger.hpp"
#include "adt/class_tracker_member.hpp"
#include "adt/scoped_class_tracker.hpp"

#include "adt/class_member_macros.hpp"
#include "adt/class_logging_macros.hpp"

#include "adt/scoped_log_enable.hpp"
#include "adt/scoped_trace_enable.hpp"

#include "adt/csv_options.hpp"
#include "adt/csv_utils.hpp"

#include "adt/init.hpp"

#endif // !ADT_ENABLE_NOOP

#endif // ADT_H
