#include "adt-private.hpp"
#include "adt/private/source_includes.cpp"

#include "adt/private/run_loop.cpp"

#include "adt/private/at_thread_exit_manager.cpp"

#include "adt/private/at_exit_manager.cpp"

#include "adt/private/global_allocator_p.cpp"

#include "adt/private/log_p.cpp"

#include "adt/private/demangle.cpp"

#include "adt/private/trace_event_sink_p.cpp"

#include "adt/private/tracking_allocator.cpp"

#include "adt/private/path_utils.cpp"

#include "adt/private/class_tracker_manager_p.cpp"

#ifdef ADT_ENABLE_CXX_ALLOCATORS
#include "adt/private/allocation_operators.cpp"
#endif
