#pragma once

#include <adt.hpp>

namespace adt {

using LogVariant = VariantTemplate<LogString>;

} // namespace adt
