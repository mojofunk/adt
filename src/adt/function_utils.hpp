#pragma once

#include <adt.hpp>

namespace adt {

// remove the function arguments if any
std::string
short_function_name(const std::string& function_name);

} // namespace adt
