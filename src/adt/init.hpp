#pragma once

#include <adt.hpp>

namespace adt
{

ADT_API void
init();

ADT_API void
cleanup();

} // namespace adt
