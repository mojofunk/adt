#include "adt/private/source_includes.cpp"

namespace adt {

void*
LogRecord::operator new(size_t bytes)
{
	void* ptr = LogAllocator::allocate_log_record_pool(bytes);

	ADT_DEBUG_ALLOCATOR("Allocated LogRecord (%p) bytes (%zu)\n", ptr, bytes);

	return ptr;
}

void
LogRecord::operator delete(void* ptr)
{
	LogAllocator::deallocate_log_record_pool(ptr, sizeof(LogRecord));

	ADT_DEBUG_ALLOCATOR(
	    "Deallocated LogRecord (%p) bytes (%zu)\n", ptr, sizeof(LogRecord));
}

} // namespace adt
