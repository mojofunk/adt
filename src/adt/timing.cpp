#include "adt/private/source_includes.cpp"

namespace adt {

std::string
Timing::type_to_string(Timing::Type const type)
{
	switch (type) {
	case Type::INSTANT:
		return "Instant";
	case Type::BEGIN:
		return "Begin";
	case Type::END:
		return "End";
	case Type::DURATION:
		return "Duration";
	}
	return {};
}

} // namespace adt
