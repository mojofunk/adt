#pragma once

#include <adt.hpp>

namespace adt {

template <class StringType>
class VariantTemplate
{
private:
	// TODO Copy ctor required for StandardVariant but we don't
	// really want it for RTVariant/Property
	// A_DISALLOW_COPY_AND_ASSIGN(VariantTemplate<StringType>);

public:
	// Allow move
	VariantTemplate(VariantTemplate&& other) = default;

	// Allow copy ctor for StandardVariant/Property
	VariantTemplate(VariantTemplate const& other) = default;

	// Allow assignment for StandardVariant/Property
	VariantTemplate& operator=(VariantTemplate const& other) = default;

	// needed?
	template <class T>
	explicit VariantTemplate(T* pointer)
	    : type(VariantType::POINTER)
	    , pointer_value((void*)pointer)
	{
	}

	explicit VariantTemplate(bool value)
	    : type(VariantType::BOOL)
	    , bool_value(value)
	{
	}

	explicit VariantTemplate(float value)
	    : type(VariantType::FLOAT)
	    , float_value(value)
	{
	}

	explicit VariantTemplate(double value)
	    : type(VariantType::DOUBLE)
	    , double_value(value)
	{
	}

	explicit VariantTemplate(int32_t value)
	    : type(VariantType::INT32)
	    , int32_value(value)
	{
	}

	explicit VariantTemplate(uint32_t value)
	    : type(VariantType::UINT32)
	    , uint32_value(value)
	{
	}

	explicit VariantTemplate(int64_t value)
	    : type(VariantType::INT64)
	    , int64_value(value)
	{
	}

	explicit VariantTemplate(uint64_t value)
	    : type(VariantType::UINT64)
	    , uint64_value(value)
	{
	}

	explicit VariantTemplate(char const* const str)
	    : type(VariantType::STRING)
	    , string_value(str)
	{
	}

	explicit VariantTemplate(std::string const& str)
	    : type(VariantType::STRING)
	    , string_value(str.c_str())
	{
	}

	explicit VariantTemplate(StringType&& str)
	    : type(VariantType::STRING)
	    , string_value(std::move(str))
	{
	}

	VariantTemplate()
	    : type(VariantType::NONE)
	    , bool_value(false)
	{
	}

public:
	std::string to_string() const
	{
		if (type == VariantType::POINTER) {
			return fmt::format("{}", pointer_value);
		} else if (type == VariantType::BOOL) {
			return fmt::format("{}", bool_value);
		} else if (type == VariantType::FLOAT) {
			return fmt::format("{}", float_value);
		} else if (type == VariantType::DOUBLE) {
			return fmt::format("{}", double_value);
		} else if (type == VariantType::INT32) {
			return fmt::format("{}", int32_value);
		} else if (type == VariantType::UINT32) {
			return fmt::format("{}", uint32_value);
		} else if (type == VariantType::INT64) {
			return fmt::format("{}", int64_value);
		} else if (type == VariantType::UINT64) {
			return fmt::format("{}", uint64_value);
		} else if (type == VariantType::STRING) {
			return std::string(string_value.c_str());
		}

		return std::string();
	}

public:
	VariantType type;

	StringType string_value;

	union {
		void* pointer_value;
		bool bool_value;
		float float_value;
		double double_value;
		int32_t int32_value;
		uint32_t uint32_value;
		int64_t int64_value;
		uint64_t uint64_value;
	};
};

} // namespace adt
