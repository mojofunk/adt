#pragma once

#include <adt.hpp>

namespace adt {

class CSVOptions
{
public:
	CSVOptions(std::shared_ptr<Record> record)
	    : record(record)
	{
		properties_enabled.fill(false);
	}

public: // Methods
    bool is_last_property_enabled(std::size_t index) const
    {
        assert(index < properties_enabled.size());
        assert(properties_enabled[index]);

        if (++index == properties_enabled.size()) {
            return true;
        }

        for (size_t i = index; i < properties_enabled.size(); ++i) {
            if (properties_enabled[i]) {
                return false;
            }
        }
        return true;
    }

public: // Data
    std::shared_ptr<adt::Record> record;
    bool add_timestamp { false };
    bool add_duration { false };
    std::array<bool, 8> properties_enabled;
};

} // namespace adt
