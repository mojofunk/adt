#pragma once

#include <adt.hpp>

namespace adt {

enum class VariantType : uint8_t {
	NONE,
	POINTER,
	BOOL,
	FLOAT,
	DOUBLE,
	INT32,
	UINT32,
	INT64,
	UINT64,
	STRING
};

extern const char*
variant_type_name(const VariantType type);

} // namespace adt
