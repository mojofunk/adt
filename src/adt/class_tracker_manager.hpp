#pragma once

#include <adt.hpp>

namespace adt {

class ClassTrackerManager
{
public: // methods
	static std::vector<ClassTracker*> get_class_trackers();

	static void reset_tracking();

public: // Called only by ClassTracker derived classes
	static void add_class_tracker(ClassTracker* class_tracker);

private:
	ClassTrackerManager();
};

} // namespace adt
