#pragma once

#include <adt.hpp>

namespace adt {

class Preset
{
public:
	Preset(const std::string& p_name)
	    : name(p_name)
	{
	}

	Preset(const std::string& p_name, std::vector<std::string>&& p_category_names)
	    : name(p_name)
	    , category_names(std::move(p_category_names))
	{
	}

	std::string name;
	std::vector<std::string> category_names;

	bool enable_message_type = false;
	bool enable_data_type = false;
	bool enable_function_type = false;
	bool enable_allocation_type = false;
};

} // namespace adt
