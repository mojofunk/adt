#pragma once

#include <adt-private.hpp>

namespace adt {

class QPCTimestampSource
{
public:
	QPCTimestampSource()
	{
		LARGE_INTEGER freq;
		if (!QueryPerformanceFrequency(&freq) || freq.QuadPart < 1) {
			ADT_DEBUG("Failed to determine frequency of QPC");
			m_timer_rate_us = 0.0;
		} else {
			m_timer_rate_us = 1000000.0 / freq.QuadPart;
		}
		ADT_DEBUG("QPC timer microseconds per tick: %f", m_timer_rate_us);
	}

	// @pre valid()
	int64_t get_microseconds()
	{
		assert(valid());

		LARGE_INTEGER current_val;

		// MS docs say this will always succeed for systems >= XP but it may
		// not return a monotonic value with non-invariant TSC's etc
		QueryPerformanceCounter(&current_val);
		return (int64_t)(current_val.QuadPart * m_timer_rate_us);
	}

	bool valid() { return m_timer_rate_us != 0.0; }

private:
	double m_timer_rate_us = 0.0;
};

} // namespace adt
