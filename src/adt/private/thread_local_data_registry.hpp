#pragma once

#include <adt-private.hpp>

namespace adt {

template <class T>
class ThreadLocalDataRegistry
{
public:
	bool get_all_thread_data(std::vector<T>& thread_local_data)
	{
		std::unique_lock<std::mutex> lock(m_map_mutex, std::try_to_lock);

		if (lock.owns_lock()) {
			for (auto const& id_data : m_map) {
				thread_local_data.push_back(id_data.second);
			}

			return true;
		}
		return false;
	}

	bool register_thread_data(const T& thread_data)
	{
		std::lock_guard<std::mutex> lock(m_map_mutex);

		bool inserted =
		    m_map.insert(std::make_pair(std::this_thread::get_id(), thread_data))
		        .second;

		return inserted;
	}

	bool unregister_thread_data()
	{
		std::lock_guard<std::mutex> lock(m_map_mutex);
		return (m_map.erase(std::this_thread::get_id()) != 0);
	}

	/**
	 * @return Return the thread name associated with the thread_id or an
	 * invalid ThreadInfo.
	 */
	T get_thread_data(std::thread::id const& thread_id)
	{
		std::unique_lock<std::mutex> lock(m_map_mutex, std::try_to_lock);

		if (lock.owns_lock()) {
			auto const it = m_map.find(thread_id);

			if (it != m_map.end()) {
				return it->second;
			}
		} else {
			// what to do here? use lock instead of trylock?
			ADT_DEBUG("ThreadLocalData : Unable to acquire lock\n");
		}
		return T();
	}

private:
	using ThreadLocalDataMap = std::map<std::thread::id, T>;
	ThreadLocalDataMap m_map;
	std::mutex m_map_mutex;
};

} // namespace adt
