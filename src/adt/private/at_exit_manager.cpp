#include "adt/private/source_includes.cpp"

namespace adt {

class AtExitManagerPrivate
{
public:
	A_DISALLOW_COPY_AND_ASSIGN(AtExitManagerPrivate);

	AtExitManagerPrivate() { ADT_DEBUG("AtExitManagerPrivate() ctor\n"); }

	~AtExitManagerPrivate() { execute_functors_now(); }

	static AtExitManagerPrivate& get_instance()
	{
		static AtExitManagerPrivate instance;
		return instance;
	}

	void add(std::function<void()> func)
	{
		ADT_DEBUG("AtExitManagerPrivate: add func\n");
		std::lock_guard<std::mutex> lock(m_exit_funcs_mutex);
		exit_funcs.push(std::move(func));
	}

	void execute_functors_now()
	{
		ADT_DEBUG("AtExitManagerPrivate: execute_functors_now\n");
		std::lock_guard<std::mutex> lock(m_exit_funcs_mutex);

		ADT_DEBUG("AtExitManagerPrivate: exit_funcs count : %zu\n",
		          exit_funcs.size());

		while (!exit_funcs.empty()) {
			exit_funcs.top()();
			exit_funcs.pop();
		}
	}

private:
	std::stack<std::function<void()>> exit_funcs;
	std::mutex m_exit_funcs_mutex;
};

void
AtExitManager::execute_functors_now()
{
	AtExitManagerPrivate::get_instance().execute_functors_now();
}

void
AtExitManager::on_exit(std::function<void()> func)
{
	AtExitManagerPrivate::get_instance().add(func);
}

} // namespace adt
