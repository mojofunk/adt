#pragma once

#include <adt-private.hpp>

namespace adt {

class TrackingAllocator
{
public: // API
	static void* allocate(std::size_t bytes);

	static void deallocate(void* ptr);

	static void deallocate(void* ptr, std::size_t bytes);

	static void start_tracking_this_thread();

	static std::shared_ptr<ThreadAllocationData> stop_tracking_this_thread();

private:
	A_DISALLOW_COPY_AND_ASSIGN(TrackingAllocator);
};

} // namespace adt
