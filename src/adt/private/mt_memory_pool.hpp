#pragma once

#include <adt-private.hpp>

namespace adt {

class MTMemoryPool
{
public:
	// Add an alignment argument
	MTMemoryPool(const std::size_t size_bytes,
	             const std::size_t count,
	             std::size_t max_expected_threads = 8)
	    : m_block(nullptr)
	    , m_size_bytes(size_bytes)
	    , m_count(count)
	    , m_available(count)
	    , m_queue(count * max_expected_threads)
	{
		m_block = (char*)std::malloc(m_size_bytes * m_count);
		if (!m_block) {
			throw std::bad_alloc();
		}

		ADT_DEBUG("MTMemoryPool ctor, size_bytes: %zu, count: %" PRIu32
		          ", max_expected_threads: %zu\n",
		          size_bytes,
		          count,
		          max_expected_threads);

		for (char* tmp = m_block; tmp < end(); tmp += m_size_bytes) {
			bool queued = m_queue.try_enqueue(tmp);
			if (!queued) {
				// TODO Find out why try_enqueue fails over a certain size
				ADT_DEBUG("MTMemoryPool try_enqueue failed, using enqueue\n");
				m_queue.enqueue(tmp);
			}
		}
	}

	~MTMemoryPool()
	{
		assert(m_available == m_count);
		std::free(m_block);
		m_block = nullptr;
	}

	// return true if allocated by pool
	bool is_from(void* ptr) const
	{
		return ((char*)ptr >= m_block) && ((char*)ptr < end());
	}

	void* allocate(const std::size_t bytes)
	{
		if (bytes > m_size_bytes) {
			return nullptr;
		}

		char* mem = nullptr;
		if (m_queue.try_dequeue(mem)) {
			ADT_DEBUG("MTMemoryPool try_dequeue success : %p\n", mem);
#ifdef ADT_ENABLE_DEBUG_ALLOCATOR
			m_available--;
#endif
		} else {
			assert(mem == nullptr);
		}
		return (void*)mem;
	}

	void deallocate(void* ptr)
	{
		assert(is_from(ptr));

		ADT_DEBUG("MTMemoryPool::Deallocate : %p\n", ptr);

		int retries = 0;
		bool enqueue_success = false;
		do {
			enqueue_success = m_queue.try_enqueue((char*)ptr);
			retries++;
		} while (!enqueue_success && retries != 5);

		if (!enqueue_success) {
			ADT_DEBUG("MTMemoryPool try_enqueue failed : %p\n", ptr);
			enqueue_success = m_queue.enqueue((char*)ptr);
		} else {
			ADT_DEBUG("MTMemoryPool try_enqueue successful : %p\n", ptr);
		}

		assert(enqueue_success);
#ifdef ADT_ENABLE_DEBUG_ALLOCATOR
		m_available++;
#endif
		ptr = nullptr;
	}

	bool empty() const { return m_available == 0; }

	std::size_t max_size() const { return m_size_bytes; }

	std::size_t count() const { return m_count; }

	std::size_t available() const { return m_available; }

private: // methods
	char* end() const { return m_block + m_size_bytes * m_count; }

private: // types
	using pool_queue_type = moodycamel::ConcurrentQueue<char*>;

private: // data
	char* m_block;
	const std::size_t m_size_bytes;
	const std::size_t m_count;
	std::atomic<std::size_t> m_available;
	pool_queue_type m_queue;
};

} // namespace adt
