#include "adt/private/source_includes.cpp"

namespace adt {

void
AtThreadExitManager::on_thread_exit(std::function<void()> func)
{
	class ThreadExitHandler
	{
		std::stack<std::function<void()>> exit_funcs;

	public:
		ThreadExitHandler() = default;
		ThreadExitHandler(ThreadExitHandler const&) = delete;
		void operator=(ThreadExitHandler const&) = delete;
		~ThreadExitHandler()
		{
			while (!exit_funcs.empty()) {
				exit_funcs.top()();
				exit_funcs.pop();
			}
		}
		void add(std::function<void()> func) { exit_funcs.push(std::move(func)); }
	};

#if defined(__MINGW32__)
// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=83562
#warning "thread_local is broken using this toolchain"
#else
	thread_local ThreadExitHandler exit_handler;
	exit_handler.add(std::move(func));
#endif
}

} // namespace adt
