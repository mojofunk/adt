#pragma once

#include <adt-private.hpp>

namespace adt {

class GlobalAllocatorPrivate
{
public:
	static GlobalAllocatorPrivate& get_instance();

	GlobalAllocatorPrivate();
	~GlobalAllocatorPrivate();

public:
	void set_allocator(std::shared_ptr<Allocator> alloc);

	void reset_allocator();

public:
	bool get_logging_enabled();

	void set_logging_enabled(bool enable = true);

public: // Internal allocation functions

	void* alloc_noexcept(std::size_t bytes) noexcept
	{
		void* ptr = nullptr;

		if (m_allocator) {
			ptr = m_allocator->allocate(bytes);
		}

		if (!ptr) {
			ptr = std::malloc(bytes);
		}
		return ptr;
	}

	void dealloc_noexcept(void* ptr) noexcept
	{
		if (m_allocator && m_allocator->is_from(ptr)) {
			m_allocator->deallocate(ptr);
			return;
		}
		std::free(ptr);
	}

public: // Common allocation and logging methods
	void* alloc_and_log_noexcept(std::size_t bytes,
	                             AllocationLogger& logger) noexcept
	{
		void* ptr = alloc_noexcept(bytes);

		if (logger.get_enabled()) {
			logger.log_record->this_ptr = ptr;
			logger.log_record->move_to_property_1("Bytes Allocated", (uint64_t)bytes);

			if (category.trace_enabled) {
				logger.set_stack_trace(new StackTrace(2));
			}
		}

		return ptr;
	}

	void dealloc_and_log_noexcept(void* ptr, AllocationLogger& logger) noexcept
	{
		if (logger.get_enabled()) {
			logger.log_record->this_ptr = ptr;

			if (category.trace_enabled) {
				logger.set_stack_trace(new StackTrace(2));
			}
		}

		dealloc_noexcept(ptr);
	}

public: // C++ global replacement new/delete
	void* new_except(std::size_t bytes)
	{
		AllocationLogger logger(&category, __LINE__, __FILE__, A_STRFUNC);
		return alloc_and_log_noexcept(bytes, logger);
	}

	void* new_array_except(std::size_t bytes)
	{
		AllocationLogger logger(&category, __LINE__, __FILE__, A_STRFUNC);
		return alloc_and_log_noexcept(bytes, logger);
	}

	void* new_noexcept(std::size_t bytes) noexcept
	{
		AllocationLogger logger(&category, __LINE__, __FILE__, A_STRFUNC);
		return alloc_and_log_noexcept(bytes, logger);
	}

	void* new_array_noexcept(std::size_t bytes) noexcept
	{
		AllocationLogger logger(&category, __LINE__, __FILE__, A_STRFUNC);
		return alloc_and_log_noexcept(bytes, logger);
	}

	void delete_except(void* ptr)
	{
		AllocationLogger logger(&category, __LINE__, __FILE__, A_STRFUNC);
		dealloc_and_log_noexcept(ptr, logger);
	}

	void delete_array_except(void* ptr)
	{
		AllocationLogger logger(&category, __LINE__, __FILE__, A_STRFUNC);
		dealloc_and_log_noexcept(ptr, logger);
	}

	void delete_noexcept(void* ptr) noexcept
	{
		AllocationLogger logger(&category, __LINE__, __FILE__, A_STRFUNC);
		dealloc_and_log_noexcept(ptr, logger);
	}

	void delete_array_noexcept(void* ptr) noexcept
	{
		AllocationLogger logger(&category, __LINE__, __FILE__, A_STRFUNC);
		dealloc_and_log_noexcept(ptr, logger);
	}

public:
	LogCategory category = { "adt::GlobalAllocator" };

private:
	std::shared_ptr<Allocator> m_allocator;
};

} // namespace adt
