#pragma once

#include <adt-private.hpp>

namespace adt {

class LogPrivate : public RunLoop
{
public:
	LogPrivate();

	~LogPrivate();

public:
	static void initialize();

public:
	bool get_enabled() const;

	void set_enabled(bool enable = true);

public: // Log interface
	void set_sink(std::shared_ptr<Sink> sink);

	std::shared_ptr<Sink> get_sink() const;

	/**
	 * @return true if allocation occured when queueing record.
	 */
	bool write_record_async(LogRecord* log_record);

	void write_record_sync(LogRecord& log_record);

public: // Categories
	LogCategory* make_category(const char* const name);

	std::set<LogCategory*> get_categories() const;

	std::vector<std::string> get_category_names();

	void set_all_categories_enabled(bool enable = true);

	void set_categories_enabled(std::vector<std::string> const& categories,
	                            bool enable);

public: // Presets
	void add_preset(std::shared_ptr<Preset>);

	void remove_preset(std::shared_ptr<Preset> const&);

	void set_preset_enabled(std::shared_ptr<Preset>);

	void set_preset_disabled(std::shared_ptr<Preset>);

	std::vector<std::shared_ptr<Preset>> get_presets();

public: // Thread
	bool is_internal_thread(std::thread::id id)
	{
		if (!running() || !m_sink) {
			return true;
		}
		return (id == m_record_processing_thread.get_id()) ||
		       m_sink->is_internal_thread(id);
	}

public: // Cache
	// Must be called with Log disabled
	void set_cache_handler(RecordCacheHandler* handler);

	// Must be called with Log disabled
	void reset_cache_handler();

public: // Filters
	void set_filters_enabled(bool enable);

	bool get_filters_enabled() { return m_filters_enabled; }

	void add_filter(std::shared_ptr<Filter>);

	void remove_filter(std::shared_ptr<Filter> const&);

	std::vector<std::shared_ptr<Filter>> get_filters();

public: // Global controls
	void set_sync_enabled(bool enable);

	bool get_sync_enabled() const { return m_sync_enabled; }

	void set_trace_enabled(bool enable);

	bool get_trace_enabled() const { return m_trace_enabled; }

	void set_properties_enabled(bool enable);

	bool get_properties_enabled() const { return m_properties_enabled; }

public: // Types
	void set_all_types_enabled(bool enable);

	void set_message_type_enabled(bool enable);

	bool get_message_type_enabled() const { return m_message_type_enabled; }

	void set_data_type_enabled(bool enable);

	bool get_data_type_enabled() const { return m_data_type_enabled; }

	void set_function_type_enabled(bool enable);

	bool get_function_type_enabled() const { return m_function_type_enabled; }

	void set_allocation_type_enabled(bool enable);

	bool get_allocation_type_enabled() const { return m_allocation_type_enabled; }

private: // RunLoop
	void on_iteration() override;

	void process_records();

private: // Cache
	void cache_flush_timeout();

	void add_record_to_cache(std::shared_ptr<Record> const& record);

	/// @pre m_cache_handler != nullptr
	bool cache_full() const
	{
		return m_cache.size() != m_cache_handler->preferred_cache_size();
	}

private: // methods
	void destroy_categories();

	void set_category_default_settings(LogCategory*);

	bool matches_filters(LogRecord const& log_record) const;

private: // data
	// queue for Records
	using RecordQueueType = moodycamel::ConcurrentQueue<LogRecord*>;
	RecordQueueType m_record_queue;

	RecordCacheHandler* m_cache_handler{ nullptr };
	RecordSPVec m_cache;
	mutable std::mutex m_cache_mutex; // only required for sync logging

	std::shared_ptr<Sink> m_sink;

	std::set<LogCategory*> m_categories;
	mutable std::mutex m_categories_mutex;

	std::vector<std::string> m_logging_categories;
	std::vector<std::string> m_categories_to_enable;
	bool m_enable_all_categories{ false };

	std::vector<std::shared_ptr<Preset>> m_presets;
	mutable std::mutex m_presets_mutex;

	std::set<std::shared_ptr<Filter>> m_filters;
	mutable std::mutex m_filters_mutex;
	std::atomic<bool> m_filters_enabled{ false };

	std::thread m_record_processing_thread;

	bool m_sync_enabled{ false };
	bool m_trace_enabled{ false };
	bool m_properties_enabled{ true };

	bool m_message_type_enabled{ false };
	bool m_data_type_enabled{ false };
	bool m_function_type_enabled{ false };
	bool m_allocation_type_enabled{ false };

public: // Singleton
	A_SINGLETON(LogPrivate);
	static LogPrivate* s_instance; // cached
};

} // namespace adt
