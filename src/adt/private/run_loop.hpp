#pragma once

#include <adt-private.hpp>

namespace adt {

class RunLoop
{
public:
	/**
	 * Create a new RunLoop, run() must be called to start
	 * the main loop.
	 */
	RunLoop();

	/**
	 * assert that quit() has been called.
	 */
	virtual ~RunLoop();

	/**
	 * Start the loop, this will block until quit is called
	 */
	void run();

	void quit(bool block = false);

	/**
	 * Signal the RunLoop to iterate and wait for it to complete.
	 */
	void wait_for_iteration();

	/**
	 * Signal the RunLoop to iterate but don't wait for it to complete.
	 */
	void schedule_iteration();

	/**
	 * @return true if RunLoop is running.
	 *
	 * This is true after run after run() has been called and false
	 * after quit(true) has been called.
	 */
	bool running() const { return m_running; }

protected:
	virtual void on_iteration() = 0;

	/**
	 * \return true if thread is able to run.
	 */
	bool can_run();

	std::mutex m_iter_mtx;

	std::condition_variable m_cond;

	std::atomic<bool> m_running;

	std::atomic<int> m_iter_count;
};

} // namespace adt
