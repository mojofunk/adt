#pragma once

#include <adt-private.hpp>

namespace adt {

class STVectorMemoryPool
{
public:
	// Add an alignment argument
	STVectorMemoryPool(const std::size_t size_bytes, const std::size_t count)
	    : m_size_bytes(size_bytes)
	    , m_count(count)
	    , m_pool((char*)std::malloc(pool_size_bytes()))
	{
		if (!m_pool) {
			throw std::bad_alloc();
		}

		m_free_blocks.reserve(m_count);

		for (char* pool_block = pool_start(); pool_block < pool_end();
		     pool_block += m_size_bytes) {
			m_free_blocks.push_back(pool_block);
		}

		assert(available() == m_count);
	}

	~STVectorMemoryPool()
	{
		assert(available() == m_count);
		std::free(m_pool);
	}

	// return true if allocated by pool
	bool is_from(void* ptr) const
	{
		return ((char*)ptr >= pool_start()) && ((char*)ptr < pool_end());
	}

	void* allocate(const std::size_t bytes)
	{
		if (bytes > m_size_bytes) {
			return nullptr;
		}

		if (!m_free_blocks.size()) {
			return nullptr;
		}

		char* mem = m_free_blocks.back();
		m_free_blocks.pop_back();

		return (void*)mem;
	}

	void deallocate(void* ptr)
	{
		assert(is_from(ptr));

		m_free_blocks.push_back((char*)ptr);
	}

	bool empty() const { return available() == 0; }

	std::size_t max_size() const { return m_size_bytes; }

	std::size_t count() const { return m_count; }

	std::size_t available() const { return m_free_blocks.size(); }

private: // methods
	char* pool_start() const { return m_pool; }
	char* pool_end() const { return pool_start() + pool_size_bytes(); }

	std::size_t pool_size_bytes() const { return m_size_bytes * m_count; }

private: // data
	std::size_t const m_size_bytes;
	std::size_t const m_count;
	char* const m_pool;

	std::vector<char*> m_free_blocks;
};

} // namespace adt
