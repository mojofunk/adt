#include "adt/private/source_includes.cpp"

namespace adt {

RunLoop::RunLoop()
    : m_running(false)
    , m_iter_count(0)
{
}

RunLoop::~RunLoop() {} // NOLINT

void
RunLoop::run()
{
	ADT_DEBUG("RunLoop::run()\n");

	m_running = true;

	while (can_run()) {

		{
			std::unique_lock<std::mutex> lock(m_iter_mtx);

			while (m_iter_count < 1) {
				m_cond.wait(lock);
			}

			// TODO just use atomic compare and exchange?
			while (--m_iter_count > 0) {
			}
		}

		ADT_DEBUG("RunLoop::run() : calling on_iteration()\n");

		on_iteration();

		{
			std::unique_lock<std::mutex> lock(m_iter_mtx);

			m_cond.notify_one();
		}
	}
}

void
RunLoop::quit(bool block)
{
	if (!m_running) {
		return;
	}

	std::unique_lock<std::mutex> lock(m_iter_mtx);

	m_running = false;

	schedule_iteration();

	if (block) {
		// wait for the iteration to complete
		// which can only happen when m_iter_mtx
		// is released in the wait.
		ADT_DEBUG("RunLoop::quit() : Waiting for thread to quit\n");
		m_cond.wait(lock);
	}
}

bool
RunLoop::can_run()
{
	std::unique_lock<std::mutex> lock(m_iter_mtx);

	if (!m_running) {
		m_cond.notify_one();
		return false;
	}
	return true;
}

void
RunLoop::wait_for_iteration()
{
	ADT_DEBUG("RunLoop::wait_for_iteration\n");

	++m_iter_count;

	std::unique_lock<std::mutex> lock(m_iter_mtx);

	m_cond.notify_one();

	// wait for one iteration to complete
	m_cond.wait(lock);
}

void
RunLoop::schedule_iteration()
{
	ADT_DEBUG("RunLoop::schedule_iteration\n");

	++m_iter_count;

	m_cond.notify_one();
}

} // namespace adt
