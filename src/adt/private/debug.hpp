//#define ADT_ENABLE_DEBUG

#ifdef ADT_ENABLE_DEBUG
#include <cstdio>
#define ADT_DEBUG(...) printf(__VA_ARGS__)
#else
#define ADT_DEBUG(...)
#endif

//#define ADT_ENABLE_DEBUG_ALLOCATOR

#ifdef ADT_ENABLE_DEBUG_ALLOCATOR
#define ADT_DEBUG_ALLOCATOR(...) printf(__VA_ARGS__)
#else
#define ADT_DEBUG_ALLOCATOR(...)
#endif

//#define ADT_ENABLE_INTERNAL_LOGGING
