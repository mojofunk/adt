#include "adt/private/source_includes.cpp"

namespace adt {

std::string
demangle_symbol(const std::string& mangled_symbol)
{
	return demangle_symbol_template<std::string>(mangled_symbol);
}

std::string
demangle(std::string const& str)
{
	return demangle_template<std::string>(str);
}

} // namespace adt
