// Keep this at the top to ensure it doesn't depend on any of the
// includes being listed below.
#include "adt-private.hpp"

#ifdef HAVE_CONFIG_H
#include "adt-config.h"
#endif

#include <iostream>
#include <queue>
#include <random>
#include <stack>
#include <sstream>

#ifdef __linux__
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sched.h>
#define HAVE_SCHED_GETCPU
#endif

#ifdef _WIN32
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <windows.h>
#endif
