#include "adt/private/source_includes.cpp"

namespace adt {

TraceEventSinkPrivate::TraceEventSinkPrivate()
    : m_record_handling_thread(&TraceEventSinkPrivate::run, this)
    , m_record_queue(8192)
{
	wait_for_iteration();
}

TraceEventSinkPrivate::~TraceEventSinkPrivate()
{
	ADT_DEBUG("~TraceEventSink()\n");

	quit();
	m_record_handling_thread.join();

	// Don't handle any records still left in queue
	// process_records();

	TraceEventSinkPrivate::set_enabled(false);
}

void
TraceEventSinkPrivate::set_output_file(std::shared_ptr<File> output_file)
{
	m_log_file = output_file;
}

void
TraceEventSinkPrivate::clear_record_queue()
{
	std::shared_ptr<Record> new_record;

	while (m_record_queue.try_dequeue(new_record)) {
		// we aren't doing anything with the dequeued record
	}
}

void
TraceEventSinkPrivate::set_enabled(bool enable)
{
	ADT_DEBUG("TraceEventSink::set_enabled: %s\n", enable ? "true" : "false");
	if (!m_log_file) {
		ADT_DEBUG("TraceEventSink::set_enabled: Invalid Trace Event log file\n");
		return;
	}

	if (enable && !m_enabled) {
		m_enable_state_changing = true;
		m_log_file->open();
		write_header();
		m_enabled = true;
	} else if (!enable && m_enabled) {
		m_enabled = false;
		{
			std::lock_guard<std::mutex> lock(m_log_file_write_mutex);

			write_thread_info(true);
			write_footer();
			m_log_file->close();
		}
		m_enable_state_changing = true;
	}
}

bool
TraceEventSinkPrivate::is_internal_thread(std::thread::id thread_id)
{
	if (!running()) {
		return true;
	}
#ifdef ADT_ENABLE_INTERNAL_LOGGING
	// Log allocations made in the record writing thread
	return false;
#else
	return (thread_id == m_record_handling_thread.get_id());
#endif
}

void
TraceEventSinkPrivate::handle_record_async(
    const std::shared_ptr<Record>& record)
{
	if (!m_enabled) {
		return;
	}

	m_record_queue.enqueue(record);

	schedule_iteration();
}

void
TraceEventSinkPrivate::handle_record_sync(const std::shared_ptr<Record>& record)
{
	if (!m_enabled) {
		return;
	}

	write_record(record);
}

void
TraceEventSinkPrivate::on_iteration()
{
	process_records();
}

void
TraceEventSinkPrivate::process_records()
{
	ADT_DEBUG("TraceEventSink::process_records\n");

	if (m_enable_state_changing) {
		clear_record_queue();
		m_enable_state_changing = false;
	}

	if (!m_enabled) {
		return;
	}

#ifdef ADT_ENABLE_INTERNAL_LOGGING
	LogRecord log_record(
	    "adt::TraceEventSink",
	    RecordType::FUNCTION_CALL,
	    Timing(Timing::Type::DURATION, TimestampSource::get_microseconds()),
	    __LINE__,
	    __FILE__,
	    A_STRFUNC);

	std::size_t record_dequeue_count = 0;
#endif

	std::shared_ptr<Record> new_record;

	while (m_record_queue.try_dequeue(new_record)) {

#ifdef ADT_ENABLE_INTERNAL_LOGGING
		record_dequeue_count++;
#endif

		write_record(new_record);
	}

#ifdef ADT_ENABLE_INTERNAL_LOGGING
	log_record.copy_to_property_1(A_STRINGIFY(record_dequeue_count),
	                              record_dequeue_count);

	log_record.timing.second = TimestampSource::get_microseconds();
	auto record = std::make_shared<Record>(log_record);
	write_record(record);
#endif
}

bool
TraceEventSinkPrivate::write_header()
{
	const std::string start_string("{\"traceEvents\":[");

	if (!m_log_file->write(start_string)) {
		ADT_DEBUG("TraceEventSink::write_header: Error writing string\n");
		return false;
	}
	return true;
}

static std::string
timing_type_to_trace_event_type(const Timing::Type type)
{
	switch (type) {
	case Timing::Type::INSTANT:
		return "i";
	case Timing::Type::BEGIN:
		return "B";
	case Timing::Type::END:
		return "E";
	case Timing::Type::DURATION:
		return "X";
	}
	return {};
}

std::string
TraceEventSinkPrivate::replace_illegal_chars(const std::string& str)
{
	std::string copy(str);

	for (char& c : copy) {
		if (c == '\"') {
			c = '\'';
		} else if (c == '\n') {
			c = ' ';
		} else if (c == '\t') {
			c = ' ';
		} else if (c == ':') {
			c = ' ';
		} else if (c == '\\') {
			c = '/';
		} else if (c == '\r') {
			c = ' ';
		}
	}
	return copy;
}

std::string
TraceEventSinkPrivate::record_to_string(Record const& record)
{
	std::ostringstream oss;

	oss.imbue(std::locale::classic());

	oss << R"({"pid":1,)";
	oss << R"("tid":")" << record.thread_id << R"(",)";
	oss << R"("ts":)" << record.timing.first << ",";
	oss << R"("ph":")" << timing_type_to_trace_event_type(record.timing.type)
	    << R"(",)";
	oss << R"("cat":")" << record.category->name << R"(",)";

	oss << R"("name":")" << short_function_name(record.src_location.function_name)
	    << R"(","args":{)";
	oss << R"("src_file":")"
	    << get_filename_from_path(record.src_location.file_path) << R"(",)";
	oss << R"("src_line":")" << record.src_location.line << R"(")";

	if (!record.properties.empty()) {
		oss << R"(,"args":{)";

		for (auto i = record.properties.begin(); i != record.properties.end();) {
			oss << R"(")" << i->name << R"(":")"
			    << replace_illegal_chars(i->value.to_string()) << R"(")";

			if (++i != record.properties.end()) {
				oss << ",";
			}
		}

		oss << "}"; // args
	}

	oss << "}"; // args

	if (record.timing.type == Timing::Type::DURATION) {
		oss << R"(,"dur":")" << record.timing.duration() << R"(")";
	}

	oss << "}";

	// TODO this probably shouldn't be part of the string
	oss << "," << std::endl;

	return oss.str();
}

bool
TraceEventSinkPrivate::write_record(const std::shared_ptr<Record>& record)
{
	std::lock_guard<std::mutex> lock(m_log_file_write_mutex);

	bool write_record_success = m_log_file->write(record_to_string(*record.get()));

	m_record_counter++;

	bool write_thread_info_success = true;
	if (m_record_counter > 10000) {
		write_thread_info_success = write_thread_info(false);
		m_record_counter = 0;
	}
	return write_record_success && write_thread_info_success;
}

bool
TraceEventSinkPrivate::write_thread_info(bool final_info)
{
	std::vector<std::shared_ptr<ThreadInfo>> all_thread_info;

	Threads::get_all_thread_info(all_thread_info);

	if (all_thread_info.empty()) {
		// If no threads have been registered or the thread name map wasn't
		// able to be aquired then add one for this thread so that a valid
		// file is produced.
		auto thread_info = std::make_shared<ThreadInfo>(
		    std::this_thread::get_id(), "Main Thread", ThreadPriority::NORMAL);

		all_thread_info.push_back(thread_info);
	}

	for (std::vector<std::shared_ptr<ThreadInfo>>::const_iterator it =
	         all_thread_info.begin();
	     it != all_thread_info.end();) {

		auto* thread_info = it->get();

		std::ostringstream oss;
		oss.imbue(std::locale::classic());

		auto const thread_name = replace_illegal_chars(thread_info->name.substr(0, 40));

		oss << R"({"pid":1,"tid":")" << thread_info->id << R"(",)";
		oss << R"("ts":0,"ph":"M",)";
		oss << R"("cat":"__metadata",)";
		oss << R"("name":"thread_name",)";
		oss << R"("args":{"name":")" << thread_name << R"("}})";

		if (++it == all_thread_info.end()) {
			if (!final_info) {
				oss << "," << std::endl;
			}
		} else {
			oss << "," << std::endl;
		}

		const std::string event_string = oss.str();
		if (!m_log_file->write(event_string)) {
			ADT_DEBUG("TraceEventSink::write_thread_info: Error writing string\n");
			return false;
		}
	}
	return true;
}

bool
TraceEventSinkPrivate::write_footer()
{
	const std::string end_string("]}");

	if (!m_log_file->write(end_string)) {
		ADT_DEBUG("TraceEventSink::write_footer: Error writing string\n");
		return false;
	}
	return true;
}

} // namespace adt
