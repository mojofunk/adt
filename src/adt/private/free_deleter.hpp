#pragma once

#include <adt-private.hpp>

namespace adt {

/**
 * A function object which invokes std::free() on its pointer parameter.
 * Can be used to store malloc-allocated pointers in std::unique_ptr
*/
struct FreeDeleter {
	inline void operator()(void* ptr) const { std::free(ptr); }
};

} // namespace adt
