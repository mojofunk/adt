#pragma once

#include <adt-private.hpp>

#if defined(__GLIBCXX__) || defined (__clang__)
#if !defined(HAVE_CXXABI_H)
#define HAVE_CXXABI_H 1
#endif
#endif

#if HAVE_CXXABI_H
#include <cxxabi.h>
#endif

namespace adt {

template <class StringType>
StringType
demangle_symbol_template(const StringType& mangled_symbol)
{
#if HAVE_CXXABI_H
	int status = 0;

	std::unique_ptr<char, FreeDeleter> realname(
	    abi::__cxa_demangle(mangled_symbol.c_str(), NULL, NULL, &status));

	if (status == 0) {
		assert(realname);
		return StringType(realname.get());
	}
#endif
	return mangled_symbol;
}

template <class StringType>
StringType
demangle_template(StringType const& str)
{
	typename StringType::size_type const b = str.find_first_of("(");

	if (b == StringType::npos) {
		return demangle_symbol_template(str);
	}

	typename StringType::size_type const p = str.find_last_of("+");

	if (p == StringType::npos) {
		return demangle_symbol_template(str);
	}

	if ((p - b) <= 1) {
		return demangle_symbol_template(str);
	}

	StringType const symbol = str.substr(b + 1, p - b - 1);

	return demangle_symbol_template<StringType>(symbol);
}

} // namespace adt
