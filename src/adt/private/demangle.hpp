#pragma once

#include <adt-private.hpp>

namespace adt {

/**
 * @param symbol a mangled symbol/name
 * @return a demangled symbol/name
 */
std::string
demangle_symbol(const std::string& mangled_symbol);

/**
 * @param str a string containing a mangled symbol/name
 * @return a string with the mangled symbol/name replaced with a demangled
 * name
 */
std::string
demangle(const std::string& str);

template <typename T>
std::string
demangled_name(T const& obj)
{
	return demangle_symbol(typeid(obj).name());
}

} // namespace adt
