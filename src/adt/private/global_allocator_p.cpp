#include "adt/private/source_includes.cpp"

namespace adt {

GlobalAllocatorPrivate&
GlobalAllocatorPrivate::get_instance()
{
	static GlobalAllocatorPrivate instance;
	return instance;
}

GlobalAllocatorPrivate::GlobalAllocatorPrivate()
{
	AllocatorRegistry::initialize();
}

GlobalAllocatorPrivate::~GlobalAllocatorPrivate()
{
	reset_allocator();
}

void
GlobalAllocatorPrivate::set_allocator(std::shared_ptr<Allocator> alloc)
{
	assert(!m_allocator);

	AllocatorRegistry::add_allocator(m_allocator);
	m_allocator = alloc;
}

void
GlobalAllocatorPrivate::reset_allocator()
{
	if (m_allocator) {
		AllocatorRegistry::remove_allocator(m_allocator);
	}
	m_allocator.reset();
}

bool
GlobalAllocatorPrivate::get_logging_enabled()
{
	return category.enabled && category.allocation_type_enabled;
}

void
GlobalAllocatorPrivate::set_logging_enabled(bool enable)
{
	category.enabled = enable;
	category.allocation_type_enabled = enable;
}

} // namespace adt
