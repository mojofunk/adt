#pragma once

#include <adt-private.hpp>

namespace adt {

template <class T, class Alloc>
class FixedSizeQueue
{
public:
	FixedSizeQueue(size_t size)
	    : m_size(size + 1)
	    , m_read_index(0)
	    , m_write_index(0)
	{
		m_buffer = m_alloc.allocate(allocation_size_bytes());
	}

	~FixedSizeQueue() { m_alloc.deallocate(m_buffer, allocation_size_bytes()); }

	bool try_dequeue(T& dest)
	{
		size_t const r = m_read_index.load(std::memory_order_relaxed);
		size_t const w = m_write_index.load(std::memory_order_acquire);
		size_t new_r = r;

		++new_r;

		if (new_r == m_size) {
			// end of buffer, wrap around
			new_r = 0;
		}

		if (r == w) {
			// full
			return false;
		}

		dest = m_buffer[r];

		m_read_index = new_r;

		return true;
	}

	bool try_enqueue(T const& src)
	{
		// relaxed as only written from write thread
		size_t const w = m_write_index.load(std::memory_order_relaxed);
		size_t const r = m_read_index.load(std::memory_order_acquire);
		size_t new_w = w;

		++new_w;

		if (new_w == m_size) {
			// end of buffer, wrap around
			new_w = 0;
		}

		if (w != r) {
			if (new_w == r) {
				// full
				return false;
			}
		}

		m_buffer[m_write_index] = src;

		m_write_index = new_w;

		return true;
	}

#if 0
	bool
	try_dequeue (T& dest)
	{
		if (!read_available()) {
			return false;
		}

		dest = m_buffer[m_read_index];

		increment_read_index(1);
		return true;
	}

	bool
	try_enqueue (T const & src)
	{
		if (!write_available()) {
			return false;
		}

		m_buffer[m_write_index] = src;

		increment_write_index (1);
		return true;
	}
#endif

	bool empty() const { return m_read_index == m_write_index; }

	size_t read_available() const
	{
		// relaxed as only written from read thread
		size_t r = m_read_index.load(std::memory_order_relaxed);
		size_t w = m_write_index.load(std::memory_order_acquire);

		return read_available(r, w);
	}

	size_t write_available() const
	{
		// relaxed as only written from write thread
		size_t w = m_write_index.load(std::memory_order_relaxed);
		size_t r = m_read_index.load(std::memory_order_acquire);

		return write_available(r, w);
	}

private:
	/**
	 * An extra slot in the array/buffer is required to be able to write
	 * the requested size into the queue.
	 */
	std::size_t allocation_size_bytes() const { return (sizeof(T) * m_size); }

	void increment_read_index(size_t count)
	{
		// TODO? use load/relaxed and store/release
		m_read_index = (m_read_index + count) % m_size;
	}

	void increment_write_index(size_t count)
	{
		// TODO? use load/relaxed and store/release
		m_write_index = (m_write_index + count) % m_size;
	}

	size_t read_available(size_t read_index, size_t write_index) const
	{
		if (write_index > read_index) {
			return write_index - read_index;
		} else {
			return (write_index - read_index + m_size) % m_size;
		}
	}

	size_t write_available(size_t read_index, size_t write_index) const
	{
		if (write_index > read_index) {
			return ((read_index - write_index + m_size) % m_size) - 1;
		} else if (write_index < read_index) {
			return (read_index - write_index) - 1;
		} else {
			return m_size - 1;
		}
	}

private: // Data
	Alloc m_alloc;

	std::size_t const m_size;

	T* m_buffer;

	std::atomic<size_t> m_read_index;
	std::atomic<size_t> m_write_index;
};

} // namespace adt
