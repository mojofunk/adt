#pragma once

#include <adt.hpp>

namespace adt {

class ClassTrackerManagerPrivate
{
public:
	ClassTrackerManagerPrivate() = default;

	~ClassTrackerManagerPrivate();

	std::vector<ClassTracker*> get_class_trackers();

	void add_class_tracker(ClassTracker* class_tracker);

	void print_leaks();

	void delete_class_trackers();

private:
	std::mutex m_class_trackers_mutex;
	std::set<ClassTracker*> m_class_trackers;

public:
	A_SINGLETON(ClassTrackerManagerPrivate);
};

} // namespace adt
