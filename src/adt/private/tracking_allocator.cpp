#include "adt/private/source_includes.cpp"

namespace adt {

class TrackingAllocatorPrivate
{
public:
	static void
	set_thread_allocation_data(std::shared_ptr<ThreadAllocationData> const& info);

	static std::shared_ptr<ThreadAllocationData>
	    get_thread_allocation_data(std::thread::id);

	static std::shared_ptr<ThreadAllocationData>& get_thread_allocation_data();

public:
	static void* allocate(std::size_t size)
	{
		auto ptr = std::malloc(size);

		auto alloc_data = get_thread_allocation_data();

		if (alloc_data) {
			alloc_data->allocations.insert({ ptr, size });
		}

		return ptr;
	}

	static void deallocate(void* ptr)
	{

		std::free(ptr);

		auto alloc_data = get_thread_allocation_data();

		if (alloc_data) {
			auto const it = alloc_data->allocations.find(ptr);

			if (it != alloc_data->allocations.end()) {
				alloc_data->deallocations.insert({ ptr, it->second });
			} else {
				// allocation occurred before tracking started.
				alloc_data->deallocations.insert({ ptr, 0 });
			}
		}
	}

	static void deallocate(void* ptr, std::size_t bytes)
	{
		(void)bytes;
		deallocate(ptr);
	}

	static void start_tracking_this_thread()
	{
		auto alloc_data = get_thread_allocation_data();

		assert(!alloc_data);

		get_thread_allocation_data() = std::make_shared<ThreadAllocationData>();
	}

	static std::shared_ptr<ThreadAllocationData> stop_tracking_this_thread()
	{
		auto alloc_data = get_thread_allocation_data();

		assert(alloc_data);

		get_thread_allocation_data().reset();

		return alloc_data;
	}
};

std::shared_ptr<ThreadAllocationData>&
TrackingAllocatorPrivate::get_thread_allocation_data()
{
#ifdef __MINGW32__
#warning "thread_local is broken using this toolchain"
	// XXX leaking seems like the only option
	thread_local std::shared_ptr<ThreadAllocationData>* tls_thread_info =
	    new std::shared_ptr<ThreadAllocationData>();
	return *tls_thread_info;
#else
	thread_local std::shared_ptr<ThreadAllocationData> tls_thread_info;
	return tls_thread_info;
#endif
}

void*
TrackingAllocator::allocate(std::size_t bytes)
{
	return TrackingAllocatorPrivate::allocate(bytes);
}

void
TrackingAllocator::deallocate(void* ptr)
{
	TrackingAllocatorPrivate::deallocate(ptr);
}

void
TrackingAllocator::deallocate(void* ptr, std::size_t bytes)
{
	TrackingAllocatorPrivate::deallocate(ptr, bytes);
}

void
TrackingAllocator::start_tracking_this_thread()
{
	TrackingAllocatorPrivate::start_tracking_this_thread();
}

std::shared_ptr<ThreadAllocationData>
TrackingAllocator::stop_tracking_this_thread()
{
	return TrackingAllocatorPrivate::stop_tracking_this_thread();
}

} // namespace adt
