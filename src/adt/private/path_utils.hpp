#pragma once

#include <adt-private.hpp>

namespace adt {

// TODO Remove once c++17 is min requirement and use <filesystem>.
std::string
get_filename_from_path(std::string const& path);

} // namespace adt
