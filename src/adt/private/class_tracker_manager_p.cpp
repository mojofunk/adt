#include "adt/private/source_includes.cpp"

namespace adt {

ClassTrackerManagerPrivate::~ClassTrackerManagerPrivate()
{
	ADT_DEBUG("~ClassTrackerManagerPrivate\n");
	print_leaks();

	delete_class_trackers();
}

std::vector<ClassTracker*>
ClassTrackerManagerPrivate::get_class_trackers()
{
	std::lock_guard<std::mutex> guard(m_class_trackers_mutex);

	std::vector<ClassTracker*> class_trackers;

	class_trackers.reserve(m_class_trackers.size());

	for (auto tracker : m_class_trackers) {
		class_trackers.emplace_back(tracker);
	}
	return class_trackers;
}

void
ClassTrackerManagerPrivate::add_class_tracker(ClassTracker* class_tracker)
{
	std::lock_guard<std::mutex> guard(m_class_trackers_mutex);

	if (!m_class_trackers.insert(class_tracker).second) {
		assert(false);
	}
}

void
ClassTrackerManagerPrivate::print_leaks()
{
	struct TrackedClassSorter {
		bool operator()(const ClassTracker* lhs, const ClassTracker* rhs) const
		{
			return lhs->get_tracked_class_name() < rhs->get_tracked_class_name();
		}
	};

	using SortedSet = std::set<ClassTracker*, TrackedClassSorter>;

	std::lock_guard<std::mutex> guard(m_class_trackers_mutex);

	SortedSet sorted_class_trackers(m_class_trackers.begin(),
	                                m_class_trackers.end());

	for (auto const& tracker : sorted_class_trackers) {
		if (tracker->get_tracked_class_count() != 0) {
			std::cerr << tracker->get_tracked_class_name() << " leaked "
			          << tracker->get_tracked_class_count() << " times " << std::endl;

			auto unique_traces = tracker->get_unique_traces();

			for (auto const& trace_and_count : unique_traces) {
				std::cerr << std::endl;
				std::cerr << "Leaked " << trace_and_count.second
				          << " times at :" << std::endl;
				trace_and_count.first->to_stream(std::cerr);
			}
		}
	}
}

void
ClassTrackerManagerPrivate::delete_class_trackers()
{
	for (auto const& tracker : m_class_trackers) {
		delete tracker;
	}
}

// Singleton interface
ClassTrackerManagerPrivate*
ClassTrackerManagerPrivate::get_instance()
{
	return Singleton<ClassTrackerManagerPrivate>::get();
}

} // namespace adt