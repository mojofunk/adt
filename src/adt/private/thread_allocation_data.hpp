#pragma once

#include <adt-private.hpp>

namespace adt {

class ThreadAllocationData
{
public:
	using AllocationMapAllocator =
	    StdMallocAllocator<std::pair<void* const, std::size_t>>;
	using AllocationMap =
	    std::map<void*, std::size_t, std::less<void*>, AllocationMapAllocator>;

	AllocationMap allocations;

	AllocationMap deallocations;

	bool no_leaks() { return allocations == deallocations; }

	bool constant_heap_usage()
	{
		return allocations.empty() && deallocations.empty();
	}
};

} // namespace adt
