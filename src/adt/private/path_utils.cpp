#include "adt/private/source_includes.cpp"

namespace adt {

std::string
get_filename_from_path(std::string const& path)
{
	struct MatchPathSeparator {
		bool operator()(char ch) const
		{
#ifdef _WIN32
			return ch == '\\' || ch == '/';
#else
			return ch == '/';
#endif
		}
	};

	return std::string(
	    std::find_if(path.rbegin(), path.rend(), MatchPathSeparator()).base(),
	    path.end());
}

} // namespace adt
