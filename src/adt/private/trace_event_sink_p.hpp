#pragma once

#include <adt-private.hpp>

namespace adt {

class TraceEventSinkPrivate : public Sink, public RunLoop
{
public: // Ctors
	TraceEventSinkPrivate();

	~TraceEventSinkPrivate();

public: // Sink interface
	void handle_record_async(const std::shared_ptr<Record>& record) override;

	void handle_record_sync(const std::shared_ptr<Record>& record) override;

private: // RunLoop interface
	void on_iteration() override;

public: // Methods
	// this should be std::unique_ptr
	void set_output_file(std::shared_ptr<File> output_file);

	bool get_enabled() const override { return m_enabled; }

	void set_enabled(bool enable = true) override;

	bool is_internal_thread(std::thread::id thread_id) override;

	static std::string record_to_string(const Record& record);

private: // Methods
	void process_records();
	void clear_record_queue();

	bool write_header();
	bool write_record(std::shared_ptr<Record> const& record);
	bool write_thread_info(bool final_info);
	bool write_footer();

	static std::string replace_illegal_chars(const std::string& str);

private: // Data
	std::thread m_record_handling_thread;

	std::atomic<bool> m_enabled{ false };
	std::atomic<bool> m_enable_state_changing{ false };

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4324) // structure was padded due to __declspec(align())
#endif

	using RecordQueueType = moodycamel::ReaderWriterQueue<std::shared_ptr<Record>>;
	RecordQueueType m_record_queue;

#ifdef _MSC_VER
#pragma warning(pop)
#endif

	mutable std::mutex m_log_file_write_mutex;
	std::shared_ptr<File> m_log_file;

	uint32_t m_record_counter{ 0 };
};

} // namespace adt
