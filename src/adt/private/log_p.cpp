#include "adt/private/source_includes.cpp"

namespace adt {

// Singleton interface
LogPrivate*
LogPrivate::get_instance()
{
#ifdef ADT_ENABLE_DEBUG
	bool first_call = !s_instance;
	if (first_call) {
		ADT_DEBUG("Creating LogPrivate instance...\n");
	}
#endif

	auto instance = Singleton<LogPrivate>::get();

#ifdef ADT_ENABLE_DEBUG
	if (first_call) {
		ADT_DEBUG("Created LogPrivate instance\n");
	}
#endif

	return instance;
}

LogPrivate* LogPrivate::s_instance = nullptr;

LogPrivate::LogPrivate()
    : m_record_queue(32768 * 4) // max capacity, max threads
    , m_sink(new OStreamSink)
{
	assert(s_instance == nullptr);

	s_instance = this;

	// The Log Singleton is registered with AtExitManager after returning
	// from ctor so these will be destroyed after the Log Singleton, which
	// is required.
	LogAllocator::initialize();
	TimestampSource::initialize();
}

LogPrivate::~LogPrivate()
{
	ADT_DEBUG("~LogPrivate()\n");

	set_all_categories_enabled(false);
	GlobalAllocator::set_logging_enabled(false);

	// don't allow messages to be queued during destruction
	set_enabled(false);

	// process/drop any log messages still left in queue
	process_records();
	destroy_categories();

	s_instance = nullptr;
}

void
LogPrivate::initialize()
{
	get_instance();
}

bool
LogPrivate::get_enabled() const
{
	return running();
}

void
LogPrivate::set_enabled(bool enable)
{
	if (!running() && enable) {
		m_record_processing_thread = std::thread(&LogPrivate::run, this);
		wait_for_iteration();
		return;
	}

	if (running() && !enable) {
		quit();
		m_record_processing_thread.join();
	}
}

void
LogPrivate::destroy_categories()
{
	// no need to lock in dtor

	for (auto category : m_categories) {
		delete category;
	}
}

void
LogPrivate::set_sink(std::shared_ptr<Sink> sink)
{
	m_sink = sink;
}

std::shared_ptr<Sink>
LogPrivate::get_sink() const
{
	return m_sink;
}

bool
LogPrivate::matches_filters(LogRecord const& log_record) const
{
	if (!m_filters_enabled) {
		return true;
	}

	std::lock_guard<std::mutex> lock(m_filters_mutex);

	for (auto const& filter : m_filters) {
		if (!filter->matches(log_record)) {
			return false;
		}
	}

	return true;
}

bool
LogPrivate::write_record_async(LogRecord* log_record)
{
	if (!running()) {
		return false;
	}

	ADT_DEBUG("Log : write_record_async()\n");

	bool queue_allocated = false;

	if (!m_record_queue.try_enqueue(log_record)) {
		ADT_DEBUG("Unable to enqueue LogRecord using non-blocking API\n");
		m_record_queue.enqueue(log_record);
		queue_allocated = true;
	}
	schedule_iteration();

	return queue_allocated;
}

void
LogPrivate::write_record_sync(LogRecord& log_record)
{
	if (!running()) {
		return;
	}

	ADT_DEBUG("Log : write_record_sync()\n");

	if (!matches_filters(log_record)) {
		return;
	}

	std::shared_ptr<Sink> sink(m_sink);

	auto record = std::make_shared<Record>(log_record);
	sink->handle_record_sync(record);

	std::lock_guard<std::mutex> lock(m_cache_mutex);
	add_record_to_cache(record);
	// no cache_flush_timeout means that sync records added to the cache won't be
	// flushed until the cache is full. I'm not sure what to do about it ATM.
}

LogCategory*
LogPrivate::make_category(const char* const name)
{
	std::lock_guard<std::mutex> lock(m_categories_mutex);

	LogCategory* category = nullptr;

	for (auto const existing_category : m_categories) {
		std::string const category_name = existing_category->name;
		if (category_name == name) {
			ADT_DEBUG("Found Existing LogCategory for Category: %s\n",
			          category_name.c_str());
			category = existing_category;
			break;
		}
	}

	if (category == nullptr) {
		ADT_DEBUG("Creating LogCategory for Category: %s\n", name);

		category = new LogCategory(name);

		if (!m_categories.insert(category).second) {
			ADT_DEBUG("Unable to insert LogCategory for Category: %s\n", name);
			return nullptr;
		};
	}

	// Enable it if requested
	auto const iter = std::find(
	    m_categories_to_enable.begin(), m_categories_to_enable.end(), name);
	if (iter != m_categories_to_enable.end() || m_enable_all_categories) {
		category->enabled = true;
	}

	set_category_default_settings(category);

	return category;
}

void
LogPrivate::set_category_default_settings(LogCategory* category)
{
	category->trace_enabled = m_trace_enabled;
	category->sync_enabled = m_sync_enabled;
	category->properties_enabled = m_properties_enabled;

	category->message_type_enabled = m_message_type_enabled;
	category->data_type_enabled = m_data_type_enabled;
	category->function_type_enabled = m_function_type_enabled;
	category->allocation_type_enabled = m_allocation_type_enabled;
}

std::set<LogCategory*>
LogPrivate::get_categories() const
{
	std::lock_guard<std::mutex> lock(m_categories_mutex);
	auto tmp = m_categories;
	return tmp;
}

std::vector<std::string>
LogPrivate::get_category_names()
{
	std::lock_guard<std::mutex> lock(m_categories_mutex);
	auto tmp = m_logging_categories;
	return tmp;
}

void
LogPrivate::set_all_categories_enabled(bool enable)
{
	m_enable_all_categories = enable;

	for (auto const& category : get_categories()) {
		category->enabled = enable;
	}
}

void
LogPrivate::set_categories_enabled(std::vector<std::string> const& categories,
                                   bool enable)
{
	m_categories_to_enable = categories;

	for (const auto& category : get_categories()) {
		std::string const name = category->name;
		for (const auto& requested_category : categories) {
			if (name.find(requested_category) != std::string::npos) {
				category->enabled = enable;
			}
		}
	}
}

void
LogPrivate::add_preset(std::shared_ptr<Preset> preset)
{
	std::lock_guard<std::mutex> lock(m_presets_mutex);

	for (const auto& p : m_presets) {
		std::string const name = p->name;
		if (preset->name == name) {
			// don't add presets with same name, return bool?
			return;
		}
	}

	m_presets.push_back(preset);
}

void
LogPrivate::remove_preset(std::shared_ptr<Preset> const& preset)
{
	std::lock_guard<std::mutex> lock(m_presets_mutex);
	m_presets.erase(std::remove(m_presets.begin(), m_presets.end(), preset),
	                m_presets.end());
}

void
LogPrivate::set_preset_enabled(std::shared_ptr<Preset> preset)
{
	set_categories_enabled(preset->category_names, true);

	if (preset->enable_message_type) {
		set_message_type_enabled(true);
	}
	if (preset->enable_data_type) {
		set_data_type_enabled(true);
	}
	if (preset->enable_function_type) {
		set_function_type_enabled(true);
	}
	if (preset->enable_allocation_type) {
		set_allocation_type_enabled(true);
	}
}

void
LogPrivate::set_preset_disabled(std::shared_ptr<Preset> preset)
{
	set_categories_enabled(preset->category_names, false);

	if (preset->enable_message_type) {
		set_message_type_enabled(false);
	}
	if (preset->enable_data_type) {
		set_data_type_enabled(false);
	}
	if (preset->enable_function_type) {
		set_function_type_enabled(false);
	}
	if (preset->enable_allocation_type) {
		set_allocation_type_enabled(false);
	}
}

std::vector<std::shared_ptr<Preset>>
LogPrivate::get_presets()
{
	std::lock_guard<std::mutex> lock(m_presets_mutex);
	auto tmp = m_presets;
	return tmp;
}

void
LogPrivate::set_cache_handler(RecordCacheHandler* handler)
{
	m_cache_handler = handler;
	m_cache.reserve(handler->preferred_cache_size());
}

void
LogPrivate::reset_cache_handler()
{
	m_cache_handler = nullptr;
}

void
LogPrivate::set_filters_enabled(bool enable)
{
	m_filters_enabled = enable;
}

void
LogPrivate::add_filter(std::shared_ptr<Filter> filter)
{
	std::lock_guard<std::mutex> lock(m_filters_mutex);
	m_filters.insert(std::move(filter));
}

void
LogPrivate::remove_filter(std::shared_ptr<Filter> const& filter)
{
	std::lock_guard<std::mutex> lock(m_filters_mutex);
	m_filters.erase(filter);
}

std::vector<std::shared_ptr<Filter>>
LogPrivate::get_filters()
{
	std::lock_guard<std::mutex> lock(m_filters_mutex);

	std::vector<std::shared_ptr<Filter>> filters;

	filters.reserve(m_filters.size());

	for (auto const& filter : m_filters) {
		filters.push_back(filter);
	}
	return filters;
}

void
LogPrivate::set_sync_enabled(bool enable)
{
	ADT_DEBUG("Setting Sync enabled : %s\n", enable ? "true" : "false");

	m_sync_enabled = enable;

	std::lock_guard<std::mutex> lock(m_categories_mutex);

	for (auto const category : m_categories) {
		category->sync_enabled = enable;
	}
}

void
LogPrivate::set_trace_enabled(bool enable)
{
	ADT_DEBUG("Setting Trace enabled : %s\n", enable ? "true" : "false");

	m_trace_enabled = enable;

	std::lock_guard<std::mutex> lock(m_categories_mutex);

	for (auto const category : m_categories) {
		category->trace_enabled = enable;
	}

	GlobalAllocatorPrivate::get_instance().category.trace_enabled = enable;
}

void
LogPrivate::set_properties_enabled(bool enable)
{
	ADT_DEBUG("Setting Properties enabled : %s\n", enable ? "true" : "false");

	m_properties_enabled = enable;

	std::lock_guard<std::mutex> lock(m_categories_mutex);

	for (auto const category : m_categories) {
		category->properties_enabled = enable;
	}
}

void
LogPrivate::set_all_types_enabled(bool enable)
{
	set_message_type_enabled(enable);
	set_data_type_enabled(enable);
	set_function_type_enabled(enable);
	set_allocation_type_enabled(enable);
}

void
LogPrivate::set_message_type_enabled(bool enable)
{
	ADT_DEBUG("Setting Message Type enabled : %s\n", enable ? "true" : "false");

	m_message_type_enabled = enable;

	std::lock_guard<std::mutex> lock(m_categories_mutex);

	for (auto const category : m_categories) {
		category->message_type_enabled = enable;
	}
}

void
LogPrivate::set_data_type_enabled(bool enable)
{
	ADT_DEBUG("Setting Data Type enabled : %s\n", enable ? "true" : "false");

	m_data_type_enabled = enable;

	std::lock_guard<std::mutex> lock(m_categories_mutex);

	for (auto const category : m_categories) {
		category->data_type_enabled = enable;
	}
}

void
LogPrivate::set_function_type_enabled(bool enable)
{
	ADT_DEBUG("Setting Function Type enabled : %s\n", enable ? "true" : "false");

	m_function_type_enabled = enable;

	std::lock_guard<std::mutex> lock(m_categories_mutex);

	for (auto const category : m_categories) {
		category->function_type_enabled = enable;
	}
}

void
LogPrivate::set_allocation_type_enabled(bool enable)
{
	ADT_DEBUG("Setting Allocation Type enabled : %s\n", enable ? "true" : "false");

	m_allocation_type_enabled = enable;

	std::lock_guard<std::mutex> lock(m_categories_mutex);

	for (auto const category : m_categories) {
		category->allocation_type_enabled = enable;
	}
}

void
LogPrivate::on_iteration()
{
	process_records();
}

void
LogPrivate::process_records()
{
	LogRecord* log_record = nullptr;

	std::shared_ptr<Sink> sink(m_sink);

	ADT_DEBUG("Processing Log Records\n");

#ifdef ADT_ENABLE_INTERNAL_LOGGING
	LogRecord internal_log_record(
	    "adt::LogPrivate",
	    RecordType::FUNCTION_CALL,
	    Timing(Timing::Type::DURATION, TimestampSource::get_microseconds()),
	    __LINE__,
	    __FILE__,
	    A_STRFUNC);
#endif

	std::set<std::shared_ptr<Filter>> filters;
	bool const filters_enabled = m_filters_enabled;

	if (filters_enabled) {
		std::lock_guard<std::mutex> lock(m_filters_mutex);
		filters = m_filters;
	}

	while (m_record_queue.try_dequeue(log_record)) {
		auto record = std::make_shared<Record>(*log_record);

		if (filters_enabled) {
			bool matches = true;

			for (auto const& filter : filters) {
				if (!filter->matches(*log_record)) {
					matches = false;
					break;
				}
			}
			if (matches) {
				sink->handle_record_async(record);
				add_record_to_cache(record);
			}
		} else {
			sink->handle_record_async(record);
			add_record_to_cache(record);
		}

		delete log_record;
	}

	cache_flush_timeout();

#ifdef ADT_ENABLE_INTERNAL_LOGGING
	internal_log_record.timing.second = TimestampSource::get_microseconds();
	auto record = std::make_shared<Record>(internal_log_record);
	sink->handle_record_async(record);
#endif
}

void
LogPrivate::add_record_to_cache(std::shared_ptr<Record> const& record)
{
	if (m_cache_handler == nullptr) {
		return;
	}

	if (cache_full()) {
		m_cache_handler->handle_new_records(m_cache);
		m_cache.clear();
	}

	m_cache.push_back(record);
}

void
LogPrivate::cache_flush_timeout()
{
	if (m_cache_handler == nullptr) {
		return;
	}

	// Ensure that even if there are no more records being queued
	// that the records left in the cache are eventually flushed
	if (!m_cache.empty()) {
		std::unique_lock<std::mutex> lock(m_iter_mtx);
		auto const status = m_cond.wait_for(lock, std::chrono::milliseconds(100));

		if (status == std::cv_status::timeout) {
			m_cache_handler->handle_new_records(m_cache);
			m_cache.clear();
		}
		// Not timing out means that new records have been added and
		// process_records records will be called again.
	}
}

} // namespace adt
