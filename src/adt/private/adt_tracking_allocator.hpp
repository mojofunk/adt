#pragma once

#include <adt-private.hpp>

namespace adt {

class AdtTrackingAllocator : public Allocator
{
public:
	std::string get_name() const override { return "AdtTrackingAllocator"; }

	std::string get_type_name() const override { return get_name(); }

	bool is_from(void*) const override { return true; }

	void* allocate(std::size_t size) override
	{
		return TrackingAllocator::allocate(size);
	}

	void deallocate(void* ptr) override
	{
		return TrackingAllocator::deallocate(ptr);
	}

	using Allocator::deallocate;
};

} // namespace adt
