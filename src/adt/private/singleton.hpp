#pragma once

#include <adt-private.hpp>

namespace adt {

/**
 * Only use the adt::Singleton class within the adt module.
 *
 * This is because there seems to be an issue (at least on linux) where not all
 * Singleton::on_exit functors are called when another dynamic library has
 * registered a Singleton with the adt::AtExitManager.
 */
template <class Type>
class Singleton
{
	friend Type* Type::get_instance();

	static Type* get()
	{
		static Type* instance = initialize();
		return instance;
	}

	static Type* initialize()
	{
		Type* new_type = new Type();
		AtExitManager::on_exit(&Singleton<Type>::on_exit);
		return new_type;
	}

	static void on_exit() { delete get(); }
};

} // namespace adt

#define A_SINGLETON(ClassName)                                                 \
	A_DISALLOW_COPY_AND_ASSIGN(ClassName);                                        \
	friend class adt::Singleton<ClassName>;                                       \
	static ClassName* get_instance()
