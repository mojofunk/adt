#pragma once

#include <adt-private.hpp>

namespace adt {

class AtThreadExitManager
{
public:
	static void on_thread_exit(std::function<void()> func);
};

} // namespace adt
