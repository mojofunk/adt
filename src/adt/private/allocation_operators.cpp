#include "adt/private/source_includes.cpp"

void*
operator new(std::size_t bytes)
{
	return adt::GlobalAllocatorPrivate::get_instance().new_except(bytes);
}

void*
operator new[](std::size_t bytes)
{
	return adt::GlobalAllocatorPrivate::get_instance().new_array_except(bytes);
}

void*
operator new(std::size_t bytes, const std::nothrow_t& unused) noexcept
{
	(void)unused;

	return adt::GlobalAllocatorPrivate::get_instance().new_noexcept(bytes);
}

void*
operator new[](std::size_t bytes, const std::nothrow_t& unused) noexcept
{
	(void)unused;

	return adt::GlobalAllocatorPrivate::get_instance().new_array_noexcept(bytes);
}

#if 0 // TODO

#ifdef __cplusplus >= 201703L

void*
operator new(std::size_t count, std::align_val_t al);

void*
operator new[](std::size_t count, std::align_val_t al);

void*
operator new(std::size_t count,
             std::align_val_t al,
             const std::nothrow_t&);

void*
operator new[](std::size_t count, std::align_val_t al, const std::nothrow_t&);

#endif // __cplusplus >= 201703L

#endif // TODO

void
operator delete(void* ptr) noexcept
{
	adt::GlobalAllocatorPrivate::get_instance().delete_except(ptr);
}

void
operator delete[](void* ptr) noexcept
{
	adt::GlobalAllocatorPrivate::get_instance().delete_array_except(ptr);
}

void
operator delete(void* ptr, const std::nothrow_t& unused) noexcept
{
	(void)unused;

	adt::GlobalAllocatorPrivate::get_instance().delete_noexcept(ptr);
}

void
operator delete[](void* ptr, const std::nothrow_t& unused) noexcept
{
	(void)unused;

	adt::GlobalAllocatorPrivate::get_instance().delete_array_noexcept(ptr);
}

#if 0
#if __cplusplus >= 201402L

void
operator delete(void* ptr, std::size_t bytes) noexcept
{
	adt::GlobalAllocatorPrivate::get_instance().delete_noexcept(ptr);
}

void
operator delete[](void* ptr, std::size_t bytes) noexcept
{
	adt::GlobalAllocatorPrivate::get_instance().delete_noexcept(ptr);
}

#endif // __cplusplus >= 201402L
#endif

#if 0 // TODO

#if __cplusplus >= 201703L

void operator delete  ( void* ptr, std::align_val_t al );
void operator delete[]( void* ptr, std::align_val_t al );
void operator delete  ( void* ptr, std::size_t sz, std::align_val_t al );
void operator delete[]( void* ptr, std::size_t sz, std::align_val_t al );

#endif // __cplusplus >= 201703L

#endif // TODO
