#pragma once

#include <adt-private.hpp>

namespace adt {

/**
 * Add templates for allocator and Queue type
 */
class FixedSizeQueueMemoryPool
{
public:
	FixedSizeQueueMemoryPool(const std::size_t size_bytes, const std::size_t count)
	    : m_size_bytes(size_bytes)
	    , m_count(count)
	    , m_pool((char*)std::malloc(pool_size_bytes()))
	    , m_queue(count)
	{
		if (!m_pool) {
			throw std::bad_alloc();
		}

		for (char* pool_block = pool_start(); pool_block < pool_end();
		     pool_block += m_size_bytes) {
			bool enqueue = m_queue.try_enqueue(pool_block);
			(void)enqueue;
			assert(enqueue);
		}

		assert(available() == m_count);
	}

	~FixedSizeQueueMemoryPool()
	{
		assert(available() == m_count);
		std::free(m_pool);
	}

	// return true if allocated by pool
	bool is_from(void* ptr) const
	{
		return ((char*)ptr >= pool_start()) && ((char*)ptr < pool_end());
	}

	void* allocate(const std::size_t bytes)
	{
		if (bytes > m_size_bytes) {
			return nullptr;
		}

		char* mem = nullptr;

		if (!m_queue.try_dequeue(mem)) {
			return nullptr;
		}

		return (void*)mem;
	}

	void deallocate(void* ptr)
	{
		assert(is_from(ptr));

		char* tmp = (char*)ptr;
		bool enqueue = m_queue.try_enqueue(tmp);
		(void)enqueue;
		assert(enqueue);
	}

	bool empty() const { return available() == 0; }

	std::size_t max_size() const { return m_size_bytes; }

	std::size_t count() const { return m_count; }

	std::size_t available() const { return m_queue.read_available(); }

private: // methods
	char* pool_start() const { return m_pool; }
	char* pool_end() const { return pool_start() + pool_size_bytes(); }

	size_t pool_size_bytes() const { return m_size_bytes * m_count; }

private:
	std::size_t const m_size_bytes;
	std::size_t const m_count;
	char* const m_pool;

	FixedSizeQueue<char*, StdMallocAllocator<char*>> m_queue;
};

} // namespace adt
