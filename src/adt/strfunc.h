#if defined(__GNUC__)
#define A_STRFUNC ((const char*)(__PRETTY_FUNCTION__))
#elif defined(_MSC_VER)
#define A_STRFUNC ((const char*)(__FUNCSIG__))
#else
#define A_STRFUNC ((const char*)(__func__))
#endif
