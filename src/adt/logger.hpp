#pragma once

#include <adt.hpp>

namespace adt {

class Logger
{
public:
	Logger(LogCategory* const category)
	    : log_category(category)
	    , m_sync_enabled(category->sync_enabled)
	    , m_trace_enabled(category->trace_enabled)
	{
	}

public: // methods
	bool get_enabled() const { return log_record != nullptr; }

public: // members
	LogRecord* log_record{ nullptr };

protected:
	LogCategory const * const log_category;

	const bool m_sync_enabled;
	const bool m_trace_enabled;
};

} // namespace adt
