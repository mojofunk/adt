#pragma once

#include <adt.hpp>

namespace adt {

class LogAllocator
{
public: // API
	// must be called before using allocation functions
	static void initialize();

	static void* allocate(std::size_t bytes);

	static void deallocate(void* ptr, std::size_t bytes);

	static void* allocate_small_pool(std::size_t bytes);

	static void deallocate_small_pool(void* ptr, std::size_t bytes);

	static void* allocate_log_record_pool(std::size_t bytes);

	static void deallocate_log_record_pool(void* ptr, std::size_t bytes);

private: // Ctor
	LogAllocator();
};

} // namespace adt
