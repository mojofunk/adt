#ifdef ADT_STATIC

#define ADT_API
#define ADT_LOCAL

#else // default is shared

#ifdef _MSC_VER
#define ADT_EXPORT __declspec(dllexport)
#define ADT_IMPORT __declspec(dllimport)
#define ADT_LOCAL
#else
#define ADT_EXPORT __attribute__((visibility("default")))
#define ADT_IMPORT __attribute__((visibility("default")))
#define ADT_LOCAL __attribute__((visibility("hidden")))
#endif

#ifdef ADT_BUILDING_DLL
#define ADT_API ADT_EXPORT
#else
#define ADT_API ADT_IMPORT
#endif

#endif
