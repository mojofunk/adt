#pragma once

#include <adt.hpp>

namespace adt {

/**
 * The LogCategory represents a stream or channel of logging information.
 *
 * The logging records for a LogCategory can be further partitioned into
 * separate types.
 */
class LogCategory
{
public:
	LogCategory(const char* const p_name)
	    : name(p_name)
	{
	}

	~LogCategory() = default;

	const char* const name;

	// These are all overriden by Log after construction anyway
	bool enabled = false;

	bool trace_enabled = false;
	bool sync_enabled = false;
	bool properties_enabled = false;

	bool message_type_enabled = false;
	bool data_type_enabled = false;
	bool function_type_enabled = false;
	bool allocation_type_enabled = false;
};

} // namespace adt
