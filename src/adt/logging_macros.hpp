#define A_LOGGING_ENABLED

#ifndef A_LOGGING_PROPERTIES_ENABLED
#define A_LOGGING_PROPERTIES_ENABLED 1
#endif

#define A_DECLARE_LOG_CATEGORY(VariableName)                                   \
	extern adt::LogCategory* VariableName;

#define A_DEFINE_LOG_CATEGORY(VariableName, LogCategoryName)                   \
	adt::LogCategory* VariableName = adt::Log::make_category(LogCategoryName);

#define A_LOG_MSG(LogCategoryPtr, Message)                                     \
	{                                                                             \
		adt::MessageLogger message_logger(                                           \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                          \
		if (message_logger.get_enabled()) {                                          \
			message_logger.log_record->move_to_property_1(                              \
			    adt::RecordTypeNames::message, Message);                              \
		}                                                                            \
	}

#define A_LOG_CLASS_MSG(LogCategoryPtr, Message)                               \
	{                                                                             \
		adt::MessageLogger message_logger(                                           \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (message_logger.get_enabled()) {                                          \
			message_logger.log_record->move_to_property_1(                              \
			    adt::RecordTypeNames::message, Message);                              \
		}                                                                            \
	}

#define A_LOG_DURATION(LogCategoryPtr, Message)                                \
	adt::DurationLogger A_TOKEN_PASTE(duration_logger, __LINE__)(                 \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (A_TOKEN_PASTE(duration_logger, __LINE__).get_enabled()) {                 \
		A_TOKEN_PASTE(duration_logger, __LINE__)                                     \
		    .log_record->move_to_property_1(adt::RecordTypeNames::message,         \
		                                    Message);                                \
	}

#define A_LOG_CLASS_DURATION(LogCategoryPtr, Message)                          \
	adt::DurationLogger A_TOKEN_PASTE(duration_logger, __LINE__)(                 \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (A_TOKEN_PASTE(duration_logger, __LINE__).get_enabled()) {                 \
		A_TOKEN_PASTE(duration_logger, __LINE__)                                     \
		    .log_record->move_to_property_1(adt::RecordTypeNames::message,         \
		                                    Message);                                \
	}

#define A_LOG_CALL(LogCategoryPtr)                                             \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);

#define A_LOG_CALL1(LogCategoryPtr, Arg1)                                      \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
	}

#define A_LOG_CALL2(LogCategoryPtr, Arg1, Arg2)                                \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
	}

#define A_LOG_CALL3(LogCategoryPtr, Arg1, Arg2, Arg3)                          \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
	}

#define A_LOG_CALL4(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4)                    \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
	}

#define A_LOG_CALL5(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5)              \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
		function_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);     \
	}

#define A_LOG_CALL6(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)        \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
		function_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);     \
		function_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);     \
	}

#define A_LOG_CALL7(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)  \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
		function_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);     \
		function_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);     \
		function_logger.log_record->copy_to_property_7(A_STRINGIFY(Arg7), Arg7);     \
	}

#define A_LOG_CALL8(                                                           \
    LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)            \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);                           \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
		function_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);     \
		function_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);     \
		function_logger.log_record->copy_to_property_7(A_STRINGIFY(Arg7), Arg7);     \
		function_logger.log_record->copy_to_property_8(A_STRINGIFY(Arg8), Arg8);     \
	}

#define A_LOG_CLASS_CALL(LogCategoryPtr)                                       \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));

#define A_LOG_CLASS_CALL1(LogCategoryPtr, Arg1)                                \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
	}

#define A_LOG_CLASS_CALL2(LogCategoryPtr, Arg1, Arg2)                          \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
	}

#define A_LOG_CLASS_CALL3(LogCategoryPtr, Arg1, Arg2, Arg3)                    \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
	}

#define A_LOG_CLASS_CALL4(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4)              \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
	}

#define A_LOG_CLASS_CALL5(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5)        \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
		function_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);     \
	}

#define A_LOG_CLASS_CALL6(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
		function_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);     \
		function_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);     \
	}

#define A_LOG_CLASS_CALL7(                                                     \
    LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)                  \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
		function_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);     \
		function_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);     \
		function_logger.log_record->copy_to_property_7(A_STRINGIFY(Arg7), Arg7);     \
	}

#define A_LOG_CLASS_CALL8(                                                     \
    LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)            \
	adt::FunctionLogger function_logger(                                          \
	    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));     \
	if (function_logger.get_enabled() && LogCategoryPtr->properties_enabled) {    \
		function_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);     \
		function_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);     \
		function_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);     \
		function_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);     \
		function_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);     \
		function_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);     \
		function_logger.log_record->copy_to_property_7(A_STRINGIFY(Arg7), Arg7);     \
		function_logger.log_record->copy_to_property_8(A_STRINGIFY(Arg8), Arg8);     \
	}

#define A_LOG_DATA1(LogCategoryPtr, Arg1)                                      \
	{                                                                             \
		adt::DataLogger data_logger(LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);  \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
		}                                                                            \
	}

#define A_LOG_DATA2(LogCategoryPtr, Arg1, Arg2)                                \
	{                                                                             \
		adt::DataLogger data_logger(LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);  \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
		}                                                                            \
	}

#define A_LOG_DATA3(LogCategoryPtr, Arg1, Arg2, Arg3)                          \
	{                                                                             \
		adt::DataLogger data_logger(LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);  \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
		}                                                                            \
	}

#define A_LOG_DATA4(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4)                    \
	{                                                                             \
		adt::DataLogger data_logger(LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);  \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
		}                                                                            \
	}

#define A_LOG_DATA5(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5)              \
	{                                                                             \
		adt::DataLogger data_logger(LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);  \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
			data_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);        \
		}                                                                            \
	}

#define A_LOG_DATA6(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)        \
	{                                                                             \
		adt::DataLogger data_logger(LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);  \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
			data_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);        \
			data_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);        \
		}                                                                            \
	}

#define A_LOG_DATA7(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)  \
	{                                                                             \
		adt::DataLogger data_logger(LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);  \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
			data_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);        \
			data_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);        \
			data_logger.log_record->copy_to_property_7(A_STRINGIFY(Arg7), Arg7);        \
		}                                                                            \
	}

#define A_LOG_DATA8(                                                           \
    LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)            \
	{                                                                             \
		adt::DataLogger data_logger(LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC);  \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
			data_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);        \
			data_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);        \
			data_logger.log_record->copy_to_property_7(A_STRINGIFY(Arg7), Arg7);        \
			data_logger.log_record->copy_to_property_8(A_STRINGIFY(Arg8), Arg8);        \
		}                                                                            \
	}

#define A_LOG_CLASS_DATA1(LogCategoryPtr, Arg1)                                \
	{                                                                             \
		adt::DataLogger data_logger(                                                 \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
		}                                                                            \
	}

#define A_LOG_CLASS_DATA2(LogCategoryPtr, Arg1, Arg2)                          \
	{                                                                             \
		adt::DataLogger data_logger(                                                 \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
		}                                                                            \
	}

#define A_LOG_CLASS_DATA3(LogCategoryPtr, Arg1, Arg2, Arg3)                    \
	{                                                                             \
		adt::DataLogger data_logger(                                                 \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
		}                                                                            \
	}

#define A_LOG_CLASS_DATA4(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4)              \
	{                                                                             \
		adt::DataLogger data_logger(                                                 \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
		}                                                                            \
	}

#define A_LOG_CLASS_DATA5(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5)        \
	{                                                                             \
		adt::DataLogger data_logger(                                                 \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
			data_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);        \
		}                                                                            \
	}

#define A_LOG_CLASS_DATA6(LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
	{                                                                             \
		adt::DataLogger data_logger(                                                 \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
			data_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);        \
			data_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);        \
		}                                                                            \
	}

#define A_LOG_CLASS_DATA7(                                                     \
    LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)                  \
	{                                                                             \
		adt::DataLogger data_logger(                                                 \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
			data_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);        \
			data_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);        \
			data_logger.log_record->copy_to_property_7(A_STRINGIFY(Arg7), Arg7);        \
		}                                                                            \
	}

#define A_LOG_CLASS_DATA8(                                                     \
    LogCategoryPtr, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)            \
	{                                                                             \
		adt::DataLogger data_logger(                                                 \
		    LogCategoryPtr, __LINE__, __FILE__, A_STRFUNC, adt::void_cast(this));    \
		if (data_logger.get_enabled() && LogCategoryPtr->properties_enabled) {       \
			data_logger.log_record->copy_to_property_1(A_STRINGIFY(Arg1), Arg1);        \
			data_logger.log_record->copy_to_property_2(A_STRINGIFY(Arg2), Arg2);        \
			data_logger.log_record->copy_to_property_3(A_STRINGIFY(Arg3), Arg3);        \
			data_logger.log_record->copy_to_property_4(A_STRINGIFY(Arg4), Arg4);        \
			data_logger.log_record->copy_to_property_5(A_STRINGIFY(Arg5), Arg5);        \
			data_logger.log_record->copy_to_property_6(A_STRINGIFY(Arg6), Arg6);        \
			data_logger.log_record->copy_to_property_7(A_STRINGIFY(Arg7), Arg7);        \
			data_logger.log_record->copy_to_property_8(A_STRINGIFY(Arg8), Arg8);        \
		}                                                                            \
	}

#define A_LOG_ENABLED(LogCategoryPtr) LogCategoryPtr->enabled

#define A_FMT(...) adt::format(adt::LogStringAllocator<char>(), __VA_ARGS__)
