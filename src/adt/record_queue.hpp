#pragma once

#include <adt.hpp>

namespace adt
{

class RecordQueuePrivate;

/*
	A single writer, single reader queue.
*/
class RecordQueue
{
public:
	RecordQueue(std::size_t size);

	~RecordQueue();

	/// @return true if record successfully enqueued.
	/// Only fails (returns false) if memory allocation fails.
	bool enqueue(std::shared_ptr<Record> const& record);

	/// @return true if record successfully enqueued
	/// Does not allocate memory
	bool try_enqueue(std::shared_ptr<Record> const& record);

	/// @return true if record successfully dequeued
	bool try_dequeue(std::shared_ptr<Record>& record);

private:
	std::unique_ptr<RecordQueuePrivate> d_ptr;
};

} // namespace adt
