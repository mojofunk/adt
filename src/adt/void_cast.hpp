#pragma once

#include <adt.hpp>

namespace adt {

template <typename T>
static inline void*
void_cast(T* ptr)
{
	return reinterpret_cast<void*>(ptr);
}

template <typename T>
static inline void*
void_cast(const T* ptr)
{
	return reinterpret_cast<void*>(const_cast<T*>(ptr));
}

} // namespace adt
