#define A_REGISTER_THREAD(Name, Priority)                                      \
	adt::Threads::set_this_thread_info(std::make_shared<adt::ThreadInfo>(         \
	    std::this_thread::get_id(), Name, Priority));

#define A_REGISTER_THREAD_NAME(Name)                                           \
	adt::Threads::get_this_thread_info()->name = Name;

#define A_REGISTER_THREAD_PRIORITY(Priority)                                   \
	adt::Threads::get_this_thread_info()->priority = Priority;
