#pragma once

#include <adt.hpp>

namespace adt {

class StdFile : public adt::File
{
public:
	StdFile(std::string const& filename)
	    : m_filename(filename)
	{
	}

public:
	/**
	 * @return true If opened successfully clears state flags and sets
	 * them to goodbit, or failbit will be set if opening failed.
	 */
	bool open() override
	{
		m_ofstream.open(m_filename, std::ios::trunc);
		return m_ofstream.good();
	}

	bool close() override
	{
		m_ofstream.close();
		// failbit will be set if closing failed.
		return m_ofstream.good();
	}

	bool write(std::string const& str) override
	{
		if (!m_ofstream.is_open()) {
			return false;
		}

		m_ofstream << str;

		return m_ofstream.good();
	}

private:
	std::string const m_filename;
	std::ofstream m_ofstream;

private:
	A_DISALLOW_COPY_AND_ASSIGN(StdFile);
};

} // namespace adt
