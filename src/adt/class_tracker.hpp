#pragma once

#include <adt.hpp>

namespace adt {

class ClassTracker
{
public: // types
	using UniqueTraceCountMap = std::map<std::shared_ptr<StackTrace>, int>;

protected:
	friend class ClassTrackerManagerPrivate;
	virtual ~ClassTracker() {}

public: // interface
	/**
	 * @return name of class being tracked.
	 */
	virtual const char* get_tracked_class_name() const = 0;

	/**
	 * @return size of class being tracked.
	 */
	virtual std::size_t get_tracked_class_size() const = 0;

	/**
	 * @return the current instance count for the class being tracked
	 */
	virtual uint32_t get_tracked_class_count() const = 0;

	/**
	 * @return the amount of times all classes of the type being tracked have been
	 * copied.
	 */
	virtual uint32_t get_tracked_class_copied_count() const = 0;

	/**
	 * @return the amount of times all classes of the type being tracked have been
	 * copied.
	 */
	virtual uint32_t get_tracked_class_moved_count() const = 0;

	/**
	 * Reset all the counters for a class so that any existing class instances
	 * will no longer be tracked so that only instances created after reset will
	 * be tracked.
	 */
	virtual void reset_tracking() = 0;

	/**
	 * Get the logger of the class being tracked
	 */
	virtual LogCategory* get_tracked_class_category() const = 0;

	/**
	 * Get unique creation traces
	 */
	virtual UniqueTraceCountMap get_unique_traces() const = 0;
};

} // namespace adt
