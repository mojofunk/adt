#pragma once

#include <adt.hpp>

namespace adt {

class Timing
{
public:
	enum class Type { INSTANT, BEGIN, END, DURATION };

	Timing(Type p_type, uint64_t p_first, uint64_t p_second = 0)
	    : type(p_type)
	    , first(p_first)
	    , second(p_second)
	{
	}

	Type type;
	uint64_t first;
	uint64_t second;

	uint64_t duration() const { return second > first ? second - first : 0; }

	static std::string type_to_string(Type const type);
};

} // namespace adt
