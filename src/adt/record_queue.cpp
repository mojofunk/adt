#include "adt/private/source_includes.cpp"

namespace adt
{

class RecordQueuePrivate
{
public:
	RecordQueuePrivate(std::size_t size)
	    : m_queue(size)
	{
	}

	using RecordQueueType = moodycamel::ReaderWriterQueue<std::shared_ptr<Record>>;
	RecordQueueType m_queue;
};

RecordQueue::RecordQueue(std::size_t size)
    : d_ptr(new RecordQueuePrivate(size))
{
}

RecordQueue::~RecordQueue() {}

bool
RecordQueue::enqueue(std::shared_ptr<Record> const& record)
{
	return d_ptr->m_queue.enqueue(record);
}

bool
RecordQueue::try_enqueue(std::shared_ptr<Record> const& record)
{
	return d_ptr->m_queue.try_enqueue(record);
}

bool
RecordQueue::try_dequeue(std::shared_ptr<Record>& record)
{
	return d_ptr->m_queue.try_dequeue(record);
}

} // namespace adt
