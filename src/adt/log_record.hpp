#pragma once

#include <adt.hpp>

namespace adt {

class LogRecord : public RecordBase
{
public: // ctors
	LogRecord(const LogCategory* const p_category,
	          const RecordType p_type,
	          const Timing& p_timing,
	          int line,
	          const char* const file_path,
	          const char* const function_str,
	          void* this_ptr = nullptr)
	    : RecordBase(p_category,
	                 p_type,
	                 std::this_thread::get_id(),
	                 p_timing,
	                 line,
	                 file_path,
	                 function_str,
	                 this_ptr)
	{
	}

	~LogRecord()
	{
		if (property_1 != nullptr) {
			property_1->~LogProperty();
		}
		if (property_2 != nullptr) {
			property_2->~LogProperty();
		}
		if (property_3 != nullptr) {
			property_3->~LogProperty();
		}
		if (property_4 != nullptr) {
			property_4->~LogProperty();
		}
		if (property_5 != nullptr) {
			property_5->~LogProperty();
		}
		if (property_6 != nullptr) {
			property_6->~LogProperty();
		}
		if (property_7 != nullptr) {
			property_7->~LogProperty();
		}
		if (property_8 != nullptr) {
			property_8->~LogProperty();
		}
	}

public: // class new/delete
	void* operator new(size_t bytes);

	void operator delete(void* ptr);

public: // Optional Properties
	template <class T>
	void copy_to_property_1(const char* const property_1_name, T property_1_value)
	{
		assert(property_1 == nullptr);
		property_1 = new ((void*)property_1_mem)
		    LogProperty(property_1_name, property_1_value);
	}

	template <class T>
	void move_to_property_1(const char* const property_1_name,
	                        T&& property_1_value)
	{
		assert(property_1 == nullptr);
		property_1 = new ((void*)property_1_mem)
		    LogProperty(property_1_name, std::move(property_1_value));
	}

	template <class T>
	void copy_to_property_2(const char* const property_2_name, T property_2_value)
	{
		assert(property_2 == nullptr);
		property_2 = new ((void*)property_2_mem)
		    LogProperty(property_2_name, property_2_value);
	}

	template <class T>
	void move_to_property_2(const char* const property_2_name,
	                        T&& property_2_value)
	{
		assert(property_2 == nullptr);
		property_2 = new ((void*)property_2_mem)
		    LogProperty(property_2_name, std::move(property_2_value));
	}

	template <class T>
	void copy_to_property_3(const char* const property_3_name, T property_3_value)
	{
		assert(property_3 == nullptr);
		property_3 = new ((void*)property_3_mem)
		    LogProperty(property_3_name, property_3_value);
	}

	template <class T>
	void move_to_property_3(const char* const property_3_name,
	                        T&& property_3_value)
	{
		assert(property_3 == nullptr);
		property_3 = new ((void*)property_3_mem)
		    LogProperty(property_3_name, std::move(property_3_value));
	}

	template <class T>
	void copy_to_property_4(const char* const property_4_name, T property_4_value)
	{
		assert(property_4 == nullptr);
		property_4 = new ((void*)property_4_mem)
		    LogProperty(property_4_name, property_4_value);
	}

	template <class T>
	void move_to_property_4(const char* const property_4_name,
	                        T&& property_4_value)
	{
		assert(property_4 == nullptr);
		property_4 = new ((void*)property_4_mem)
		    LogProperty(property_4_name, std::move(property_4_value));
	}

	template <class T>
	void copy_to_property_5(const char* const property_5_name, T property_5_value)
	{
		assert(property_5 == nullptr);
		property_5 = new ((void*)property_5_mem)
		    LogProperty(property_5_name, property_5_value);
	}

	template <class T>
	void move_to_property_5(const char* const property_5_name,
	                        T&& property_5_value)
	{
		assert(property_5 == nullptr);
		property_5 = new ((void*)property_5_mem)
		    LogProperty(property_5_name, std::move(property_5_value));
	}

	template <class T>
	void copy_to_property_6(const char* const property_6_name, T property_6_value)
	{
		assert(property_6 == nullptr);
		property_6 = new ((void*)property_6_mem)
		    LogProperty(property_6_name, property_6_value);
	}

	template <class T>
	void move_to_property_6(const char* const property_6_name,
	                        T&& property_6_value)
	{
		assert(property_6 == nullptr);
		property_6 = new ((void*)property_6_mem)
		    LogProperty(property_6_name, std::move(property_6_value));
	}

	template <class T>
	void copy_to_property_7(const char* const property_7_name, T property_7_value)
	{
		assert(property_7 == nullptr);
		property_7 = new ((void*)property_7_mem)
		    LogProperty(property_7_name, property_7_value);
	}

	template <class T>
	void move_to_property_7(const char* const property_7_name,
	                        T&& property_7_value)
	{
		assert(property_7 == nullptr);
		property_7 = new ((void*)property_7_mem)
		    LogProperty(property_7_name, std::move(property_7_value));
	}

	template <class T>
	void copy_to_property_8(const char* const property_8_name, T property_8_value)
	{
		assert(property_8 == nullptr);
		property_8 = new ((void*)property_8_mem)
		    LogProperty(property_8_name, property_8_value);
	}

	template <class T>
	void move_to_property_8(const char* const property_8_name,
	                        T&& property_8_value)
	{
		assert(property_8 == nullptr);
		property_8 = new ((void*)property_8_mem)
		    LogProperty(property_8_name, std::move(property_8_value));
	}

public:
	LogProperty* property_1 = nullptr;
	char property_1_mem[sizeof(LogProperty)];

	LogProperty* property_2 = nullptr;
	char property_2_mem[sizeof(LogProperty)];

	LogProperty* property_3 = nullptr;
	char property_3_mem[sizeof(LogProperty)];

	LogProperty* property_4 = nullptr;
	char property_4_mem[sizeof(LogProperty)];

	LogProperty* property_5 = nullptr;
	char property_5_mem[sizeof(LogProperty)];

	LogProperty* property_6 = nullptr;
	char property_6_mem[sizeof(LogProperty)];

	LogProperty* property_7 = nullptr;
	char property_7_mem[sizeof(LogProperty)];

	LogProperty* property_8 = nullptr;
	char property_8_mem[sizeof(LogProperty)];

private:
	A_DISALLOW_COPY_AND_ASSIGN(LogRecord);
};

} // namespace adt
