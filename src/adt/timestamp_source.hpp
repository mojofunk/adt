#pragma once

#include <adt.hpp>

namespace adt {

class TimestampSource
{
public:
	static uint64_t get_microseconds();

	// Must be called before timestamps can be generated
	static void initialize();

private:
	TimestampSource();
};

} // namespace adt
