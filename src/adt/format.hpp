#pragma once

#include <adt.hpp>

namespace adt {

LogString
vformat(LogStringAllocator<char> alloc,
        fmt::string_view format_str,
        fmt::format_args args);

template <typename... Args>
inline LogString
format(LogStringAllocator<char> alloc,
       fmt::string_view format_str,
       const Args&... args)
{
	return vformat(alloc, format_str, fmt::make_format_args(args...));
}

} // namespace adt
