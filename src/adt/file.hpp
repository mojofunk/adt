#pragma once

#include <adt.hpp>

namespace adt {

class File
{
public:
	virtual ~File() = default;

public:
	virtual bool open() = 0;
	virtual bool close() = 0;

	virtual bool write(std::string const& str) = 0;
};

} // namespace adt
