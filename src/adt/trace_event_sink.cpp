#include "adt/private/source_includes.cpp"

namespace adt {

TraceEventSink::TraceEventSink()
    : d_ptr(new TraceEventSinkPrivate)
{
}

TraceEventSink::~TraceEventSink() {} // NOLINT

void
TraceEventSink::handle_record_async(const std::shared_ptr<Record>& record)
{
	d_ptr->handle_record_async(record);
}

void
TraceEventSink::handle_record_sync(const std::shared_ptr<Record>& record)
{
	d_ptr->handle_record_sync(record);
}

void
TraceEventSink::set_output_file(std::shared_ptr<File> output_file)
{
	d_ptr->set_output_file(output_file);
}

bool
TraceEventSink::get_enabled() const
{
	return d_ptr->get_enabled();
}

void
TraceEventSink::set_enabled(bool enable)
{
	d_ptr->set_enabled(enable);
}

bool
TraceEventSink::is_internal_thread(std::thread::id thread_id)
{
	return d_ptr->is_internal_thread(thread_id);
}

} // namespace adt
