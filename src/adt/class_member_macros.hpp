#define A_DECLARE_CLASS_MEMBERS(ClassType)                                     \
	static const char* get_class_name();                                          \
	static adt::LogCategory* class_category;                                      \
	friend class adt::ClassTrackerMember<ClassType>;                              \
	adt::ClassTrackerMember<ClassType> class_tracker##__LINE__;

#define A_DEFINE_CLASS_MEMBERS(ClassType)                                      \
	const char* ClassType::get_class_name() { return #ClassType; }                \
	adt::LogCategory* ClassType::class_category =                                 \
	    adt::Log::make_category(ClassType::get_class_name());

#define A_DEFINE_CLASS_AS_MEMBERS(ClassType, ClassName)                        \
	const char* ClassType::get_class_name() { return ClassName; }                 \
	adt::LogCategory* ClassType::class_category =                                 \
	    adt::Log::make_category(ClassType::get_class_name());
