#pragma once

#include <adt.hpp>

namespace adt {

using Variant = VariantTemplate<std::string>;

} // namespace adt
