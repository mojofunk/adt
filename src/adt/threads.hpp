#pragma once

#include <adt.hpp>

namespace adt {

class Threads
{
public: // API
	// Set the info for the currently executing thread
	static void set_this_thread_info(std::shared_ptr<ThreadInfo> const& thread_info);

	// Get the info of this thread.
	static std::shared_ptr<ThreadInfo> get_this_thread_info();

	// Get the ThreadInfo of a thread given a thread::id or null if it
	// hasn't been set
	static std::shared_ptr<ThreadInfo> get_thread_info(std::thread::id const&);

	// Get all the ThreadInfo currently registered
	static bool get_all_thread_info(std::vector<std::shared_ptr<ThreadInfo>>&);

	static uint32_t get_cpu_id();

	static std::string to_string(std::thread::id const&);
};

} // namespace adt
