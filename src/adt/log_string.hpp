#pragma once

#include <adt.hpp>

namespace adt {

using LogString =
    std::basic_string<char, std::char_traits<char>, LogStringAllocator<char>>;

} // namespace adt
