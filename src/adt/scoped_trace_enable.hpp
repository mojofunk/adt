#pragma once

#include <adt.hpp>

namespace adt {

/**
 * Enable traces for log records for all record types for the duration
 * of the instance.
 *
 * @post tracing will be disabled, regardless of whether is was
 * previously enabled.
 */
struct ScopedTraceEnable {
	ScopedTraceEnable() { Log::set_trace_enabled(true); }

	~ScopedTraceEnable() { Log::set_trace_enabled(false); }
};

} // namespace adt
