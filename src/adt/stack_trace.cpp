#include "adt/private/source_includes.cpp"

#ifdef HAVE_EXECINFO_H
#include <execinfo.h>
#endif

namespace adt {

#ifdef HAVE_DBGHELP_H
#pragma warning (push)
#pragma warning (disable:4091) // a microsoft header has warnings. Very nice.
#include <dbghelp.h>
#pragma warning (pop)

class SymbolContext
{
public:
	static SymbolContext* get_instance()
	{
		// SymbolContext may be accessed during process termination so leak this...for now.
		static auto context = new SymbolContext();
		return context;
	}

private:
	SymbolContext() { init(); }

	static bool init()
	{
		SymSetOptions(SYMOPT_DEFERRED_LOADS | SYMOPT_UNDNAME | SYMOPT_LOAD_LINES);

		if (!SymInitialize(GetCurrentProcess(), NULL, TRUE)) { // NOLINT
			// TODO convert error code to string/return failure();
			std::cout << "SymInitialize failed: " << GetLastError();
			return false;
		}

#if 0
		// It doesn't seem necessary to add the executable directory to the symbol search path.
		// Symbol lookup still works after relocating the folder containing the binary
		// but I'm leaving this code in place in case it is needed under certain conditions
		// that I haven't tested.
		std::size_t const symbols_array_size = 1024;
		std::unique_ptr<wchar_t[]> symbols_path(new wchar_t[symbols_array_size]);

		if (!SymGetSearchPathW(
			GetCurrentProcess(), symbols_path.get(), symbols_array_size)) {
			std::cout << "SymGetSearchPath failed: " << GetLastError();
			return false;
		}

		wchar_t system_buffer[MAX_PATH];
		GetModuleFileNameW(NULL, system_buffer, MAX_PATH);
		system_buffer[MAX_PATH - 1] = L'\0';
		std::wstring module_dir(system_buffer);

		module_dir = module_dir.substr(0, module_dir.find_last_of(L"\\"));

		std::wstring new_path(std::wstring(symbols_path.get()) + L";" + module_dir);

		if (!SymSetSearchPathW(GetCurrentProcess(), new_path.c_str())) {
			std::cout << "SymSetSearchPath failed." << GetLastError();
			return false;
		}
#endif
		return true;
	}

public: // Methods
	std::vector<std::shared_ptr<StackFrame>>
	get_stack_frames(void* const* trace, std::size_t const size)
	{
		std::vector<std::shared_ptr<StackFrame>> stack_frames;

		std::lock_guard<std::mutex> lock(m_sym_mutex);

		for (size_t i = 0; i < size; ++i) {
			const int max_name_length = 256;
			auto frame_ptr = reinterpret_cast<DWORD_PTR>(trace[i]);

			// Code adapted from MSDN example:
			// http://msdn.microsoft.com/en-us/library/ms680578(VS.85).aspx
			ULONG64 buffer[(sizeof(SYMBOL_INFO) + max_name_length * sizeof(wchar_t) +
			                sizeof(ULONG64) - 1) /
			               sizeof(ULONG64)];
			std::memset(buffer, 0, sizeof(buffer));

			// Initialize symbol information retrieval structures.
			DWORD64 sym_displacement = 0;
			auto symbol_info = reinterpret_cast<PSYMBOL_INFO>(&buffer[0]);
			symbol_info->SizeOfStruct = sizeof(SYMBOL_INFO);
			symbol_info->MaxNameLen = max_name_length - 1;
			BOOL has_symbol = SymFromAddr(
			    GetCurrentProcess(), frame_ptr, &sym_displacement, symbol_info);

			// Attempt to retrieve line number information.
			DWORD line_displacement = 0;
			IMAGEHLP_LINE64 line = {};
			line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
			BOOL has_line = SymGetLineFromAddr64(
			    GetCurrentProcess(), frame_ptr, &line_displacement, &line);

			auto stack_frame = std::make_shared<StackFrame>(trace[i]);

			if (has_symbol) { // NOLINT
				stack_frame->set_function_name(symbol_info->Name);
			}
			if (has_line) { // NOLINT
				stack_frame->set_file_name(line.FileName);
				stack_frame->set_line_number(line.LineNumber);
			}
			stack_frames.emplace_back(std::move(stack_frame));
		}
		return stack_frames;
	}

private:
	// DbgHelp Sym* family of functions may only be invoked by one thread at a time.
	std::mutex m_sym_mutex;
};

#endif // HAVE_DBGHELP_H

StackTrace::StackTrace(uint16_t offset)
    : m_offset(offset)
{
#if defined(HAVE_EXECINFO_H)

	auto const count = backtrace(m_trace, static_cast<int>(s_max_traces));
	m_count = static_cast<size_t>(count);

#elif defined(HAVE_DBGHELP_H)

	m_count = CaptureStackBackTrace(0, (DWORD)s_max_traces, m_trace, NULL); // NOLINT

#endif

}

void*
StackTrace::operator new(size_t bytes)
{
	return std::malloc(bytes);
}

void
StackTrace::operator delete(void* ptr)
{
	std::free(ptr);
}

void
StackTrace::to_stream(std::ostream& os, size_t start_offset) const
{
	auto const stack_frames = get_stack_frames(start_offset);

	for (auto const& stack_frame : stack_frames) {
		if (stack_frame->has_function_name()) {
			os << stack_frame->get_function_name();
		} else {
			os << stack_frame->get_address();
		}
		if (stack_frame->has_file_name()) {
			os << " : ";
			os << stack_frame->get_file_name();
		}
		if (stack_frame->has_line_number()) {
			os << " : ";
			os << stack_frame->get_line_number();
		}
		os << std::endl;
	}
}

std::string
StackTrace::to_string(size_t start_offset) const
{
	std::ostringstream oss;
	to_stream(oss, start_offset);
	return oss.str();
}

std::vector<std::shared_ptr<StackFrame>>
StackTrace::get_stack_frames(std::size_t start_offset) const
{
	std::size_t const total_offset = m_offset + start_offset;
	std::size_t const address_count = m_count - total_offset;

	assert(total_offset < m_count);

#ifdef HAVE_EXECINFO_H
	std::vector<std::shared_ptr<StackFrame>> stack_frames;

	assert(address_count <= std::numeric_limits<int>::max());

	std::unique_ptr<char*, FreeDeleter> trace_symbols(backtrace_symbols(
	    m_trace + total_offset, static_cast<int>(address_count)));

	for (size_t i = total_offset; i < address_count; ++i) {
		auto stack_frame = std::make_shared<StackFrame>(m_trace[i]);
		stack_frame->set_function_name(demangle(trace_symbols.get()[i]));
		stack_frames.emplace_back(std::move(stack_frame));
	}
	return stack_frames;
#elif defined(HAVE_DGBHELP_H)
	return SymbolContext::get_instance()->get_stack_frames(m_trace + total_offset,
	                                                       address_count);
#endif
	return {};
}

} // namespace adt
