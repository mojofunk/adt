#pragma once

#include <adt.hpp>

namespace adt {

using RecordSPVec = std::vector<std::shared_ptr<Record>>;

class RecordCacheHandler
{
public:
	virtual ~RecordCacheHandler();

public:
	virtual void handle_new_records(RecordSPVec const&) = 0;
	virtual std::size_t preferred_cache_size() = 0;
};

class Log
{
public: // Master Control
	static bool get_enabled();

	static void set_enabled(bool enable = true);

public: // Sink
	static void set_sink(std::shared_ptr<Sink> sink_ptr);

	static std::shared_ptr<Sink> get_sink();

public: // Categories
	static LogCategory* make_category(const char* const name);

	static std::set<LogCategory*> get_categories();

	static std::vector<std::string> get_category_names();

	static void set_all_categories_enabled(bool enable = true);

	static void set_categories_enabled(std::vector<std::string> const&,
	                                   bool enable);

public: // Presets
	static void add_preset(std::shared_ptr<Preset>);

	static void remove_preset(std::shared_ptr<Preset> const&);

	static void set_preset_enabled(std::shared_ptr<Preset>);

	static void set_preset_disabled(std::shared_ptr<Preset>);

	static std::vector<std::shared_ptr<Preset>> get_presets();

public: // Cache

	static void set_cache_handler(RecordCacheHandler* handler);

	static void reset_cache_handler();

public: // Filters
	static void set_filters_enabled(bool enable = true);

	static bool get_filters_enabled();

	static void add_filter(std::shared_ptr<Filter>);

	static void remove_filter(std::shared_ptr<Filter> const&);

	static std::vector<std::shared_ptr<Filter>> get_filters();

public: // Global controls
	static void set_sync_enabled(bool enable = true);

	static bool get_sync_enabled();

	static void set_trace_enabled(bool enable = true);

	static bool get_trace_enabled();

	static void set_properties_enabled(bool enable = true);

	static bool get_properties_enabled();

public: // Global record type controls
	static void set_all_types_enabled(bool enable = true);

	static void set_message_type_enabled(bool enable = true);

	static bool get_message_type_enabled();

	static void set_data_type_enabled(bool enable = true);

	static bool get_data_type_enabled();

	static void set_function_type_enabled(bool enable = true);

	static bool get_function_type_enabled();

	static void set_allocation_type_enabled(bool enable = true);

	static bool get_allocation_type_enabled();

private: // Ctors
	Log();
};

} // namespace adt
