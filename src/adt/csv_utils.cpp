#include "adt/private/source_includes.cpp"

namespace adt {

bool
write_records_to_csv_file(File& file,
                          std::vector<std::shared_ptr<Record>> const& records,
                          CSVOptions const& options)
{
	if (!file.open()) {
		return false;
	}

	std::vector<std::string> enabled_properties;

	{
		std::string str;

		if (options.add_timestamp) {
			str += "Timestamp, ";
		}

		if (options.add_duration) {
			str += "Duration, ";
		}

		for (std::size_t i = 0; i < options.record->properties.size(); ++i) {

			bool wrote_property = false;

			if (options.properties_enabled[i]) {
				auto const& prop_name = options.record->properties[i].name;
				str += prop_name;
				enabled_properties.push_back(prop_name);
				wrote_property = true;
			}

			auto next_property_index = i;
			++next_property_index;

			if (next_property_index != options.record->properties.size()) {
				if (wrote_property && !options.is_last_property_enabled(i)) {
					str += ", ";
				}
			} else {
				str += "\n";
			}
		}

		file.write(str);
	}

	for (const auto& record : records) {
		auto const property_indexes = record->get_properties(enabled_properties);

		if (property_indexes.empty()) {
			continue;
		}

		std::string str;

		if (options.add_timestamp) {
			str += std::to_string(record->timing.first);
			str += ", ";
		}

		if (options.add_duration) {
			str += std::to_string(record->timing.duration());
			str += ", ";
		}

		for (std::size_t i = 0; i < property_indexes.size();) {
			str += record->properties[property_indexes[i]].value.to_string();

			if (++i != property_indexes.size()) {
				str += ", ";
			} else {
				str += "\n";
			}
		}
		file.write(str);
	}
	return true;
}

} // namespace adt
