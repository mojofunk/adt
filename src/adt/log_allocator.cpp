#include "adt/private/source_includes.cpp"

namespace adt {

class LogAllocatorPrivate
{
public: // ctors
	LogAllocatorPrivate()
	    : m_small_pool(s_small_pool_size, s_small_pool_count)
	    , m_log_record_pool(s_log_record_pool_size, s_log_record_pool_count)
	{
		assert(s_instance == nullptr);
		// cache pointer for optimization
		s_instance = this;

		ADT_DEBUG_ALLOCATOR("adt::LogAllocator: Creating small pool with blocks of "
		                    "size (%zu) bytes and count (%zu)\n",
		                    s_small_pool_size,
		                    s_small_pool_count);

		ADT_DEBUG_ALLOCATOR("adt::LogAllocator: Creating record pool with blocks of "
		                    "size (%zu) bytes and count (%zu)\n",
		                    s_log_record_pool_size,
		                    s_log_record_pool_count);
	}

	void* allocate(std::size_t bytes)
	{
		void* ptr = m_small_pool.allocate(bytes);

		if (ptr != nullptr) {
			ADT_DEBUG_ALLOCATOR(
			    "adt::LogAllocator: allocated (%zu) bytes from small pool(%d) : %p\n",
			    bytes,
			    m_small_pool.available(),
			    ptr);
			return ptr;
		}

		ptr = m_log_record_pool.allocate(bytes);

		if (ptr != nullptr) {
			ADT_DEBUG_ALLOCATOR(
			    "adt::LogAllocator: allocated (%zu) bytes from record pool(%d) : %p\n",
			    bytes,
			    m_log_record_pool.available(),
			    ptr);
			return ptr;
		}

		ptr = std::malloc(bytes);
		fprintf(stderr,
		        "adt::LogAllocator: Allocated (%zu) bytes using malloc : %p\n",
		        bytes,
		        ptr);

#ifdef ADT_ENABLE_DEBUG_ALLOCATOR
		StackTrace trace;
		trace.print_to_file(stderr);
#endif

		return ptr;
	}

	void deallocate(void* ptr, std::size_t bytes)
	{
		(void)bytes;

		if (m_small_pool.is_from(ptr)) {
			m_small_pool.deallocate(ptr);
			ADT_DEBUG_ALLOCATOR(
			    "LogAllocator: deallocate (%zu) bytes into small pool(%d) : %p\n",
			    bytes,
			    m_small_pool.available(),
			    ptr);
			return;
		}

		if (m_log_record_pool.is_from(ptr)) {
			m_log_record_pool.deallocate(ptr);
			ADT_DEBUG_ALLOCATOR(
			    "LogAllocator: deallocate (%zu) bytes into record pool(%d) : %p\n",
			    bytes,
			    m_log_record_pool.available(),
			    ptr);
			return;
		}

		ADT_DEBUG_ALLOCATOR(
		    "LogAllocator: deallocate (%zu) bytes using std::free : %p\n",
		    bytes,
		    ptr);
		std::free(ptr);
	}

	void* allocate_small_pool(std::size_t bytes)
	{
		return allocate_from_pool(m_small_pool, bytes);
	}

	void deallocate_small_pool(void* ptr, std::size_t bytes)
	{
		deallocate_from_pool(m_small_pool, ptr, bytes);
	}

	void* allocate_log_record_pool(std::size_t bytes)
	{
		// Should only be called by the LogRecord::operator new
		assert(bytes == sizeof(LogRecord));
		return allocate_from_pool(m_log_record_pool, bytes);
	}

	void deallocate_log_record_pool(void* ptr, std::size_t bytes)
	{
		assert(bytes == sizeof(LogRecord));
		deallocate_from_pool(m_log_record_pool, ptr, bytes);
	}

	void* allocate_from_pool(MTMemoryPool& pool, std::size_t bytes)
	{
		void* ptr = nullptr;

		ptr = pool.allocate(bytes);

		if (ptr != nullptr) {
			return ptr;
		}

		ptr = std::malloc(bytes);

		fprintf(stderr,
		        "adt::LogAllocator: Allocated (%zu) bytes using malloc : %p\n",
		        bytes,
		        ptr);

		return ptr;
	}

	void deallocate_from_pool(MTMemoryPool& pool, void* ptr, std::size_t bytes)
	{
		(void)bytes;

		if (pool.is_from(ptr)) {
			pool.deallocate(ptr);
			return;
		}
		std::free(ptr);
	}

	MTMemoryPool m_small_pool;
	MTMemoryPool m_log_record_pool;

	static std::size_t const s_small_pool_size = 128;
	static std::size_t const s_small_pool_count = 8192;

	static std::size_t const s_log_record_pool_size = sizeof(LogRecord);
	static std::size_t const s_log_record_pool_count = 4096;

public:
	A_SINGLETON(LogAllocatorPrivate);

	static LogAllocatorPrivate* s_instance; // cached
};

LogAllocatorPrivate* LogAllocatorPrivate::s_instance = nullptr;

// Singleton interface
LogAllocatorPrivate*
LogAllocatorPrivate::get_instance()
{
	return Singleton<LogAllocatorPrivate>::get();
}

void
LogAllocator::initialize()
{
	LogAllocatorPrivate::get_instance();
}

void*
LogAllocator::allocate(std::size_t bytes)
{
	return LogAllocatorPrivate::s_instance->allocate(bytes);
}

void
LogAllocator::deallocate(void* ptr, std::size_t bytes)
{
	LogAllocatorPrivate::s_instance->deallocate(ptr, bytes);
}

void*
LogAllocator::allocate_small_pool(std::size_t bytes)
{
	return LogAllocatorPrivate::s_instance->allocate_small_pool(bytes);
}

void
LogAllocator::deallocate_small_pool(void* ptr, std::size_t bytes)
{
	LogAllocatorPrivate::s_instance->deallocate_small_pool(ptr, bytes);
}

void*
LogAllocator::allocate_log_record_pool(std::size_t bytes)
{
	return LogAllocatorPrivate::s_instance->allocate_log_record_pool(bytes);
}

void
LogAllocator::deallocate_log_record_pool(void* ptr, std::size_t bytes)
{
	LogAllocatorPrivate::s_instance->deallocate_log_record_pool(ptr, bytes);
}

} // namespace adt
