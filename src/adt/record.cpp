#include "adt/private/source_includes.cpp"

namespace adt {

Property
Record::convert_to_property(const LogProperty& log_property)
{
	switch (log_property.value.type) {
	case VariantType::POINTER:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.pointer_value));
	case VariantType::BOOL:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.bool_value));
	case VariantType::FLOAT:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.float_value));
	case VariantType::DOUBLE:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.double_value));
	case VariantType::INT32:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.int32_value));
	case VariantType::UINT32:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.uint32_value));
	case VariantType::INT64:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.int64_value));
	case VariantType::UINT64:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.uint64_value));
	case VariantType::STRING:
		return Property(log_property.name_c_str,
		                Variant(log_property.value.string_value.c_str()));
	default: // VariantType::NONE
		return Property();
	}
}

std::string
Record::to_string(bool include_properties) const
{
	std::ostringstream oss;

	oss << "Record :: Category : " << category
	    << " Type : " << adt::RecordTypeNames::get_name(type)
	    << " Thread : " << Threads::to_string(thread_id)
	    << " Duration : " << timing.duration()
	    << " File : " << src_location.file_path << " Line : " << src_location.line
	    << " Function : " << src_location.function_name;

	if (include_properties) {
		oss << properties_to_string();
	}
	return oss.str();
}

std::string
Record::properties_to_string() const
{
	std::string prop_string;

	for (auto i = properties.begin(); i != properties.end();) {
		// these record types only use the first property
		if (type == adt::RecordType::MESSAGE ||
		    type == adt::RecordType::ALLOCATION) {
			prop_string = i->value.to_string();
		} else {
			prop_string += i->name + " : " + i->value.to_string();
		}

		if (++i != properties.end()) {
			prop_string += ", ";
		}
	}
	return prop_string;
}

std::vector<std::size_t>
Record::get_properties(std::vector<std::string> const& names) const
{
	std::vector<std::size_t> indexes;

	for (size_t i = 0; i < properties.size(); ++i) {
		for (auto const& name : names) {
			if (properties[i].name == name) {
				indexes.push_back(i);
				break;
			}
		}
	}

	if (names.size() != indexes.size()) {
		return {};
	}
	return indexes;
}

} // namespace adt
