#include "adt/private/source_includes.cpp"

namespace adt {

void
OStreamSink::handle_record_async(const std::shared_ptr<Record>& record)
{
	if (!get_enabled()) {
		return;
	}
	handle_record_sync(record);
}

void
OStreamSink::handle_record_sync(const std::shared_ptr<Record>& record)
{
	if (!get_enabled()) {
		return;
	}

	std::string property_string;

	for (auto const& property : record->properties) {
		property_string += property.name + " : " + property.value.to_string() + " ";
	}

	std::cout << "LogCategory: " << record->category
	          << ", Log Message: " << property_string
	          << ", Thread ID: " << record->thread_id
	          << ", Timestamp: " << record->timing.first
	          << ", File: " << get_filename_from_path(record->src_location.file_path)
	          << ", Line: " << record->src_location.line << ", Function: "
	          << short_function_name(record->src_location.function_name)
	          << std::endl;
}

} // namespace adt
