#pragma once

#include <adt.hpp>

namespace adt {

class ScopedClassTracker
{
public:
	ScopedClassTracker(adt::ClassTracker const& tracker)
	    : m_class_tracker(tracker)
	    , m_initial_instance_count(tracker.get_tracked_class_count())
	    , m_initial_copied_count(tracker.get_tracked_class_copied_count())
	    , m_initial_moved_count(tracker.get_tracked_class_moved_count())
	{
	}

public:
	int64_t get_instance_count_difference() noexcept
	{
		return (int64_t)m_class_tracker.get_tracked_class_count() - m_initial_instance_count ;
	}

	int64_t get_copied_count_difference() noexcept
	{
		return (int64_t)m_class_tracker.get_tracked_class_copied_count() -
		       m_initial_copied_count;
	}

	int64_t get_moved_count_difference() noexcept
	{
		return (int64_t)m_class_tracker.get_tracked_class_moved_count() -
		       m_initial_moved_count;
	}

private:
	ClassTracker const& m_class_tracker;

	uint32_t m_initial_instance_count;
	uint32_t m_initial_copied_count;
	uint32_t m_initial_moved_count;
};

} // namespace adt
