#include "adt/private/source_includes.cpp"

namespace adt {

std::string
short_function_name(const std::string& function_name)
{
	auto function_name_pos = function_name.find_first_of(' ');

	if (function_name_pos != std::string::npos) {
		// move pos to first character in function name.
		++function_name_pos;
	} else {
		// no return type, ctor etc
		function_name_pos = 0;
	}

	auto remove_return_type = function_name.substr(function_name_pos, std::string::npos);

	// remove parameter list
	auto end_pos = remove_return_type.find_first_of('(');

	if (end_pos == 0) {
		end_pos = remove_return_type.find_first_of('(', 1);
	}
	return remove_return_type.substr(0, end_pos);
}

} // namespace adt
