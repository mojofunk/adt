#include "adt/private/source_includes.cpp"

namespace adt {

MessageLogger::MessageLogger(LogCategory* const category,
                             int line,
                             const char* const file_path,
                             const char* const function_str,
                             void* this_ptr)
    : Logger(category)
{
	if (!category->enabled || !category->message_type_enabled) {
		return;
	}

	log_record = new LogRecord(
	    category,
	    RecordType::MESSAGE,
	    Timing(Timing::Type::INSTANT, TimestampSource::get_microseconds()),
	    line,
	    file_path,
	    function_str,
	    this_ptr);

	if (m_trace_enabled) {
		log_record->stack_trace.reset(new StackTrace(2));
	}
}

MessageLogger::~MessageLogger()
{
	if (log_record == nullptr) {
		return;
	}

	if (m_sync_enabled) {
		LogPrivate::s_instance->write_record_sync(*log_record);
		delete log_record;
	} else {
		LogPrivate::s_instance->write_record_async(log_record);
	}
}

} // namespace adt
