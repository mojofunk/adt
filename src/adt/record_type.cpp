#include "adt/private/source_includes.cpp"

namespace adt {

const char* const RecordTypeNames::message = "Message";
const char* const RecordTypeNames::data = "Data";
const char* const RecordTypeNames::function = "Function call";
const char* const RecordTypeNames::allocation = "Allocation";

const char*
RecordTypeNames::get_name(RecordType const type)
{
	static const char* const names[] = { message, data, function, allocation };
	return names[(int)type];
}

} // namespace adt
