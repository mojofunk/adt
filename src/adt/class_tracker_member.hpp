#pragma once

#include <adt.hpp>

namespace adt {

template <class TrackedClassType>
class ClassTrackerMember
{

	class ClassTrackerCollector : public ClassTracker
	{
	public: // ctors
		ClassTrackerCollector()
		    : m_instance_count(0)
		    , m_copied_count(0)
		    , m_moved_count(0)
		    , m_class_category(Log::make_category(TrackedClassType::get_class_name()))
		{
			assert(m_class_category);
			ClassTrackerManager::add_class_tracker(this);
		}

		~ClassTrackerCollector() { reset_stack_traces(); }

	public: // ClassTracker interface
		const char* get_tracked_class_name() const override
		{
			return TrackedClassType::get_class_name();
		}

		std::size_t get_tracked_class_size() const override
		{
			return sizeof(TrackedClassType);
		}

		uint32_t get_tracked_class_count() const override { return m_instance_count; }

		uint32_t get_tracked_class_copied_count() const override
		{
			return m_copied_count;
		}

		uint32_t get_tracked_class_moved_count() const override
		{
			return m_moved_count;
		}

		void reset_tracking() override
		{
			m_instance_count = 0;
			m_copied_count = 0;
			m_moved_count = 0;
			reset_stack_traces();
		}

		LogCategory* get_tracked_class_category() const override
		{
			return m_class_category;
		}

		ClassTracker::UniqueTraceCountMap get_unique_traces() const override
		{
			ClassTracker::UniqueTraceCountMap unique_trace_map;

			std::lock_guard<std::mutex> guard(m_creation_trace_map_mutex);

			for (auto const& instance_and_trace : m_creation_trace_map) {
				std::shared_ptr<StackTrace> stack_trace = instance_and_trace.second;
				bool unique = true;
				for (auto& trace_and_count : unique_trace_map) {
					std::shared_ptr<StackTrace> unique_trace = trace_and_count.first;
					// compare the actual traces via StackTrace::operator==()
					if (*stack_trace == *unique_trace) {
						// increase the unique trace count
						trace_and_count.second++;
						unique = false;
						break;
					}
				}
				if (unique) {
					unique_trace_map.insert(std::make_pair(stack_trace, 1));
				}
			}
			return unique_trace_map;
		}

	public: // methods
		void class_created(ClassTrackerMember* ptr)
		{
			++m_instance_count;

			{
				ClassTrackerLogger logger(m_class_category,
				                          __LINE__,
				                          __FILE__,
				                          A_STRFUNC,
				                          nullptr); // see above

				if (logger.get_enabled()) {
					logger.log_record->move_to_property_1(
					    RecordTypeNames::allocation,
					    A_FMT("class created of type: {}, current instance count = {}",
					          get_tracked_class_name(),
					          m_instance_count));
					if (m_class_category->trace_enabled) {
						StackTrace* trace = new StackTrace(3);
						logger.set_stack_trace(trace);
						{
							std::lock_guard<std::mutex> guard(m_creation_trace_map_mutex);
							// copy the trace
							m_creation_trace_map.emplace(ptr, std::make_shared<StackTrace>(*trace));
						}
					}
				}
			}
		}

		void class_copied(ClassTrackerMember* ptr)
		{
			++m_instance_count;

			++m_copied_count;

			{
				ClassTrackerLogger logger(m_class_category,
				                          __LINE__,
				                          __FILE__,
				                          A_STRFUNC,
				                          nullptr); // see above

				if (logger.get_enabled()) {
					logger.log_record->move_to_property_1(
					    RecordTypeNames::allocation,
					    A_FMT("class copied of type: {}, current instance count = {}",
					          get_tracked_class_name(),
					          m_instance_count));
					if (m_class_category->trace_enabled) {
						StackTrace* trace = new StackTrace(3);
						logger.set_stack_trace(trace);
						{
							std::lock_guard<std::mutex> guard(m_creation_trace_map_mutex);
							// copy the trace
							m_creation_trace_map.emplace(ptr, std::make_shared<StackTrace>(*trace));
						}
					}
				}
			}
		}

		void class_moved(ClassTrackerMember* ptr)
		{
			++m_instance_count;

			++m_moved_count;

			{
				ClassTrackerLogger logger(m_class_category,
				                          __LINE__,
				                          __FILE__,
				                          A_STRFUNC,
				                          nullptr); // see above

				if (logger.get_enabled()) {
					logger.log_record->move_to_property_1(
					    RecordTypeNames::allocation,
					    A_FMT("class moved of type: {}, current instance count = {}",
					          get_tracked_class_name(),
					          m_instance_count));
					if (m_class_category->trace_enabled) {
						StackTrace* trace = new StackTrace(3);
						logger.set_stack_trace(trace);
						{
							std::lock_guard<std::mutex> guard(m_creation_trace_map_mutex);
							// copy the trace
							m_creation_trace_map.emplace(ptr, std::make_shared<StackTrace>(*trace));
						}
					}
				}
			}
		}

		void class_destroyed(ClassTrackerMember* ptr)
		{
			--m_instance_count;

			{
				ClassTrackerLogger logger(m_class_category,
				                          __LINE__,
				                          __FILE__,
				                          A_STRFUNC,
				                          nullptr); // see above

				if (logger.get_enabled()) {
					logger.log_record->move_to_property_1(
					    RecordTypeNames::allocation,
					    A_FMT("class destroyed of type: {}, current instance count = {}",
					          get_tracked_class_name(),
					          m_instance_count));
					if (m_class_category->trace_enabled) {
						logger.set_stack_trace(new StackTrace(3));
						{
							std::lock_guard<std::mutex> guard(m_creation_trace_map_mutex);
							m_creation_trace_map.erase(ptr);
						}
					}
				}
			}
		}

		void reset_stack_traces()
		{
			std::lock_guard<std::mutex> guard(m_creation_trace_map_mutex);
			m_creation_trace_map.clear();
		}

	private: // data
		std::atomic<uint32_t> m_instance_count;
		std::atomic<uint32_t> m_copied_count;
		std::atomic<uint32_t> m_moved_count;

		using TraceMap = std::map<ClassTrackerMember*, std::shared_ptr<StackTrace>>;

		mutable std::mutex m_creation_trace_map_mutex;
		TraceMap m_creation_trace_map;

		LogCategory* const m_class_category;
	};

	static ClassTrackerCollector& get_collector()
	{
		// managed by ClassTrackerManager
		static ClassTrackerCollector* collector = new ClassTrackerCollector();
		return *collector;
	}

public:
	static ClassTracker const& get_class_tracker() { return get_collector(); }

public:
	ClassTrackerMember() { get_collector().class_created(this); }

	ClassTrackerMember(const ClassTrackerMember&)
	{
		get_collector().class_copied(this);
	}

	ClassTrackerMember(ClassTrackerMember&&)
	{
		get_collector().class_moved(this);
	}

	ClassTrackerMember& operator=(const ClassTrackerMember&)
	{
		get_collector().class_copied(this);
		return *this;
	}

	ClassTrackerMember& operator=(const ClassTrackerMember&&)
	{
		get_collector().class_moved(this);
		return *this;
	}

	~ClassTrackerMember() { get_collector().class_destroyed(this); }
};

} // namespace adt
