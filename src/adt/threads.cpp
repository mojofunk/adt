#include "adt/private/source_includes.cpp"

namespace adt {

/**
 * ThreadInfo for a thread is stored in TLS for quick lock free access
 * from within the executing thread and in a Registry for access by
 * other threads.
 */
class ThreadsPrivate
{
public:
	static void set_this_thread_info(std::shared_ptr<ThreadInfo> const& info);

	static std::shared_ptr<ThreadInfo>& get_this_thread_info();

	static std::shared_ptr<ThreadInfo> get_thread_info(std::thread::id tid);

	static ThreadLocalDataRegistry<std::shared_ptr<ThreadInfo>>& get_registry();

private:
	static void on_thread_exit(std::thread::id id);
};

std::shared_ptr<ThreadInfo>&
ThreadsPrivate::get_this_thread_info()
{
#ifdef __MINGW32__
#warning "thread_local is broken using this toolchain"
	// XXX leaking seems like the only option
	thread_local std::shared_ptr<ThreadInfo>* tls_thread_info =
	    new std::shared_ptr<ThreadInfo>();
	return *tls_thread_info;
#else
	thread_local std::shared_ptr<ThreadInfo> tls_thread_info;
	return tls_thread_info;
#endif
}

ThreadLocalDataRegistry<std::shared_ptr<ThreadInfo>>&
ThreadsPrivate::get_registry()
{
	static ThreadLocalDataRegistry<std::shared_ptr<ThreadInfo>> s_registry;
	return s_registry;
}

void
ThreadsPrivate::set_this_thread_info(std::shared_ptr<ThreadInfo> const& info)
{
	get_this_thread_info() = info;

	get_registry().register_thread_data(info);

	// Support setting a null Info
	if (info) {
		assert(info->id == std::this_thread::get_id());
	}
	AtThreadExitManager::on_thread_exit(
	    std::bind(&on_thread_exit, std::this_thread::get_id()));
}

std::shared_ptr<ThreadInfo>
ThreadsPrivate::get_thread_info(std::thread::id tid)
{
	return get_registry().get_thread_data(tid);
}

void
ThreadsPrivate::on_thread_exit(std::thread::id id)
{
	(void)id;

	ADT_DEBUG("Thread: %s exited...\n",
	          get_registry().get_thread_data(id)->name.c_str());
	get_registry().unregister_thread_data();
}

void
Threads::set_this_thread_info(std::shared_ptr<ThreadInfo> const& thread_info)
{
	ThreadsPrivate::set_this_thread_info(thread_info);
}

std::shared_ptr<ThreadInfo>
Threads::get_thread_info(std::thread::id const& id)
{
	return ThreadsPrivate::get_thread_info(id);
}

std::shared_ptr<ThreadInfo>
Threads::get_this_thread_info()
{
	auto this_thread_info = ThreadsPrivate::get_this_thread_info();

	if(!this_thread_info) {
		this_thread_info = std::make_shared<ThreadInfo>();
		this_thread_info->id = std::this_thread::get_id();
		ThreadsPrivate::set_this_thread_info(this_thread_info);
	}

	return this_thread_info;
}

bool
Threads::get_all_thread_info(
    std::vector<std::shared_ptr<ThreadInfo>>& all_thread_info)
{
	return ThreadsPrivate::get_registry().get_all_thread_data(all_thread_info);
}

uint32_t
Threads::get_cpu_id()
{
#ifdef HAVE_SCHED_GETCPU
	return static_cast<uint32_t>(sched_getcpu());
#elif defined(_WIN32)
	// Supported in Windows version >= Vista
	return GetCurrentProcessorNumber();
#else
	return 0;
#endif
}

std::string
Threads::to_string(std::thread::id const& id)
{
	std::ostringstream oss;
	oss << id;
	return oss.str();
}

} // namespace adt
