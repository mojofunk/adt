#include "adt/private/source_includes.cpp"

namespace adt
{

void
init()
{
	LogPrivate::initialize();
	ClassTrackerManagerPrivate::get_instance();
}

void
cleanup()
{
	AtExitManager::execute_functors_now();
}

} // namespace adt
