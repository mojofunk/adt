#include "adt/private/source_includes.cpp"

namespace adt {

ClassTrackerLogger::ClassTrackerLogger(LogCategory* const category,
                                       int line,
                                       const char* const file_path,
                                       const char* const function_str,
                                       void* this_ptr)
    : Logger(category)
{
	if (!category->enabled || !category->allocation_type_enabled) {
		return;
	}

	log_record = new LogRecord(
	    category,
	    RecordType::ALLOCATION,
	    Timing(Timing::Type::INSTANT, TimestampSource::get_microseconds()),
	    line,
	    file_path,
	    function_str,
	    this_ptr);
}

ClassTrackerLogger::~ClassTrackerLogger()
{
	if (log_record == nullptr) {
		return;
	}

	if (m_sync_enabled) {
		LogPrivate::s_instance->write_record_sync(*log_record);
		delete log_record;
	} else {
		LogPrivate::s_instance->write_record_async(log_record);
	}
}

} // namespace adt
