#pragma once

#include <adt.hpp>

namespace adt {

class StackTrace
{
public: // Ctors
	StackTrace(uint16_t offset = 1);

	StackTrace(StackTrace const&) = default;

	StackTrace& operator=(StackTrace const&) = default;

public: // class new/delete
	void* operator new(size_t bytes);

	void operator delete(void* ptr);

public: // Methods
	/**
	 * Get addresses contained in stacktrace
	 * @return addresses or NULL
	 * @param count number of elements/addresses in returned array
	 */
	void* const* get_addresses(size_t& count) const
	{
		count = m_count - m_offset;
		if (m_count) {
			return m_trace + m_offset;
		}
		return NULL;
	}

	/**
	 * @return true if stacktraces are equivalent
	 *
	 * This compares all the frame addresses in the stack
	 */
	bool operator==(const StackTrace& other)
	{
		if (m_count != other.m_count) return false;

		for (size_t i = m_offset; i < m_count; ++i) {
			if (m_trace[i] != other.m_trace[i]) {
				return false;
			}
		}
		return true;
	}

	bool operator!=(const StackTrace& other) { return !operator==(other); }

	/**
	 * @param os stream to output stacktrace
	 * @param start_offset stack frames to offset output
	 */
	void to_stream(std::ostream& os, size_t start_offset = 0) const;

	/**
	 * @param start_offset stack frames to offset output
	 * @return string containing stacktrace
	 */
	std::string to_string(size_t start_offset = 0) const;

	std::vector<std::shared_ptr<StackFrame>>
	get_stack_frames(std::size_t start_offset = 0) const;

protected: // Data
	static const size_t s_max_traces = 62;

	void* m_trace[s_max_traces];

	uint16_t m_offset;
	std::size_t m_count{ 0ull };
};

} // namespace adt
