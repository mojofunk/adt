#define A_DISALLOW_COPY(TypeName) TypeName(const TypeName&) = delete

#define A_DISALLOW_ASSIGN(TypeName) void operator=(const TypeName&) = delete

#define A_DISALLOW_COPY_AND_ASSIGN(TypeName)                                   \
	TypeName(const TypeName&) = delete;                                           \
	void operator=(const TypeName&) = delete
