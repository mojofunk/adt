#pragma once

#include <adt.hpp>

namespace adt {

class OStreamSink : public Sink
{
public:
	void handle_record_async(const std::shared_ptr<Record>& record) override;

	void handle_record_sync(const std::shared_ptr<Record>& record) override;

	bool get_enabled() const override { return m_enabled; }

	void set_enabled(bool enable) override { m_enabled = enable; }

private:
	std::atomic<bool> m_enabled{ true };
};

} // namespace adt
