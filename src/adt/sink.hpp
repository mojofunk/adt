#pragma once

#include <adt.hpp>

namespace adt {

class Sink
{
public:
	virtual ~Sink(){};

public:
	/**
	 * Records should be handled in an asyncronous manner.
	 */
	virtual void handle_record_async(const std::shared_ptr<Record>& record) = 0;

	/**
	 * Records are required to be handled in a syncronous manner.
	 */
	virtual void handle_record_sync(const std::shared_ptr<Record>& record) = 0;

	virtual bool get_enabled() const = 0;

	virtual void set_enabled(bool) = 0;

	/**
	 * This is used to prevent infinite recursion when logging allocation.
	 * The default of true means that Sinks that don't implement threading
	 * will not be able to log allocation records by default.
	 */
	virtual bool is_internal_thread(std::thread::id) { return true; }
};

} // namespace adt
