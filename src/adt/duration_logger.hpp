#pragma once

#include <adt.hpp>

namespace adt {

class DurationLogger : public Logger
{
public:
	DurationLogger(LogCategory* const category,
	               int line,
	               const char* const file_path,
	               const char* const function_str,
	               void* this_ptr = nullptr);

	~DurationLogger();
};

} // namespace adt
