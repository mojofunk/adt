#pragma once

#include <adt.hpp>

namespace adt {

class TraceEventSinkPrivate;

class TraceEventSink : public Sink
{
public: // ctors
	TraceEventSink();

	~TraceEventSink();

public: // Sink interface
	void handle_record_async(const std::shared_ptr<Record>& record) override;

	void handle_record_sync(const std::shared_ptr<Record>& record) override;

	bool get_enabled() const override;

	void set_enabled(bool) override;

	bool is_internal_thread(std::thread::id) override;

public:
	void set_output_file(std::shared_ptr<File> output_file);

private:
	A_DISALLOW_COPY_AND_ASSIGN(TraceEventSink);

	std::unique_ptr<TraceEventSinkPrivate> d_ptr;
};

} // namespace adt
