#pragma once

#include <adt.hpp>

namespace adt {

class RecordBase
{
protected:
	RecordBase(const LogCategory * const p_category,
	           const RecordType p_type,
	           const std::thread::id p_thread_id,
	           const Timing& p_timing,
	           int line,
	           const char* const file_path,
	           const char* const function_str,
	           void* p_this_ptr = nullptr)
	    : category(p_category)
	    , type(p_type)
	    , thread_id(p_thread_id)
	    , timing(p_timing)
	    , src_location(line, file_path, function_str)
	    , this_ptr(p_this_ptr)
	{
	}

public:
	const LogCategory* const category;

	const RecordType type;

	const std::thread::id thread_id;

	Timing timing;

	const SourceFileLocation src_location;

	// Optional Data

	void* this_ptr;

	std::unique_ptr<StackTrace> stack_trace;
};

} // namespace adt
