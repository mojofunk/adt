#pragma once

#include <adt.hpp>

namespace adt {

/**
 * Enable all logging categories and log record types for the duration
 * of the instance.
 *
 * @post all categories and record types will be disabled, regardless of
 * whether they were enabled previously.
 */
struct ScopedLogEnable {
	ScopedLogEnable()
	{
		Log::set_all_categories_enabled(true);
		Log::set_all_types_enabled(true);
	}

	~ScopedLogEnable()
	{
		Log::set_all_categories_enabled(false);
		Log::set_all_types_enabled(false);
	}
};

} // namespace adt
