#pragma once

#include <adt.hpp>

namespace adt {

class GlobalAllocator
{
public:
	static bool get_logging_enabled();
	static void set_logging_enabled(bool enable = true);

public:
	/**
	 * @pre Any changes to the global allocator are syncronized externally.
	 *
	 * The allocator must be reset before setting another and all
	 * allocations must be deallocated before setting another.
	 *
	 * So it should really only be done once at the start of program
	 * execution when there is only a single thread of execution.
	*/
	static void set_allocator(std::shared_ptr<Allocator> alloc);

	static void reset_allocator();

private:
	A_DISALLOW_COPY_AND_ASSIGN(GlobalAllocator);
};

} // namespace adt
