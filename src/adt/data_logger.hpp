#pragma once

#include <adt.hpp>

namespace adt {

class DataLogger : public Logger
{
public:
	DataLogger(LogCategory* const category,
	           int line,
	           const char* const file_path,
	           const char* const function_str,
	           void* this_ptr = nullptr);

	~DataLogger();
};

} // namespace adt
