#include "adt/private/source_includes.cpp"

namespace adt {

using adt_fmt_memory_buffer =
    fmt::basic_memory_buffer<char,
                             fmt::inline_buffer_size,
                             LogStringAllocator<char>>;

LogString
vformat(LogStringAllocator<char> alloc,
        fmt::string_view format_str,
        fmt::format_args args)
{
	adt_fmt_memory_buffer buf(alloc);
	fmt::vformat_to(buf, format_str, args);
	return LogString(buf.data(), buf.size(), alloc);
}

} // namespace adt
