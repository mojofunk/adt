#include "adt/private/source_includes.cpp"

namespace adt {

RecordCacheHandler::~RecordCacheHandler() {}

bool
Log::get_enabled()
{
	return LogPrivate::get_instance()->get_enabled();
}

void
Log::set_enabled(bool enable)
{
	LogPrivate::get_instance()->set_enabled(enable);
}

void
Log::set_sink(std::shared_ptr<Sink> sink_ptr)
{
	LogPrivate::get_instance()->set_sink(sink_ptr);
}

std::shared_ptr<Sink>
Log::get_sink()
{
	return LogPrivate::get_instance()->get_sink();
}

LogCategory*
Log::make_category(const char* const name)
{
	return LogPrivate::get_instance()->make_category(name);
}

std::set<LogCategory*>
Log::get_categories()
{
	return LogPrivate::get_instance()->get_categories();
}

std::vector<std::string>
Log::get_category_names()
{
	return LogPrivate::get_instance()->get_category_names();
}

void
Log::set_all_categories_enabled(bool enable)
{
	LogPrivate::get_instance()->set_all_categories_enabled(enable);
}

void
Log::set_categories_enabled(std::vector<std::string> const& categories,
                            bool enable)
{
	LogPrivate::get_instance()->set_categories_enabled(categories, enable);
}

void
Log::add_preset(std::shared_ptr<Preset> preset)
{
	LogPrivate::get_instance()->add_preset(preset);
}

void
Log::remove_preset(std::shared_ptr<Preset> const& preset)
{
	LogPrivate::get_instance()->remove_preset(preset);
}

void
Log::set_preset_enabled(std::shared_ptr<Preset> preset)
{
	LogPrivate::get_instance()->set_preset_enabled(preset);
}

void
Log::set_preset_disabled(std::shared_ptr<Preset> preset)
{
	LogPrivate::get_instance()->set_preset_disabled(preset);
}

std::vector<std::shared_ptr<Preset>>
Log::get_presets()
{
	return LogPrivate::get_instance()->get_presets();
}

void
Log::set_cache_handler(RecordCacheHandler* handler)
{
	LogPrivate::get_instance()->set_cache_handler(handler);
}

void
Log::reset_cache_handler()
{
	LogPrivate::get_instance()->reset_cache_handler();
}

void
Log::set_filters_enabled(bool enable)
{
	LogPrivate::get_instance()->set_filters_enabled(enable);
}

bool
Log::get_filters_enabled()
{
	return LogPrivate::get_instance()->get_filters_enabled();
}

void
Log::add_filter(std::shared_ptr<Filter> filter)
{
	LogPrivate::get_instance()->add_filter(std::move(filter));
}

void
Log::remove_filter(std::shared_ptr<Filter> const& filter)
{
	LogPrivate::get_instance()->remove_filter(filter);
}

std::vector<std::shared_ptr<Filter>>
Log::get_filters()
{
	return LogPrivate::get_instance()->get_filters();
}

bool
Log::get_trace_enabled()
{
	return LogPrivate::get_instance()->get_trace_enabled();
}

void
Log::set_trace_enabled(bool enable)
{
	LogPrivate::get_instance()->set_trace_enabled(enable);
}

bool
Log::get_sync_enabled()
{
	return LogPrivate::get_instance()->get_sync_enabled();
}

void
Log::set_sync_enabled(bool enable)
{
	LogPrivate::get_instance()->set_sync_enabled(enable);
}

bool
Log::get_properties_enabled()
{
	return LogPrivate::get_instance()->get_properties_enabled();
}

void
Log::set_properties_enabled(bool enable)
{
	LogPrivate::get_instance()->set_properties_enabled(enable);
}

void
Log::set_all_types_enabled(bool enable)
{
	LogPrivate::get_instance()->set_all_types_enabled(enable);
}

bool
Log::get_message_type_enabled()
{
	return LogPrivate::get_instance()->get_message_type_enabled();
}

void
Log::set_message_type_enabled(bool enable)
{
	LogPrivate::get_instance()->set_message_type_enabled(enable);
}

bool
Log::get_data_type_enabled()
{
	return LogPrivate::get_instance()->get_data_type_enabled();
}

void
Log::set_data_type_enabled(bool enable)
{
	LogPrivate::get_instance()->set_data_type_enabled(enable);
}

bool
Log::get_function_type_enabled()
{
	return LogPrivate::get_instance()->get_function_type_enabled();
}

void
Log::set_function_type_enabled(bool enable)
{
	LogPrivate::get_instance()->set_function_type_enabled(enable);
}

bool
Log::get_allocation_type_enabled()
{
	return LogPrivate::get_instance()->get_allocation_type_enabled();
}

void
Log::set_allocation_type_enabled(bool enable)
{
	LogPrivate::get_instance()->set_allocation_type_enabled(enable);
}

} // namespace adt
