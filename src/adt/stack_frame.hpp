#pragma once

#include <adt.hpp>

namespace adt {

class StackFrame
{
public: // Ctors
	StackFrame(void* const address)
	    : m_address(address)
	{
	}

	~StackFrame() { }

public: // Methods
	void* get_address() const
	{
		assert(m_address != nullptr);
		return m_address;
	}

	bool has_function_name() const { return !m_function_name.empty(); }

	std::string const& get_function_name() const { return m_function_name; }

	void set_function_name(char const* const function_name)
	{
		m_function_name = function_name;
	}

	void set_function_name(std::string&& function_name)
	{
		m_function_name = std::move(function_name);
	}

	bool has_file_name() const { return !m_file_name.empty(); }

	std::string const& get_file_name() const { return m_file_name; }

	void set_file_name(char const* const file_name) { m_file_name = file_name; }

	bool has_line_number() const { return m_line_number >= 0; }

	int32_t get_line_number() const { return m_line_number; }

	void set_line_number(int32_t line_number) { m_line_number = line_number; }

private: // Data
	void* const m_address = nullptr;

	std::string m_function_name;
	std::string m_file_name;
	int32_t m_line_number = -1;
};

} // namespace adt
