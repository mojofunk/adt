#include "adt/private/source_includes.cpp"

namespace adt {

FunctionLogger::FunctionLogger(LogCategory* const category,
                               int line,
                               const char* const file_path,
                               const char* const function_str,
                               void* this_ptr)
    : Logger(category)
{
	if (!category->enabled || !category->function_type_enabled) {
		return;
	}

	log_record = new LogRecord(
	    category,
	    RecordType::FUNCTION_CALL,
	    Timing(Timing::Type::DURATION, TimestampSource::get_microseconds()),
	    line,
	    file_path,
	    function_str,
	    this_ptr);

	if (m_trace_enabled) {
		log_record->stack_trace.reset(new StackTrace(2));
	}

	if (m_sync_enabled) {
		log_record->timing.type = Timing::Type::BEGIN;
		LogPrivate::s_instance->write_record_sync(*log_record);
		// dont delete record as it will be reused
	}
}

FunctionLogger::~FunctionLogger()
{
	if (log_record == nullptr) {
		return;
	}

	if (m_sync_enabled) {
		// reset these
		log_record->timing.type = Timing::Type::END;
		log_record->timing.first = TimestampSource::get_microseconds();
		LogPrivate::s_instance->write_record_sync(*log_record);
		delete log_record;
	} else {
		log_record->timing.second = TimestampSource::get_microseconds();
		LogPrivate::s_instance->write_record_async(log_record);
	}
}

} // namespace adt
