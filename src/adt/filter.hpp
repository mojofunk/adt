#pragma once

#include <adt.hpp>

namespace adt {

class Filter
{
public: // Methods
	Filter() = default;

	bool enabled() const
	{
		return (match_thread_id || match_thread_priority || match_instance ||
		        match_file_path || match_function);
	}

	bool matches(LogRecord const& record) const
	{
		if (not_matching) {
			return !matches_true(record);
		}

		return matches_true(record);
	}

	bool matches_true(LogRecord const& record) const
	{
		assert(enabled());

		if (match_thread_id && thread_id != record.thread_id) {
			return false;
		}

		if (match_thread_priority &&
		    thread_priority != get_thread_priority(record.thread_id)) {
			return false;
		}

		if (match_instance && instance_ptr != record.this_ptr) {
			return false;
		}

		if (match_file_path && file_path != record.src_location.file_path) {
			return false;
		}

		if (match_function && function_name != record.src_location.function_name) {
			return false;
		}
		return true;
	}

	static ThreadPriority get_thread_priority(std::thread::id p_thread_id)
	{
		auto thread_info = Threads::get_thread_info(p_thread_id);

		if (thread_info) {
			return thread_info->priority;
		}
		return ThreadPriority::NONE;
	}

	void set_match_thread_id(std::thread::id p_thread_id)
	{
		match_thread_id = true;
		thread_id = p_thread_id;
	}

	void set_match_thread_priority(ThreadPriority p_thread_priority)
	{
		match_thread_priority = true;
		thread_priority = p_thread_priority;
	}

	void set_match_instance(void* p_instance_ptr)
	{
		match_instance = true;
		instance_ptr = p_instance_ptr;
	}

	void set_match_duration(uint64_t p_min_duration, uint64_t p_max_duration)
	{
		match_duration = true;
		min_duration = p_min_duration;
		max_duration = p_max_duration;
	}

	void set_match_file_path(const char* const p_file_path)
	{
		match_file_path = true;
		file_path = p_file_path;
	}

	void set_match_line_range(int p_min_line, int p_max_line)
	{
		match_line_range = true;
		min_line = p_min_line;
		max_line = p_max_line;
	}

	void set_match_function(const char* const p_function_name)
	{
		match_function = true;
		function_name = p_function_name;
	}

public: // Data
	bool not_matching = false;

	bool match_thread_id = false;
	bool match_thread_priority = false;
	bool match_instance = false;
	bool match_duration = false;
	bool match_file_path = false;
	bool match_line_range = false;
	bool match_function = false;
	// bool match_property = false;

	std::thread::id thread_id;
	ThreadPriority thread_priority = ThreadPriority::NONE;
	void* instance_ptr = nullptr;
	uint64_t min_duration = 0;
	uint64_t max_duration = 0;
	const char* file_path = nullptr;
	int min_line = 0;
	int max_line = 0;
	const char* function_name = nullptr;
};

} // namespace adt
