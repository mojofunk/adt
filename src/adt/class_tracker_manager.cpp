#include "adt/private/source_includes.cpp"

namespace adt {

std::vector<ClassTracker*>
ClassTrackerManager::get_class_trackers()
{
	return ClassTrackerManagerPrivate::get_instance()->get_class_trackers();
}

void
ClassTrackerManager::add_class_tracker(ClassTracker* class_tracker)
{
	return ClassTrackerManagerPrivate::get_instance()->add_class_tracker(
	    class_tracker);
}

} // namespace adt
