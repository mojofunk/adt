#include "adt/private/source_includes.cpp"

namespace adt {

class TimestampSourcePrivate
{
public:
	TimestampSourcePrivate()
	{
		assert(s_instance == nullptr);
		s_instance = this;
	}

	~TimestampSourcePrivate() { ADT_DEBUG("~TimeStampPrivate\n"); }

	uint64_t get_microseconds()
	{
#ifdef __MINGW32__
		return m_qpc.get_microseconds();
#else
		auto duration = std::chrono::high_resolution_clock::now();
		auto const microseconds =
		    std::chrono::duration_cast<std::chrono::microseconds>(
		        duration.time_since_epoch())
		        .count();
		return static_cast<uint64_t>(microseconds);
#endif
	}

#ifdef __MINGW32__
	QPCTimestampSource m_qpc;
#endif

	A_SINGLETON(TimestampSourcePrivate);
	static TimestampSourcePrivate* s_instance; // cached
};

TimestampSourcePrivate* TimestampSourcePrivate::s_instance = nullptr;

TimestampSourcePrivate*
TimestampSourcePrivate::get_instance()
{
	return Singleton<TimestampSourcePrivate>::get();
}

void
TimestampSource::initialize()
{
	TimestampSourcePrivate::get_instance();
}

uint64_t
TimestampSource::get_microseconds()
{
	return TimestampSourcePrivate::s_instance->get_microseconds();
}

} // namespace adt
