#define A_TOKEN_PASTE_HELPER(x, y) x##y
#define A_TOKEN_PASTE(x, y) A_TOKEN_PASTE_HELPER(x, y)

#define A_STRINGIFY_HELPER(x) #x
#define A_STRINGIFY(y) A_STRINGIFY_HELPER(y)
