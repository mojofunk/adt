#pragma once

#include <adt.hpp>

namespace adt {

template <class StringType>
class PropertyTemplate
{
public:
	PropertyTemplate(StringType&& p_name, VariantTemplate<StringType>&& p_value)
	    : name(std::move(p_name))
	    , value(std::move(p_value))
	{
	}

	PropertyTemplate() = default;

	// PropertyTemplate(const PropertyTemplate& ) = default;

	// TODO just use const char* const?? issue with loadable modules
	StringType name;                   // TODO const
	VariantTemplate<StringType> value; // TODO
};

} // namespace adt
