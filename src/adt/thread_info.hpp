#pragma once

#include <adt.hpp>

namespace adt {

struct ThreadInfo {
public: // Ctors
	ThreadInfo(std::thread::id p_id,
	           const char* const p_name,
	           ThreadPriority p_priority)
	    : id(p_id)
	    , name(p_name)
	    , priority(p_priority)
	{
	}

	ThreadInfo() = default;

	ThreadInfo(const ThreadInfo&) = default;

	ThreadInfo& operator=(ThreadInfo const&) = default;

public: // Members
	std::thread::id id;
	std::string name = "Unnamed Thread";
	ThreadPriority priority = ThreadPriority::NONE;
};

} // namespace adt
