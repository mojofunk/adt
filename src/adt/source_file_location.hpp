#pragma once

#include <adt.hpp>

namespace adt {

struct SourceFileLocation {

	SourceFileLocation(int p_line,
	                   const char* const p_file_path,
	                   const char* const p_function_name)
	    : line(p_line)
	    , file_path(p_file_path)
	    , function_name(p_function_name)
	{
	}

	bool operator==(const SourceFileLocation& other)
	{
		return line == other.line && file_path == other.file_path;
	}

	const int line;
	const char* const file_path;
	const char* const function_name;
};

} // namespace adt
