#include "adt/private/source_includes.cpp"

namespace adt {

bool
GlobalAllocator::get_logging_enabled()
{
	return GlobalAllocatorPrivate::get_instance().get_logging_enabled();
}

void
GlobalAllocator::set_logging_enabled(bool enable)
{
	GlobalAllocatorPrivate::get_instance().set_logging_enabled(enable);
}

void
GlobalAllocator::set_allocator(std::shared_ptr<Allocator> alloc)
{
	return GlobalAllocatorPrivate::get_instance().set_allocator(alloc);
}

void
GlobalAllocator::reset_allocator()
{
	return GlobalAllocatorPrivate::get_instance().reset_allocator();
}

} // namespace adt
