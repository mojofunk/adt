#pragma once

#include <adt.hpp>

namespace adt {

class MessageLogger : public Logger
{
public:
	MessageLogger(LogCategory* const category,
	              int line,
	              const char* const file_path,
	              const char* const function_str,
	              void* this_ptr = nullptr);

	~MessageLogger();
};

} // namespace adt
