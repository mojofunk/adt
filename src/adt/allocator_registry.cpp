#include "adt/private/source_includes.cpp"

namespace adt {

/**
 * AllocatorRegistry may be accessed by the GlobalAllocator and so any
 * allocations must be performed via std::malloc.
 */
class AllocatorRegistryPrivate
{
public:
	std::vector<std::shared_ptr<Allocator>> get_allocators()
	{
		std::vector<std::shared_ptr<Allocator>> allocators;

		allocators.reserve(m_allocators.size());

		for (auto const& allocator : m_allocators) {
			allocators.push_back(allocator);
		}

		return allocators;
	}

	void add_allocator(std::shared_ptr<Allocator> allocator)
	{
		std::lock_guard<std::mutex> lock(m_allocators_mutex);

		m_allocators.insert(allocator);
	}

	void remove_allocator(std::shared_ptr<Allocator> allocator)
	{
		std::lock_guard<std::mutex> lock(m_allocators_mutex);

		m_allocators.erase(allocator);
	}

	using AllocatorSet = std::set<std::shared_ptr<Allocator>,
	                              std::less<std::shared_ptr<Allocator>>,
	                              StdMallocAllocator<std::shared_ptr<Allocator>>>;

	AllocatorSet m_allocators;
	std::mutex m_allocators_mutex;
};

static AllocatorRegistryPrivate&
get_allocator_registry_private()
{
	static AllocatorRegistryPrivate allocator_registry_p;
	return allocator_registry_p;
}

void
AllocatorRegistry::initialize()
{
	get_allocator_registry_private();
}


std::vector<std::shared_ptr<Allocator>>
AllocatorRegistry::get_allocators()
{
	return get_allocator_registry_private().get_allocators();
}

void
AllocatorRegistry::add_allocator(std::shared_ptr<Allocator> allocator)
{
	get_allocator_registry_private().add_allocator(allocator);
}

void
AllocatorRegistry::remove_allocator(std::shared_ptr<Allocator> allocator)
{
	get_allocator_registry_private().remove_allocator(allocator);
}

} // namespace adt
