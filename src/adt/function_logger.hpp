#pragma once

#include <adt.hpp>

namespace adt {

class FunctionLogger : public Logger
{
public:
	FunctionLogger(LogCategory* const category,
	               int line,
	               const char* const file_path,
	               const char* const function_str,
	               void* this_ptr = nullptr);

	~FunctionLogger();
};

} // namespace adt
