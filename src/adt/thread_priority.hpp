#pragma once

#include <adt.hpp>

namespace adt {

enum class ThreadPriority { NONE, LOW, NORMAL, HIGH, REALTIME };

} // namespace adt
