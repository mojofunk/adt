#include "adt/private/source_includes.cpp"

namespace adt {

const char*
variant_type_name(const VariantType type)
{
	static const char* const names[] = { "none",   "pointer", "bool",   "float",
		                                    "double", "int32",   "uint32", "int64",
		                                    "uint64", "string" };

	return names[(int)type];
}

} // namespace adt
