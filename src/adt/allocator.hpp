#pragma once

#include <adt.hpp>

namespace adt {

class Allocator
{
public:
	virtual ~Allocator() {}

	virtual std::string get_name() const = 0;

	virtual std::string get_type_name() const = 0;

	virtual bool is_from(void*) const = 0;

#if 0
	virtual void set_monitoring_enabled(bool on_off) = 0;

	virtual bool get_monitoring_enabled() = 0;
#endif

	virtual void* allocate(std::size_t size) = 0;

	virtual void deallocate(void* ptr) = 0;

	// Optional
	virtual void deallocate(void* ptr, std::size_t bytes)
	{
		(void)bytes;
		deallocate(ptr);
	};

	template <class T>
	static std::shared_ptr<T> make_shared()
	{
		// TODO forward args
		return std::shared_ptr<T>(
		    new T, std::default_delete<T>(), StdMallocAllocator<T>());
	}
};

} // namespace adt
