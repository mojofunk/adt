#pragma once

#include <adt.hpp>

namespace adt {

class ClassTrackerLogger : public Logger
{
public:
	ClassTrackerLogger(LogCategory* const category,
	                   int line,
	                   const char* const file_path,
	                   const char* const function_str,
	                   void* this_ptr = nullptr);

	~ClassTrackerLogger();

	void set_stack_trace(StackTrace* trace)
	{
		log_record->stack_trace.reset(trace);
	}
};

} // namespace adt
