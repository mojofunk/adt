#define A_CLASS_MSG(Message) A_LOG_CLASS_MSG(class_category, Message)

#define A_CLASS_STATIC_MSG(Message) A_LOG_MSG(class_category, Message)

#define A_CLASS_DURATION(Message) A_LOG_CLASS_DURATION(class_category, Message)

#define A_CLASS_STATIC_DURATION(Message) A_LOG_DURATION(class_category, Message)

#define A_CLASS_CALL() A_LOG_CLASS_CALL(class_category)

#define A_CLASS_CALL1(Arg1) A_LOG_CLASS_CALL1(class_category, Arg1)

#define A_CLASS_CALL2(Arg1, Arg2) A_LOG_CLASS_CALL2(class_category, Arg1, Arg2)

#define A_CLASS_CALL3(Arg1, Arg2, Arg3)                                        \
	A_LOG_CLASS_CALL3(class_category, Arg1, Arg2, Arg3)

#define A_CLASS_CALL4(Arg1, Arg2, Arg3, Arg4)                                  \
	A_LOG_CLASS_CALL4(class_category, Arg1, Arg2, Arg3, Arg4)

#define A_CLASS_CALL5(Arg1, Arg2, Arg3, Arg4, Arg5)                            \
	A_LOG_CLASS_CALL5(class_category, Arg1, Arg2, Arg3, Arg4, Arg5)

#define A_CLASS_CALL6(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)                      \
	A_LOG_CLASS_CALL6(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define A_CLASS_CALL7(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)                \
	A_LOG_CLASS_CALL7(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#define A_CLASS_CALL8(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)          \
	A_LOG_CLASS_CALL8(                                                            \
	    class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)

#define A_CLASS_STATIC_CALL() A_LOG_CALL(class_category)

#define A_CLASS_STATIC_CALL1(Arg1) A_LOG_CALL1(class_category, Arg1)

#define A_CLASS_STATIC_CALL2(Arg1, Arg2) A_LOG_CALL2(class_category, Arg1, Arg2)

#define A_CLASS_STATIC_CALL3(Arg1, Arg2, Arg3)                                 \
	A_LOG_CALL3(class_category, Arg1, Arg2, Arg3)

#define A_CLASS_STATIC_CALL4(Arg1, Arg2, Arg3, Arg4)                           \
	A_LOG_CALL4(class_category, Arg1, Arg2, Arg3, Arg4)

#define A_CLASS_STATIC_CALL5(Arg1, Arg2, Arg3, Arg4, Arg5)                     \
	A_LOG_CALL5(class_category, Arg1, Arg2, Arg3, Arg4, Arg5)

#define A_CLASS_STATIC_CALL6(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)               \
	A_LOG_CALL6(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define A_CLASS_STATIC_CALL7(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)         \
	A_LOG_CALL7(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#define A_CLASS_STATIC_CALL8(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)   \
	A_LOG_CALL8(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)

#define A_CLASS_DATA1(Arg1) A_LOG_CLASS_DATA1(class_category, Arg1)

#define A_CLASS_DATA2(Arg1, Arg2) A_LOG_CLASS_DATA2(class_category, Arg1, Arg2)

#define A_CLASS_DATA3(Arg1, Arg2, Arg3)                                        \
	A_LOG_CLASS_DATA3(class_category, Arg1, Arg2, Arg3)

#define A_CLASS_DATA4(Arg1, Arg2, Arg3, Arg4)                                  \
	A_LOG_CLASS_DATA4(class_category, Arg1, Arg2, Arg3, Arg4)

#define A_CLASS_DATA5(Arg1, Arg2, Arg3, Arg4, Arg5)                            \
	A_LOG_CLASS_DATA5(class_category, Arg1, Arg2, Arg3, Arg4, Arg5)

#define A_CLASS_DATA6(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)                      \
	A_LOG_CLASS_DATA6(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define A_CLASS_DATA7(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)                \
	A_LOG_CLASS_DATA7(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#define A_CLASS_DATA8(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)          \
	A_LOG_CLASS_DATA8(                                                            \
	    class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)

#define A_CLASS_STATIC_DATA1(Arg1) A_LOG_DATA1(class_category, Arg1)

#define A_CLASS_STATIC_DATA2(Arg1, Arg2) A_LOG_DATA2(class_category, Arg1, Arg2)

#define A_CLASS_STATIC_DATA3(Arg1, Arg2, Arg3)                                 \
	A_LOG_DATA3(class_category, Arg1, Arg2, Arg3)

#define A_CLASS_STATIC_DATA4(Arg1, Arg2, Arg3, Arg4)                           \
	A_LOG_DATA4(class_category, Arg1, Arg2, Arg3, Arg4)

#define A_CLASS_STATIC_DATA5(Arg1, Arg2, Arg3, Arg4, Arg5)                     \
	A_LOG_DATA5(class_category, Arg1, Arg2, Arg3, Arg4, Arg5)

#define A_CLASS_STATIC_DATA6(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)               \
	A_LOG_DATA6(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define A_CLASS_STATIC_DATA7(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)         \
	A_LOG_DATA7(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#define A_CLASS_STATIC_DATA8(Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)   \
	A_LOG_DATA8(class_category, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)
