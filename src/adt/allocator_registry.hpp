#pragma once

#include <adt.hpp>

namespace adt {

class AllocatorRegistry
{
public:
	static void initialize();

public: // API all static
	static std::vector<std::shared_ptr<Allocator>> get_allocators();

	static void add_allocator(std::shared_ptr<Allocator>);

	static void remove_allocator(std::shared_ptr<Allocator>);

private: // Ctor
	AllocatorRegistry() = delete;
	A_DISALLOW_COPY_AND_ASSIGN(AllocatorRegistry);
};

} // namespace adt
