#pragma once

#include <adt.hpp>

namespace adt {

using Property = PropertyTemplate<std::string>;

} // namespace adt
