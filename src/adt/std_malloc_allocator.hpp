#pragma once

#include <adt.hpp>

namespace adt {

template <typename T>
class StdMallocAllocator
{
public:
	typedef T value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;

public:
	template <typename U>
	struct rebind {
		typedef StdMallocAllocator<U> other;
	};

public:
	inline StdMallocAllocator(){}; // = default

	inline ~StdMallocAllocator() {}

	inline explicit StdMallocAllocator(const StdMallocAllocator<T>&) {}

	template <typename U>
	inline StdMallocAllocator(const StdMallocAllocator<U>&)
	{
	}

	bool empty() { return false; }

	inline pointer address(reference r) { return &r; }
	inline const_pointer address(const_reference r) { return &r; }

	inline pointer allocate(size_type cnt, const void* hint = 0)
	{
		(void)hint;

		if (cnt <= max_size()) {
			void* ptr = std::malloc(cnt * sizeof(T));
			if (ptr) {
				return static_cast<T*>(ptr);
			}
		}
		throw std::bad_alloc();
	}

	inline void deallocate(pointer ptr, size_type) { std::free(ptr); }

	inline size_type max_size() const
	{
		return std::numeric_limits<std::size_t>::max() / sizeof(T);
	}

	inline void construct(pointer p, const T& t) { new (p) T(t); }

	inline void destroy(pointer p) { p->~T(); }

#if 0
	inline bool operator==(const StdMallocAllocator&) { return true; }
	inline bool operator!=(const StdMallocAllocator& a) { return !operator==(a); }
#endif

	template <class U1, class U2>
	friend bool operator==(const StdMallocAllocator<U1>& x,
	                       const StdMallocAllocator<U2>& y) noexcept;

	template <class U>
	friend class StdMallocAllocator;
};

template <class U1, class U2>
inline bool
operator==(const StdMallocAllocator<U1>&,
           const StdMallocAllocator<U2>&) noexcept
{
	return true;
}

template <class U1, class U2>
inline bool
operator!=(const StdMallocAllocator<U1>& x,
           const StdMallocAllocator<U2>& y) noexcept
{
	return !(x == y);
}

} // namespace adt
