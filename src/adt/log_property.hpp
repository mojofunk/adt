#pragma once

#include <adt.hpp>

namespace adt {

class LogProperty
{
public:
	template <class T>
	explicit LogProperty(const char* const c_str, T val)
	    : name_c_str(c_str)
	    , value(std::move(val))
	{
	}

	LogProperty(LogProperty&&) = delete;

	LogProperty() = delete;

public:
	const char* const name_c_str = nullptr;
	const LogVariant value;

private:
	A_DISALLOW_COPY_AND_ASSIGN(LogProperty);
};

} // namespace adt
