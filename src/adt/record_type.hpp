#pragma once

#include <adt.hpp>

namespace adt {

enum class RecordType : uint8_t {
	MESSAGE,
	DATA,
	FUNCTION_CALL,
	ALLOCATION,
};

class ADT_API RecordTypeNames
{
public:
	static const char* const message;
	static const char* const data;
	static const char* const function;
	static const char* const allocation;

	static const char* get_name(RecordType const);
};

} // namespace adt
