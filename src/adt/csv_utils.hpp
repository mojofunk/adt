#pragma once

#include <adt.hpp>

namespace adt {

bool
write_records_to_csv_file(File&,
                          std::vector<std::shared_ptr<Record>> const&,
                          CSVOptions const&);

} // namespace adt
