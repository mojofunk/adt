#pragma once

#include <adt.hpp>

namespace adt {

class Record : public RecordBase
{
public: // ctors
	Record(LogRecord& r)
	    : RecordBase(r.category,
	                 r.type,
	                 r.thread_id,
	                 r.timing,
	                 r.src_location.line,
	                 r.src_location.file_path,
	                 r.src_location.function_name,
	                 r.this_ptr)
	{
		properties.reserve(8);

		add_valid_log_property(r.property_1) &&
		    add_valid_log_property(r.property_2) &&
		    add_valid_log_property(r.property_3) &&
		    add_valid_log_property(r.property_4) &&
		    add_valid_log_property(r.property_5) &&
		    add_valid_log_property(r.property_6) &&
		    add_valid_log_property(r.property_7) &&
		    add_valid_log_property(r.property_8);

		// move the trace as LogRecord is deleted after copying anyway
		stack_trace = std::move(r.stack_trace);
	}

public:
	std::string to_string(bool include_properties = true) const;

	std::string properties_to_string() const;

	/**
	 * Get indexes into vector for properties matching names
	 *
	 * @return Indexes to properties or an empty vector if not all properties
	 * matching the names are found.
	 */
	std::vector<std::size_t>
	get_properties(std::vector<std::string> const& names) const;

public: // optional data
	std::vector<Property> properties;

private:
	static Property convert_to_property(const LogProperty& log_property);

	/**
	 * @return true if LogProperty was non-null
	 */
	bool add_valid_log_property(const LogProperty* log_property)
	{
		if (log_property != nullptr) {
			Property prop(convert_to_property(*log_property));

			assert(prop.value.type != VariantType::NONE);

			properties.emplace_back(std::move(prop));
			return true;
		}
		return false;
	}

private:
	A_DISALLOW_COPY_AND_ASSIGN(Record);
};

} // namespace adt
