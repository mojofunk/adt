#ifndef ADT_TEST_H
#define ADT_TEST_H

#include <adt.hpp>

#ifndef ADT_ENABLE_NOOP

#include "adt-test/test_utils.hpp"

#include "adt-test/test.hpp"

#include "adt-test/concurrency_test.hpp"
#endif

#endif // ADT_TEST_H
