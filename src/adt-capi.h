#ifndef ADT_CAPI_H
#define ADT_CAPI_H

#include "adt/visibility.h"

#include "adt/macros.h"
#include "adt/strfunc.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "adt-capi/log_category.h"

#include "adt-capi/log_record.h"

#include "adt-capi/logger.h"

#include "adt-capi/log_call.h"

#ifdef __cplusplus
}
#endif

#include "adt-capi/log_macros.h"

#endif /* ADT_CAPI_H */
