BUILD_DIR=cmake-build-coverage

rm -rf $BUILD_DIR

mkdir $BUILD_DIR && cd $BUILD_DIR

BUILD_TYPE="-DCMAKE_BUILD_TYPE=Debug"

cmake -GNinja $BUILD_TYPE "-DBUILD_SHARED_LIBS=1" "-DENABLE_COVERAGE=ON" ../../../ && ninja -v adt-tests-coverage
gnome-open adt-tests-coverage.html
