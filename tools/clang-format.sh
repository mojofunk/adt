#! /bin/bash

if hash clang-format 2>/dev/null; then
    find ../src/adt -type f -name "*.?pp" | xargs clang-format -i "$@"
    find ../tests -type f -name "*.?pp" | xargs clang-format -i "$@"
else
    echo "clang-format is not installed, cannot automatically format source code"
fi
