#! /bin/bash

set -x

# directory that contains vcpkg
DEV_DIR=../../..
# relative when inside build dir
TOP_DIR=../../..

BUILD_DIR=cmake-vcpkg-build

VCPKG_PATH=`cygpath -w ${DEV_DIR}/vcpkg`
VCPKG_TOOLCHAIN_FILE="$VCPKG_PATH\scripts\buildsystems\vcpkg.cmake"

VSDEVCMD_PATH="C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\Common7\Tools\VsDevCmd.bat"
CMAKE_PATH="C:\Program Files\CMake\bin"
NINJA_PATH="$VCPKG_PATH\downloads\tools\ninja\ninja-1.8.2"

# TODO add arch argument
function call_in_vsdevcmd
{
	cat << EOF > vsdevcmd.tmp.bat
	CALL "$VSDEVCMD_PATH"
	SET PATH=%PATH%;$CMAKE_PATH;$NINJA_PATH
	ECHO %PATH%
	$1
EOF

	./vsdevcmd.tmp.bat
}
